package mercs.data;

import java.io.IOException;

import clus.data.rows.DataTuple;
import clus.data.rows.RowData;
import clus.util.ClusException;
import mercs.core.MercsRun;
import mercs.util.MercsException;

/**
 * MemoryStream
 *
 * This is an extension of the MercsData class
 *
 * This data type represents a stream of memory resident data.
 * It is possible to retrieve the memory data as a whole for non-incremental learners.
 *
 * This implies, of course, that this class is general to suit both kinds of settings (i.e. (non)-incremental)
 *
 */


public class MemoryStream extends MercsData{

    /**
     * =======================================
     * Properties (E.g.:Decl, Get, Set, isValid, ...)
     * =======================================
     */

    private RowData data;
    private int index;

    /**
     * Retrieves the memory resident data as a whole for non-incremental
     * learning
     */
    public RowData getData() {
        return data;
    }
    /**
     * Sets the data of the stream
     */
    private void setData(RowData data) {
        this.data = data;
    }


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes the memorystream.
     *
     * @param data RowData object, contains the actual data.
     * @param mr MercsRun that contains all the relevant information about Mercs
     * @param maxPasses Maximum amount of passes over the data
     * @param maxSamples Maximum number of samples to read
     *
     */
    public MemoryStream(MercsRun mr, RowData data, long maxSamples, int maxPasses) throws IOException, ClusException {
        super(mr, maxSamples, maxPasses);
        setData(data);
        init();
    }

    /**
     * Reinitializes the pointer of the stream to the beginning of the data.
     */
    @Override
    public void init() throws IOException, ClusException {
        super.init();
        index = 0;
    }

    /*
    * Related to data
    * */

    /**
     * Checks whether there is a next data tuple available.
     *
     * Also takes into account the maximum amount of samples that may be read.
     *
     * @throws ClusException
     */
    @Override
    public boolean hasNextTuple() throws IOException, ClusException {
        if (!super.hasNextTuple()) {
            return false;
        }
        if (getData() == null || index >= data.getNbRows() && getCurrentPass() < getMaxPasses()) {
            incrementCurrentPass();
            init();
            return hasNextTuple();
        }

        return getData() != null && index < data.getNbRows();
    }

    /**
     * Reads the next data tuple. If no data tuple is available an exception is
     * thrown.
     *
     */
    @Override
    public DataTuple readNextTuple() throws IOException, MercsException {
        if (getData() == null || index >= data.getNbRows())
            throw new MercsException("No tuples available");
        incrementSamplesRead();
        return getData().getTuple(index++);
    }

    /*
    * Related to stream itself
    * */

    /**
     * Closes the stream.
     */
    @Override
    public void close() {
        setData(null);
    }

    /**
     * Makes a new MercsData object, equal to this object but re-initialised.
     */
    @Override
    public MercsData copy() throws IOException, ClusException {
        MemoryStream copy = new MemoryStream(getMercsRun(), getData(), getMaxSamples(), getMaxPasses());
        copy.init();

        return copy;
    }
}
