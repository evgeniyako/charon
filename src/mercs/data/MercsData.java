package mercs.data;

import java.io.IOException;

import clus.data.io.ClusView;
import clus.data.rows.DataTuple;
import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.core.MercsRun;
import mercs.util.MercsException;

/*
* An abstract class representing data.
* This is a datastream.
* For RAM data, a memorystream can be used
* */

public abstract class MercsData {
    /**
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    protected MercsRun mercsRun;
    protected ClusSchema schema;
    protected ClusView view;

    private int maxPasses;
    private int currentPass;
    private long maxSamples;
    private long samplesRead;


    /**
     * Returns the mercs run of this data object
     */
    public MercsRun getMercsRun() {
        return mercsRun;
    }
    /**
     * Sets the mercsrun of this data object
     */
    private void setMercsRun(MercsRun mercsRun) {
        this.mercsRun = mercsRun;
    }
    /**
     * Checks whether the given mercsRun is valid. It is valid if and only if it
     * is not null.
     */
    public boolean isValidMercsRun(MercsRun mercsRun) {
        return mercsRun != null;
    }

    /**
     * Returns the schema
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema
     */
    protected void setSchema(ClusSchema schema) {
        this.schema = schema;
    }

    /**
     * Returns the view
     */
    public ClusView getView() {
        return view;
    }
    /**
     * Sets the view
     */
    protected void setView(ClusView view) {
        this.view = view;
    }


    /**
     * = 0 Returns the maximum amount of samples that this data will read
     * (re-init not included)
     */
    public long getMaxSamples() {
        return maxSamples;
    }
    /**
     * Sets the maximum samples that can be read
     */
    public void setMaxSamples(long maxSamples) {
        this.maxSamples = maxSamples;
    }

    /**
     * Returns the current pass over the data
     */
    public int getCurrentPass() {
        return currentPass;
    }
    /**
     * Sets the currentPass over the data
     */
    private void setCurrentPass(int currentPass) {
        this.currentPass = currentPass;
    }

    /**
     * Returns the maximum amount of passes allowed over the data
     */
    public int getMaxPasses() {
        return maxPasses;
    }
    /**
     * Sets the maximum amount of passes over the data allowed
     */
    private void setMaxPasses(int maxPasses) {
        this.maxPasses = maxPasses;
    }

    /**
     * Returns the amount of samples read
     */
    public long getSamplesRead() {
        return samplesRead;
    }
    /**
     * Sets the amount of samples that is read
     */
    private void setSamplesRead(long samplesRead) {
        this.samplesRead = samplesRead;
    }


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Initializes this data from the given mercRun, the mercsRun is used to
     * initialize the schema and view. The schema contains the description of
     * the data, while the view provides the functionality to read data tuples.
     * The maximum amount of samples read is set to the given parameter value.
     *
     * @throws MercsException
     *             Thrown when the given mercsRun is not valid
     */
    public MercsData(MercsRun mercsRun, long maxSamples, int maxPasses) throws MercsException {
        if (!isValidMercsRun(mercsRun)) {
            throw new MercsException("Invalid MercsRun");
        }
        setMercsRun(mercsRun);
        setSchema(mercsRun.getSchema());
        setView(mercsRun.getView());
        setMaxSamples(maxSamples);
        setMaxPasses(maxPasses);
        setCurrentPass(1);
    }

    /**
     * Initializes the stream
     */
    public void init() throws IOException, ClusException {
        setSamplesRead(0);
    }

    /**
     * Increments the amount of samples read
     */
    public void incrementSamplesRead() {
        setSamplesRead(getSamplesRead() + 1);
    }

    /**
     * Increments the current pass over the data
     */
    protected void incrementCurrentPass() {
        setCurrentPass(getCurrentPass() + 1);
    }

    /**
     * Checks whether there is a next data tuple available.
     *
     * Also takes into account the maximum amount of samples that may be read.
     *
     * @throws ClusException
     */
    public boolean hasNextTuple() throws IOException, ClusException {
        return getSamplesRead() < getMaxSamples();
    }


    /**
     * =======================================
     * ABSTRACT METHODS
     * =======================================
     */

    /**
     * Reads the next data tuple. If no data tuple is available an exception is
     * thrown.
     *
     */
    public abstract DataTuple readNextTuple() throws IOException, ClusException;

    /**
     * Closes the stream.
     */
    public abstract void close() throws IOException;

    /**
     * Makes a new MercsData object, equal to this object but re-initialised.
     */
    public abstract MercsData copy() throws IOException, ClusException;

}
