package mercs.data;

import java.io.IOException;

import clus.util.ClusException;
import mercs.core.MercsRun;

/**
 *  StreamBuilder
 *
 *  Class used to build datastreams for the Mercs system.
 *
 *  This can be either MemoryStreams or DiskStreams.
 *
 *  NB.: T stands for type. Avoids having to use the explicit class name and allows to still specify the methods in the superclass
 * */
public abstract class StreamBuilder<T extends StreamBuilder<T>> {

    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    private MercsRun mercsRun;
    private long maxSamples = Long.MAX_VALUE;
    private int maxPasses = 1;

    /**
     * Set the MercsRun field
     * */
    public T setMercsRun(MercsRun cr) {
        this.mercsRun = cr;
        return getThis();
    }
    /**
     * Return the MercsRun
     * */
    public MercsRun getMercsRun() {
        return mercsRun;
    }

    /**
     * Get the maximum amount of samples
     * */
    public long getMaxSamples() {
        return maxSamples;
    }
    /**
     * Set the maximum amount of samples
     * */
    public T setMaxSamples(long maxSamples) {
        this.maxSamples = maxSamples;
        return getThis();
    }

    /**
     * Return the maximum amount of passes over the data
     * */
    public int getMaxPasses() {
        return maxPasses;
    }
    /**
     * Set the maximum amount of passes over the data
     * */
    public T setMaxPasses(int maxPasses) {
        this.maxPasses = maxPasses;
        return getThis();
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Return the current object
     * */
    public abstract T getThis();

    /**
     * Build the actual memorystream
     * */
    public abstract MercsData build() throws IOException, ClusException;

}