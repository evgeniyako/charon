package mercs.data;

import java.io.IOException;

import clus.Clus;
import clus.data.rows.RowData;
import clus.main.Settings;
import clus.util.ClusException;

import mercs.core.MercsRun;


/**
 * MemoryStreamBuilder
 *
 * StreamBuilder -> *MemorystreamBuilder* || DiskStreamBuilder
 *
 * */
public class MemoryStreamBuilder extends StreamBuilder<MemoryStreamBuilder> {

    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    private RowData data;

    /**
     * Return the data (a RowData object) property of this MemoryStreamBuilder object
     * */
    public RowData getData() {
        return data;
    }
    /**
     * Set the data property and return the MemoryStreamBuilder itself.
     * */
    public MemoryStreamBuilder setData(RowData data) {
        this.data = data;
        return this;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Return the current object
     * */
    @Override
    public MemoryStreamBuilder getThis() {
        return this;
    }

    /**
     * initializeData
     *
     * Initialize the data contained in this object. This means filling in
     * the data property of the MemoryStream by getting the data from the given MercsRun
     * */
    private void initializeData(MercsRun mercsRun) throws IOException, ClusException {
        if (mercsRun.getData() instanceof MemoryStream) {                                               // If it is already a MemoryStream object, we can use the getData method of *this* class
            setData(((MemoryStream) mercsRun.getData()).getData());
        } else {
            setData(mercsRun.getView().readData(mercsRun.getReader(), mercsRun.getSchema()));           // If it is not, we read the data in the usual way (MercsRun->ClusView->readData method)
            mercsRun.getReader().close();
        }

        if (mercsRun.getSchema().getSettings().getNormalizeData() == Settings.NORMALIZE_DATA_NUMERIC) { // if the numeric data should be normalized, do so.
            // Uses Clus normalize method
            setData(Clus.returnNormalizedData(getData()));
        }
    }

    /**
     * Build the actual memorystream
     * */
    @Override
    public MemoryStream build() throws IOException, ClusException {
        if (getData() == null) {                                                            // If we do not have any data present in the current object, we first initialize
            initializeData(getMercsRun());
        }

        return new MemoryStream(getMercsRun(), getData(), getMaxSamples(), getMaxPasses()); // Return a new MemoryStream with the correct parameters
    }
}
