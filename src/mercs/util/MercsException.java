package mercs.util;

import clus.util.ClusException;
import mercs.core.SettingsMercs;

/**
 * Specific exception for the Mercs system.
 * Contains an error message (obviously)
 */
public class MercsException extends ClusException {
    public final static long serialVersionUID = SettingsMercs.SERIAL_VERSION_ID;

    public MercsException(String msg) {
        super(msg);
    }

    @Override
    public String toString() {
        return getMessage();
    }

}
