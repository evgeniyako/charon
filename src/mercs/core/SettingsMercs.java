package mercs.core;

import jeans.io.ini.*;
import jeans.math.MathUtil;
import mercs.util.MercsException;

import java.util.Enumeration;

/**
 * Extension of the Clus Settings file to the Mercs scenario
 *
 * We rely heavily on the jeans package here, just as the Clus system did.
 * Example:
 *      Consider syntax INIFileDouble. This means that we fetch a double
 *      from the settingsfile. The exact name of this variable tells us where to collect it in the
 *      settings file. It is then stored in the variable's name.
 */
public class SettingsMercs extends clus.main.Settings {
    private static final long serialVersionUID = 285024601724623798L;   // I have no clue why this is here

    /*
     * =======================================
     * 1] DATA
     * =======================================
     */

    public final static double DATA_MAX_SAMPLES_DEFAULT = Long.MAX_VALUE;

    /**
    * The amount of maximal samples to read, useful to restrict using all data.
    * We can then easily collect statistics on runs w/ different data sizes, w/o switching datasets.
    */
    protected INIFileDouble m_DataMaxSamples;

    public void setDataMaxSamples(double value) {
        m_DataMaxSamples.setValue(value);
    }
    public long getDataMaxSamples() {
        long value = (long) m_DataMaxSamples.getValue();
        return Math.min(Long.MAX_VALUE, Math.max(0, value));
    }

    public String getDataSettingsInfo() {
        return getSettingsInfo((INIFileSection) m_Ini.getNode("Data"));
    }

    /*
     * =======================================
     * 2] MERCS
     * =======================================
     */

    /*
    * The different options we have in the Mercs system,
    * And their translation to numbers.
    *
    * Basically, there are three BIG choices to make:
    * 1] Base type:         "TDIDT", "Incremental (~VFDT)"
    *                               This is the choice between static learning (first learn, then predict) or dynamic learning.
    *
    * 2] Selection method:   "Naive", "Base", "Random", "Iterate"
    *                               How do we select the different components of the Mercs Model (we cannot have them all!)
    *
    * 3] Prediction method: "Simple", "ModelWeighted", "InputCapped", "ParticipationCapped"
    *                               Given our components, how do we combine them to get the prediction we want?
    *
    * Lastly, less fundamental, there is some stuff going on in terms of accuracy.
    */

    public final static String[] MERCS_BASE_TYPE = { "TDIDT", "Incremental" };
    public final static String[] MERCS_SELECTION_METHOD = { "Naive", "Base", "Random", "Iterate" };
    public final static String[] MERCS_PREDICTION_METHOD = { "Simple", "ModelWeighted", "InputCapped", "ParticipationCapped" };
    public final static String[] MERCS_SELECTION_ACCURACY_MEASURE = { "ExampleAccuracy", "LabelAccuracy" };

    public final static int MERCS_BASE_TYPE_TDIT = 0;
    public final static int MERCS_BASE_TYPE_VFDT = 1;

    public final static int MERCS_SELECTION_TYPE_NAIVE = 0;
    public final static int MERCS_SELECTION_TYPE_BASE = 1;
    public final static int MERCS_SELECTION_TYPE_RANDOM = 2;
    public final static int MERCS_SELECTION_TYPE_ITERATE = 3;

    public final static int MERCS_PREDICTION_TYPE_SIMPLE = 0;
    public final static int MERCS_PREDICTION_TYPE_MODELWEIGHTED = 1;
    public final static int MERCS_PREDICTION_TYPE_INPUTCAPPED = 2;
    public final static int MERCS_PREDICTION_TYPE_PARTICIPATIONCAPPED = 3;

    public final static int MERCS_SELECTION_ACCURACY_MEASURE_EXAMPLE = 0;
    public final static int MERCS_SELECTION_ACCURACY_MEASURE_LABEL = 1;


    /*
    * Everything relevant for the settings file. The INI comes from the jeans package,
    * which handles all the interaction with the settings file.
    *
    * Properties, first the fields, followed by get and set
    */
    protected INIFileSection m_SectionMercs;
    protected INIFileString m_MercsTestSet;                     // Unseen set used after complete training (and pruning), to assess real performance.
    protected INIFileBool m_StoreMercsModel;                    // Whether we need to store the loaded model;
    protected INIFileString m_MercsModelStorage;                // The location where a MERCS model should be stored or loaded from

    protected INIFileNominal m_MercsBaseType;                   // The base type of the mercs model (i.e. TDIT or VFDT)

    protected INIFileNominal m_MercsSelectionMethod;            // The main selection method that is used to learn new trees (which trees do we need?)
    protected INIFileNominal m_MercsSelectionAccuracyMeasure;   // The accuracy measure for the selection (i.e. example or label)
    protected INIFileInt m_MaxSelectionIterations;              // Number of iterations in the selection process

    protected INIFileNominal m_MercsPredictionMethod;           // The prediction algorithm used to predict an example.
    protected INIFileBool m_MercsPredictionWeighted;            // Whether to use a weighted prediction approach (if possible).

    /**
     * Do we print Mercs settings to the output file?
     * */
    public boolean isSectionMercsEnabled() {
        return m_SectionMercs.isEnabled();
    }

    public String getMercsTestSet() throws MercsException {
        if (m_MercsTestSet != null && !m_MercsTestSet.getValue().isEmpty()) {
            return m_MercsTestSet.getValue();
        } else {
            throw new MercsException("No Mercs Test Set supplied");
        }
    }
    public void setMercsTestSet(String testFile) {
        m_MercsTestSet.setValue(testFile);
    }

    public boolean shouldModelBeStored() {
        return m_StoreMercsModel.getValue();
    }

    public String getMercsModelStorage() {
        return m_MercsModelStorage.getValue();
    }


    public int getMercsBaseType() {
        return m_MercsBaseType.getValue();
    }
    public void setMercsBaseType(String value) {
        m_MercsBaseType.setValue(value);
    }
    public void setMercsBaseType(int value) {
        m_MercsBaseType.setSingleValue(value);
    }


    public int getMercsSelectionMethod() {
        return m_MercsSelectionMethod.getValue();
    }
    public void setMercsSelectionMethod(String value) {
        m_MercsSelectionMethod.setValue(value);
    }
    public void setMercsSelectionMethod(int value) {
        m_MercsSelectionMethod.setSingleValue(value);
    }

    public int getMercsSelectionAccuracyMeasure() {
        return m_MercsSelectionAccuracyMeasure.getValue();
    }
    public void setMercsSelectionAccuracyMeasure(String value) {
        m_MercsSelectionAccuracyMeasure.setValue(value);
    }
    public void setMercsSelectionAccuracyMeasure(int value) {
        m_MercsSelectionAccuracyMeasure.setSingleValue(value);
    }

    public int getMaxSelectionIterations() {
        return m_MaxSelectionIterations.getValue();
    }
    public void setMaxSelectionIterations(int value) {
        value = Math.max(0, value);
        m_MaxSelectionIterations.setValue(value);
    }


    public void setMercsPredictionMethod(String value) {
        m_MercsPredictionMethod.setValue(value);
    }

    public int getMercsPredictionMethod() {
        return m_MercsPredictionMethod.getValue();
    }
    public void setMercsPredictionMethod(int value) {
        m_MercsPredictionMethod.setSingleValue(value);
    }

    public boolean getMercsPredictionWeighted() {
        return m_MercsPredictionWeighted.getValue();
    }
    public void setMercsPredictionWeighted(boolean value) {
        m_MercsPredictionWeighted.setValue(value);
    }


    /*
     * =======================================
     * 3] MercsTest-stuff
     * =======================================
     */

    // The section group bundles different tests.
    private INIFileSectionGroup m_SectionGroupMercsTest;

    // No direct access to instances within the section group, so we need helper methods to access all we want to know.
    public int getNbMercsTests() {
        return m_SectionGroupMercsTest.getNbSections();
    }

    public String getMercsTestName(int idx) {
        INIFileSection sec = m_SectionGroupMercsTest.getSectionAt(idx);
        return sec.getName();
    }

    public String getMercsTestSet(int idx) {
        INIFileSection sec = m_SectionGroupMercsTest.getSectionAt(idx);
        return ((INIFileString) sec.getEntry("TestSet")).getValue();
    }

    public String getMercsTestTarget(int idx) {
        INIFileSection sec = m_SectionGroupMercsTest.getSectionAt(idx);
        return ((INIFileString) sec.getEntry("Target")).getValue();
    }

    public String getMercsTestDescriptive(int idx) {
        INIFileSection sec = m_SectionGroupMercsTest.getSectionAt(idx);
        return ((INIFileString) sec.getEntry("Descriptive")).getValue();
    }

    public String getMercsTestOutputDest(int idx) {
        INIFileSection sec = m_SectionGroupMercsTest.getSectionAt(idx);
        String outputDest = ((INIFileString) sec.getEntry("Output")).getValue();
        if (outputDest == NONE) {                                               // If there is no outputlocation, we fix one ourselves
            outputDest = getMercsTestSet(idx);
            if (outputDest.endsWith(".arff")) {
                outputDest = outputDest.substring(0, outputDest.length() - 5);
            }
            outputDest += "_out";
        }
        return outputDest;
    }

    /*
     * =======================================
     * 4] INCREMENTAL
     * =======================================
     */

    /*
    * The actual variables and their encoding
    * */

    public final static String[] INCREMENTAL_DATA_SOURCE = { "MEMORY", "DISK" };
    public final static String[] INCREMENTAL_SPLIT_HEURISTIC = { "INCREMENTALVARIANCERATIO", "VARIANCERATIO",
            "VARIANCEDIFFERENCE" };

    public final static int INCREMENTAL_MEMORY_SOURCE = 0;
    public final static int INCREMENTAL_DISK_SOURCE = 1;

    public final static int INCREMENTAL_INCREMENTAL_VARIANCE_RATIO = 0;
    public final static int INCREMENTAL_VARIANCE_RATIO = 1;
    public final static int INCREMENTAL_VARIANCE_DIFFERENCE = 2;

    public final static double	INCREMENTAL_VFDT_DELTA_DEFAULT = 0.001;
    public final static int 	INCREMENTAL_NB_THREADS_DEFAULT = 1;
    public final static int		INCREMENTAL_NB_NUMERIC_DISTINCT_DEFAULT = 1000;
    public final static double	INCREMENTAL_NB_NUMERIC_DISTINCT_DROP_FACTOR_DEFAULT = 0.8;
    public final static int		INCREMENTAL_NB_BEFORE_TEST_DEFAULT = 100;
    public final static int		INCREMENTAL_NB_BEFORE_RETESTING_DEFAULT = 1;
    public final static double	INCREMENTAL_TIE_THRESHOLD_DEFAULT = 0.01;
    public final static double	INCREMENTAL_TEST_PERCENTAGE_DEFAULT = 0.1;
    public final static int		INCREMENTAL_ACTIVATION_SCAN_PERIOD_DEFAULT = 100000;
    public final static double	INCREMENTAL_MEMORY_TRIM_FACTOR_DEFAULT = 0.8;
    public final static int		INCREMENTAL_MAX_MEMORY_KB_DEFAULT = 1000000;
    public final static double	INCREMENTAL_MEMORY_DEACTIVATE_TRIM_PERCENT_DEFAULT = 0.1;
    public final static int		INCREMENTAL_MERCS_NEW_ITERATION_SAMPLES_AMOUNT_DEFAULT = 30000;
    public final static int		INCREMENTAL_MAX_DATA_PASSES_DEFAULT = 1;

    /*
    * The things collected from the settings file
    * */

    protected INIFileSection m_SectionIncremental;

    protected INIFileNominal m_IncrementalMethod;                       // The method that is used as incremental learner. Thus far only VFDT will be supported

    protected INIFileNominal m_IncrementalDataSource;                   // The source of data that is to be used, memory, disk, (generator in the future)

    protected INIFileNominal m_IncrementalSplitHeuristic;               // The split heuristic that is used, deciding when a split in a node should occur

    protected INIFileDouble m_IncrementalVFDTDelta;                     // The delta for VFDT, it assures that with 1-delta probability the best test is chosen at a split
    protected INIFileInt m_IncrementalNbThreads;                        // # threads used to process new samples
    protected INIFileInt m_IncrementalNbNumericDistinct;                // # distinct numeric values that have to be collected as potential split values in the nodes
    protected INIFileDouble m_IncrementalNbNumericDistinctDropFactor;   // The factor of how much the nbNumericDistinct drops each level in the tree
    protected INIFileInt m_IncrementalNbBeforeTest;                     // # arrived samples, before evaluating tests using the Hoeffding bound
    protected INIFileInt m_IncrementalNbBeforeRetesting;                // # newly arrived samples, before re-evaluating tests using the Hoeffding bound.
    protected INIFileDouble m_IncrementalTieThreshold;                  // The threshold when two tests are considered equally good
    protected INIFileDouble m_IncrementalTestPercentage;                // The percentage of the data that is used for performance measuring (test set)
    protected INIFileInt m_IncrementalActivationScanPeriod;             // # newly arrived samples, before scanning the vfdt nodes to de/re-activate (un)promising nodes.
    protected INIFileDouble m_IncrementalMemoryTrimFactor;              // Memory that has to be trimmed to when doing an activation scan
    protected INIFileInt m_IncrementalMaxMemoryKB;                      // The maximum amount of memory to be used in KB
    protected INIFileDouble m_IncrementalMemoryDeactivateTrimPercent;   // % of leaf nodes that are deactivated in each iteration if too much memory is used.
    protected INIFileInt m_IncrementalMercsNewIterationSamplesAmount;   // # samples learned in an iteration of the selection algo, i.e. the # samples seen before re-eval. of VFDT trees.
    protected INIFileInt m_IncrementalMaxDataPasses;                    // The amount of passes through the data allowed

    /**
     * Do we print incremental settings in the output files?
     * */
    public boolean isSectionIncrementalEnabled() {
        return m_SectionIncremental.isEnabled();
    }
    public void setSectionIncrementalEnabled(boolean value) {
        m_SectionIncremental.setEnabled(value);
    }
    public String getIncrementalSettingsInfo() {
        return getSettingsInfo(m_SectionIncremental);
    }


    public int getIncrementalMethod() {
        return m_IncrementalMethod.getValue();
    }
    public void setIncrementalMethod(String value) {
        m_IncrementalMethod.setValue(value);
    }
    public void setIncrementalMethod(int value) {
        m_IncrementalMethod.setSingleValue(value);
    }


    public int getDataSource() {
        return m_IncrementalDataSource.getValue();
    }
    public void setDataSource(String value) {
        m_IncrementalDataSource.setValue(value);
    }
    public void setDataSource(int value) {
        m_IncrementalDataSource.setSingleValue(value);
    }


    public int getIncrementalSplitHeuristic() {
        return m_IncrementalSplitHeuristic.getValue();
    }
    public void setIncrementalSplitHeuristic(int value) {
        m_IncrementalSplitHeuristic.setSingleValue(value);
    }
    public void setIncrementalSplitHeuristic(String value) {
        m_IncrementalSplitHeuristic.setValue(value);
    }


    public double getIncrementalVFDTDelta() {
        return m_IncrementalVFDTDelta.getValue();
    }
    public void setIncrementalVFDTDelta(double value) {
        m_IncrementalVFDTDelta.setValue(value);
    }

    public int getIncrementalNbThreads() {
        int value = m_IncrementalNbThreads.getValue();
        return Math.max(1, value);
    }
    public void setIncrementalNbThreads(int threads) {
        m_IncrementalNbThreads.setValue(threads);
    }

    public int getIncrementalNbNumericDistinct() {
        return m_IncrementalNbNumericDistinct.getValue();
    }
    public void setIncrementalNbNumericDistinct(int value) {
        m_IncrementalNbNumericDistinct.setValue(value);
    }

    public double getIncrementalNbNumericDistinctDropFactor() {
        double value = m_IncrementalNbNumericDistinctDropFactor.getValue();
        return Math.max(0.0, Math.min(1.0, value));
    }
    public void setIncrementalNbNumericDistinctDropFactor(double value) {
        m_IncrementalNbNumericDistinctDropFactor.setValue(value);
    }

    public int getIncrementalNbBeforeTest() {
        int value = m_IncrementalNbBeforeTest.getValue();
        return Math.max(1, value);
    }
    public void setIncrementalNbBeforeTest(int value) {
        m_IncrementalNbBeforeTest.setValue(value);
    }

    public int getIncrementalNbBeforeRetesting() {
        int value = m_IncrementalNbBeforeRetesting.getValue();
        return Math.max(1, value);
    }
    public void setIncrementalNbBeforeRetesting(int value) {
        m_IncrementalNbBeforeRetesting.setValue(value);
    }

    public double getIncrementalTieThreshold() {
        double value = m_IncrementalTieThreshold.getValue();
        return Math.max(MathUtil.C1E_9, value);
    }
    public void setIncrementalTieThreshold(double value) {
        m_IncrementalTieThreshold.setValue(value);
    }

    public double getIncrementalTestPercentage() {
        double value = m_IncrementalTestPercentage.getValue();
        return Math.max(0.01, Math.min(0.99, value));
    }
    public void setIncrementalTestPercentage(double value) {
        m_IncrementalTestPercentage.setValue(value);
    }

    public int getIncrementalActivationScanPeriod() {
        int value = m_IncrementalActivationScanPeriod.getValue();
        return Math.max(1, value);
    }
    public void setIncrementalActivationScanPeriod(int value) {
        m_IncrementalActivationScanPeriod.setValue(value);
    }

    public double getIncrementalMemoryTrimFactor() {
        double value = m_IncrementalMemoryTrimFactor.getValue();
        return Math.max(0, value);
    }
    public void setIncrementalMemoryTrimFactor(double value) {
        m_IncrementalMemoryTrimFactor.setValue(value);
    }

    public int getIncrementalMaxMemoryKB() {
        int value = m_IncrementalMaxMemoryKB.getValue();
        return Math.max(0, value);
    }
    public void setIncrementalMaxMemoryKB(int value) {
        m_IncrementalMaxMemoryKB.setValue(value);
    }

    public double getIncrementalMemoryDeactivateTrimPercent() {
        double value =  m_IncrementalMemoryDeactivateTrimPercent.getValue();
        return Math.min(100, Math.max(1, value));
    }
    public void setIncrementalMemoryDeactivateTrimPercent(double value) {
        m_IncrementalMemoryDeactivateTrimPercent.setValue(value);
    }

    public int getIncrementalMercsNewIterationSamplesAmount() {
        int value = m_IncrementalMercsNewIterationSamplesAmount.getValue();
        return Math.max(1, value);
    }
    public void setIncrementalMercsNewIterationSamplesAmount(int value) {
        m_IncrementalMercsNewIterationSamplesAmount.setValue(value);
    }

    public int getIncrementalMaxDataPasses() {
        int value = m_IncrementalMaxDataPasses.getValue();
        return Math.max(1, value);
    }
    public void setIncrementalMaxDataPasses(int value) {
        m_IncrementalMaxDataPasses.setValue(value);
    }

    /*
     * =======================================
     * EXTRA FUNCTIONS
     * =======================================
     */

    /**
     * Creates the settings tree structure by invoking the settings of Clus and
     * extending it afterwards with the Mercs settings
     *
     * TODO: The other settings (incremental-clustering) also have to be added here
     */
    @Override
    public void create() {
        // Clus
        super.create();

        // Adding a node to the Data section
        INIFileSection dataNode = (INIFileSection) m_Ini.getNode("Data");
        dataNode.addNode(m_DataMaxSamples = new INIFileDouble("DataMaxSamples", DATA_MAX_SAMPLES_DEFAULT));

        // The Mercs-section
        m_SectionMercs = new INIFileSection("Mercs");
        m_SectionMercs.addNode(m_MercsTestSet = new INIFileString("MercsTestSet"));
        m_SectionMercs.addNode(m_MaxSelectionIterations = new INIFileInt("MaxSelectionIterations", 1));
        m_SectionMercs.addNode(
                m_MercsPredictionMethod = new INIFileNominal("MercsPredictionMethod", MERCS_PREDICTION_METHOD));
        m_SectionMercs.addNode(
                m_MercsSelectionMethod = new INIFileNominal("MercsSelectionMethod", MERCS_SELECTION_METHOD));
        m_SectionMercs.addNode(
                m_MercsSelectionMethod = new INIFileNominal("MercsSelectionMethod", MERCS_SELECTION_METHOD));
        m_SectionMercs.addNode(m_MercsPredictionWeighted = new INIFileBool("PredictWeighted", false));
        m_SectionMercs.addNode(m_MercsSelectionAccuracyMeasure = new INIFileNominal("MercsSelectionAccuracyMeasure",
                MERCS_SELECTION_ACCURACY_MEASURE, MERCS_SELECTION_ACCURACY_MEASURE_LABEL));
        m_SectionMercs.addNode(m_MercsBaseType = new INIFileNominal("MercsBaseType", MERCS_BASE_TYPE));
        m_SectionMercs.addNode(m_StoreMercsModel = new INIFileBool("StoreMercsModel"));
        m_SectionMercs.addNode(m_MercsModelStorage = new INIFileString("MercsModelStorage"));
        m_SectionMercs.setEnabled(true);

        // MercsTest is a section group, with a single section for each mercs.test instance.
        // The structure of these instances are given by the prototype created below.
        m_SectionGroupMercsTest = new INIFileSectionGroup("MercsTest");                             // New section group
        INIFileSection m_SectionMercsTestsPrototype = new INIFileSection("MercsTestPrototype");     // New section (the 'prototype') + adding nodes to this section
        m_SectionMercsTestsPrototype.addNode(new INIFileString("TestSet", NONE));
        m_SectionMercsTestsPrototype.addNode(new INIFileString("Target", DEFAULT));
        m_SectionMercsTestsPrototype.addNode(new INIFileString("Descriptive", DEFAULT));
        m_SectionMercsTestsPrototype.addNode(new INIFileString("Output", NONE));
        m_SectionGroupMercsTest.setPrototype(m_SectionMercsTestsPrototype);                         // Set the prototype in the section group
        m_SectionGroupMercsTest.setEnabled(false);                                                  // We disable the group for now?

        // Finally, we add our sections to m_Ini (again, as nodes)
        m_Ini.addNode(m_SectionMercs);
        m_Ini.addNode(m_SectionGroupMercsTest);
    }

    public static String getSettingsInfo(INIFileSection masterSection) {
        StringBuilder info = new StringBuilder(masterSection.getName() + "\n=====================\n");

        @SuppressWarnings("unchecked")
        Enumeration<INIFileEntry> sections = masterSection.getNodes();
        INIFileEntry section;
        while (sections.hasMoreElements()) {
            section = sections.nextElement();
            info.append(section.getName()).append(" = ").append(section.getStringValue()).append("\n");
        }

        return info.toString();
    }

}
