package mercs.core;

import clus.data.io.ARFFFile;
import clus.data.io.ClusReader;
import clus.data.io.ClusView;
import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import clus.error.ClusErrorList;
import clus.error.MisclassificationError;
import clus.error.RMSError;
import clus.main.Settings;
import clus.util.ClusException;
import clus.util.ClusRandom;

import mercs.data.MercsData;
import mercs.model.EnsembleModel;
import mercs.model.MercsModel;

// These imports are unnecessary at the moment

// import mercs.algo.selection.EnsemblePerformance;
// import mercs.algo.clustering.ClusterBuilder;
// import mercs.model.ClusteringModel;

import jeans.resource.ResourceInfo;
import jeans.util.cmdline.CMDLineArgs;
import jeans.util.cmdline.CMDLineArgsProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;

/**
 * Main Class of the MERCS-system.
 *
 */

public class Mercs implements CMDLineArgsProvider {

    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set) START HERE
     * =======================================
     */

    // These parameters come from the commandline
    private final static int NB_MAIN_ARGS = 1;
    private final static String[] OPTIONAL_ARGS = {"silent"};
    private final static int[] OPTIONAL_ARITIES = {0};

    /**
     * Contains the settings that can be read from a settings file (filename.s). Based on
     * these settings the appropriate algorithms will run.
     */
    private SettingsMercs settings;
    /**
     * The mercsRun of this instantiation of Mercs. It functions as a compact
     * wrapper of the view, reader, schema and data. It is used to pass along
     * this information in a compact way.
     */
    private MercsRun mercsRun;
    /**
     * A Clusschema that contains the different attributes of the data. It
     * describes the different attributes (nominal, numeric, ...) and their
     * function (target, clustering, descriptive)
     */
    private ClusSchema schema;
    /**
     * A reader that is able to read from an arrf file containing the data
     * relations and data.
     */
    private ClusReader reader;
    /**
     * A view that interprets a ClusReader and can make data tuples from it.
     */
    private ClusView view;

    /**
     * Returns the optional parameters that can be used in the console
     */
    @Override
    public String[] getOptionArgs() {
        return OPTIONAL_ARGS;
    }
    /**
     * Returns the arities of the different optional parameters that can be used
     * in the console. There is a 1:1 mapping of optional parameters and the
     * arities
     */
    @Override
    public int[] getOptionArgArities() {
        return OPTIONAL_ARITIES;
    }
    /**
     * Returns the number of main arguments that must be passed to this main
     * class
     */
    @Override
    public int getNbMainArgs() {
        return NB_MAIN_ARGS;
    }


    /**
     * Returns the settings that were loaded from a settings file
     */
    public SettingsMercs getSettings() {
        return settings;
    }
    /**
     * Sets the settings.
     */
    private void setSettings(SettingsMercs settings) throws IllegalArgumentException {
        this.settings = settings;
    }

    /**
     * Returns the mercsRun
     */
    public MercsRun getMercsRun() {
        return mercsRun;
    }
    /**
     * Sets the mercsRun
     */
    private void setMercsRun(MercsRun mercsRun) {
        this.mercsRun = mercsRun;
    }
    
    /**
     * Returns the schema of the data where will be learned on
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema
     */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }

    /**
     * Returns the reader
     */
    public ClusReader getReader() {
        return reader;
    }
    /**
     * Returns the reader
     */
    private void setReader(ClusReader reader) {
        this.reader = reader;
    }

    /**
     * Returns the view
     */
    public ClusView getView() {
        return view;
    }
    /**
     * Sets the view
     */
    private void setView(ClusView view) {
        this.view = view;
    }

    /**
     * =======================================
     * METHODS START HERE
     * =======================================
     */

    /**
     * Main Method.
     * First create a Mercs object, process cmdl arguments and give them to the program.
     * The the Mercs.initialize() is called, which fixes schema, settings and reader.
     * After that, we can start a MercsRun.
     */
    public static void main(String[] args) {
        try {
            // Make a new Mercs object
            Mercs mercs = new Mercs();
            // Runtime.getRuntime().loadLibrary("ResourceInfo");

            CMDLineArgs cargs = new CMDLineArgs(mercs);             // Deal with the cmdline arguments using a library

            cargs.process(args);                                    // Process the arguments and store them in cargs.
            if (!cargs.allOK()) {
                mercs.showHelp();
                System.out.println();
                System.out.println("Expected main argument, instead found: " + Arrays.toString(args));
                System.exit(0);
            }



            mercs.setSettings(new SettingsMercs());                             // Initialize and fill the settings of this Mercs object

            mercs.getSettings().setDate(new Date());                            // Set the start date to the current time
            mercs.getSettings().setAppName(cargs.getMainArg(0));                // Set the appName to the first command line argument
            mercs.getSettings().initialize(cargs, true);                        // Initialize the basic settings structure tree

            mercs.getSettings().VERBOSE = mercs.getSettings().getVerbose();     // Clus works with two verbosity values (Verbose and m_Verbose), I want them all to be user-defined

            // The Mercs initialize() method. Fixes schema, reader, settings and initializes a MercsRun
            mercs.initialize();
            System.out.println("Done initialize");

            // Start the MercsRun (only possible after initialization!). This means building an EnsembleModel
            //mercs.startRun();
            //System.out.println("Done startRun");

            // Tests can go here
            System.out.println("==========================================");
            System.out.println("          Testing starts here             ");
            System.out.println("==========================================");
            //Tests.multipleMercsTDIDTRuns(mercs,1);
            //Tests.multipleMercsPrediction(mercs, 1);
            Tests.mercsPrediction(mercs);

        } catch (IOException | ClusException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialize
     * Create a ClusSchema (Contains information for the settings)
     * Create a ClusReader (to fetch the data)
     * Get the ARFF-file
     * Fix the MercsSettings based on the ClusSchema
     *
     * This method does not yet load the data into memory as this does not need to happen with every
     * algorithm (see Incremental learners).
     *
     * Start the statistics
     * Set ClusView
     * Set a MercsRun
     *
     */
    private void initialize() throws IOException, ClusException {
        // Load resource info (this measures among others CPU time on Linux)
        boolean test = settings.getResourceInfoLoaded() == Settings.RESOURCE_INFO_LOAD_TEST;

        ResourceInfo.loadLibrary(test);

        // Initialisation random engine to be able to reproduce runs
        ClusRandom.initialize(settings);

        // Tell the user the settings file is being loaded
        if (settings.getVerbose() > 0) {
            System.out.println("Loading '" + settings.getAppName() + "'");
        }


        // Create reader for data file
        setReader(new ClusReader(settings.getDataFile(), settings));
        if (settings.getVerbose() > 0) {
            System.out.println("File set for Reader: '" + reader.getName() + "'");
        }

        System.out.println("Reading ARFF Header");
        ARFFFile arff = new ARFFFile(reader);           // Read ARFF header (=use for settings) of data file (but leaves data segment untouched)
        setSchema(arff.read(settings));                 // This header is what constitutes the schema.
        if (settings.getVerbose() > 0) {
            System.out.println("ClusSchema set according to settings file");
        }

        // All the useful information from the schema goes into the settings
        settings.updateTarget(schema);
        schema.initializeSettings(settings);
        settings.setTarget(schema.getTarget().toString());
        settings.setDescriptive(schema.getDescriptive().toString());
        settings.setDisabled(schema.getDisabled().toString());
        settings.setClustering(schema.getClustering().toString());

        // Initialize statistics
        MercsRunStatistics.setInitialMemory(MercsRunStatistics.getCurrentMemory());
        MercsRunStatistics.setBeginTime(ResourceInfo.getTime());

        // setView + print info (bytes/tuple message)
        setView(schema.createNormalView());
        schema.printInfo();

        // set a MercsRun for this Mercs object
        setMercsRun(new MercsRun(this));
    }

    /**
     * startRun.
     * Create EnsembleModel (Using ModelBuilder)
     * Figure out whether or not it should be stored (and where, using modelStorage string)
     *
     * TODO: This is a version that does not include Clustering yet!
     */
    private void startRun() throws IOException, ClusException {
        // Local variables. An EnsembleModel and modelStorage.
        EnsembleModel ensembleModel;
        String modelStorage = getSettings().getMercsModelStorage();

        if (getSettings().shouldModelBeStored()) {											    // IF the model has to be stored
            if(!modelStorage.isEmpty()) {													    // and IF storage is not empty (~we know where to save)
                ensembleModel = ModelBuilder.buildAndStoreMercsModel(this, modelStorage);	    // We build AND store an ensembleModel using ModelBuilder class
            } else {																		    // when storage IS empty (~we do not know where to save)
                System.out.println("No MERCS model storage specified. " + "The generated model will not be saved to disk.");
                ensembleModel = ModelBuilder.buildMercsModel(this);							    // Just build a model
            }
        } else {																			// IF the model does NOT have to be stored
            ensembleModel = ModelBuilder.buildMercsModel(this);								// Just build the model
        }

    }

    /**
     * Yields ClusErrorList using
     * the RMSError for numeric targets and
     * MisclassificationError for nominal targets
     * on the supplied model with the given data.
     *
     * This method uses alongside its two obvious params also an internal ClusErrorList to keep track of the errors.
     *
     * @param data MercsData, used to evaluate the performance of the model
     * @param model MercsModel the model to be evaluated
     *
     *
     */
    public static ClusErrorList getModelErrorOnData(MercsModel model, MercsData data)
            throws IOException, ClusException
    {
        // Relevant variables for the task: a ClusErrorList object and a MercsData object to check.
        ClusErrorList errorList = new ClusErrorList();
        data.init();

        if (model.getSchema().getNumericAttrUse(ClusAttrType.ATTR_USE_TARGET).length > 0) {
            RMSError er1 = new RMSError(errorList, model.getSchema().getNumericAttrUse(ClusAttrType.ATTR_USE_TARGET));  // Using the RMSEError class, this figures out everything
            errorList.addError(er1);
        }

        if (model.getSchema().getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET).length > 0) {
            MisclassificationError er2 = new MisclassificationError(errorList,
                    model.getSchema().getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET));                                 // Using the MisclassificationError class, this figures out everything
            errorList.addError(er2);
        }

        // Run over all the tuples, and take note of what the model does.
        DataTuple sample;                                           // DataTuple is a class dedicated to host a single tuple
        while (data.hasNextTuple()) {                               // MercsData class has some convenient methods, such as hasNextTuple, readNextTuple (which fetches a single one)
            sample = data.readNextTuple();
            sample.setSchema(model.getSchema());
            errorList.addExample(sample, model.predict(sample));    // Adding example/example to the ClusErrorList object. Note that MercsModel has a 'predict' method
        }

        return errorList;
    }

    /**
     * Given a MercsModel, we print it to a file (using PrintWriter)
     */
    public static void writeModelToFile(MercsModel model, String filename) throws IOException {
        File outputFile = new File(filename);
        model.printModel(new PrintWriter(new FileOutputStream(outputFile)));
    }

    /**
     * Returns the help. This contains the main arguments and a short
     * explanation. Furthermore it contains the optional parameters, their
     * arities and their explanation
     */
    @Override
    public void showHelp() {
        // TODO Show help text in console
    }

    /**
     * Finalizes the statistics, about memory usage, time,...
     */
    void finalizeStatistics() {
        System.gc();
        MercsRunStatistics.setEndMemory(MercsRunStatistics.getCurrentMemory());
        MercsRunStatistics.setEndTime(ResourceInfo.getTime());
    }

}