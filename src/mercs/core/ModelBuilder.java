package mercs.core;

import clus.util.ClusException;
import mercs.algo.induce.MercsInductionAlgorithm;
import mercs.algo.type.MultiDirectionalEnsembleClassifier;
import mercs.io.MercsModelSerializer;
import mercs.model.EnsembleModel;

import java.io.IOException;

/**
 *
 */
public class ModelBuilder {

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * buildMercsModel
     *
     * Builds a EnsembleModel, but does not save it.
     * Needs a Mercs object (=the general class) as input.
     *
     * 1) Creates a classifier (MultiDirectionalEnsembleClassifier)
     * 2) Build an inducer (MercsInductionAlgorithm, but in practice a MercsInducer, we are operating at the Mercs level)
     * 3) Build the actual model (EnsembleModel)
     *
         The type-package takes care of this by its dual structure:

                                        MercsInductionAlgorithmType                                 [algo/type]
                                        |                       |
                        MercsDecisionTreeClassifier         MultiDirectionalEnsembleClassifier      [algo/type] (one of these two gets returned by getBaseType)
                                    creates                                 creates
                        DepthFirstInduceDecisionTreeWrapper              MercsInducer               [algo/induce]
     *
     * @param mercs
     * @return EnsembleModel model
     * @throws IOException
     * @throws ClusException
     */
    public static EnsembleModel buildMercsModel(Mercs mercs) throws IOException, ClusException {
        // We explicitly create a classifier at the Mercs-level (the ensemble-level)
        MultiDirectionalEnsembleClassifier clss = new MultiDirectionalEnsembleClassifier();

        // This MultiDirectionalEnsembleClassifier then creates a MercsInducer (the move to the algo/induce package)
        MercsInductionAlgorithm inducer = clss.createInduce(mercs.getSchema(), mercs.getSettings());

        // Applying this induction, is what yields our actual ensembleModel. This implies induction of all the individual models
        EnsembleModel model = (EnsembleModel)inducer.induce(mercs.getMercsRun());

        return model;
    }

    /**
     * buildAndStoreMercsModel
     *
     * Builds a EnsembleModel, and saves it too.
     * Needs a Mercs object (=the general class) as input.
     *
     * 1) Builds a mercsmodel (EnsembleModel)
     * 2) Saves it in the specified location
     * 3) Also returns the model
     *
     * @param mercs
     * @param file
     * @return EnsembleModel model
     * @throws IOException
     * @throws ClusException
     */
    public static EnsembleModel buildAndStoreMercsModel(Mercs mercs, String file) throws IOException, ClusException {
        // Build the model
        EnsembleModel multiModel = buildMercsModel(mercs);

        // Write to file
        MercsModelSerializer.writeModelToFile(multiModel, file);

        return multiModel;
    }

    /**
     * loadMercsModel
     *
     * Loads a mercsmodel (EnsembleModel) from a file.
     * Uses class MercsModelSerializer to do so.
     *
     */
    public static EnsembleModel loadMercsModel(String file) {
        return MercsModelSerializer.readModelFromFile(file);
    }

}
