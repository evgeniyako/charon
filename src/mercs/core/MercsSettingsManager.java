package mercs.core;

import clus.Clus;
import clus.data.attweights.ClusAttributeWeights;
import clus.data.io.ARFFFile;
import clus.data.io.ClusReader;
import clus.data.rows.RowData;
import clus.data.type.ClusSchema;
import clus.heuristic.ClusHeuristic;
import clus.heuristic.VarianceReductionHeuristic;
import clus.main.Settings;
import clus.selection.RandomSelection;
import clus.statistic.ClusStatistic;
import clus.util.ClusException;

import jeans.util.IntervalCollection;

import mercs.algo.prediction.*;
import mercs.algo.selection.*;
import mercs.algo.type.MercsDecisionTreeClassifier;
import mercs.algo.type.MercsInductionAlgorithmType;

import mercs.data.MemoryStreamBuilder;
import mercs.data.MercsData;
import mercs.data.StreamBuilder;
import mercs.util.MercsException;

/* Everything incremental
import mercs.error.DummyStopCriterion;
import mercs.split.heuristic.IncrementalRatioSplitHeuristic;
import mercs.split.heuristic.RatioSplitHeuristic;
import mercs.split.heuristic.SplitHeuristic;
import mercs.split.heuristic.VarianceReductionSplitHeuristic;
import mercs.data.DiskStreamBuilder;
import mercs.algo.type.IncrementalAlgorithmClassifier;
*/

import org.javatuples.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * MercsSettingsManager
 *
 * A settings manager that based on the given settings, initialises the settings
 * of algorithms like heuristics, stopping criteria, pruning method, prediction
 * algorithm, selection algorithm, ...
 *
 * The main goal of this class is to have a
 * single place where code should be adapted when new settings become available.
 *
 * NB.: TODO: Incremental settings currently omitted here
 */

public class MercsSettingsManager {
    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    private ClusSchema schema;
    private SettingsMercs settings;

    /**
     * Returns the schema of the manager
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema of the manager
     */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Checks whether the given schema is valid. It is valid if and only if the
     * given schema is not equal to null.
     */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }

    /**
     * Returns the settings of the manager
     */
    public SettingsMercs getSettings() {
        return settings;
    }
    /**
     * Sets the settings of the manager
     */
    private void setSettings(SettingsMercs settings) {
        this.settings = settings;
    }
    /**
     * Checks whether the given settings is valid. It is valid if and only if
     * the given settings is not equal to null.
     */
    public boolean isValidSettings(SettingsMercs settings) {
        return settings != null;
    }


    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initialises the settings manager with the given schema and settings.
     * Throws an exception when the given schema or settings are invalid.
     */
    public MercsSettingsManager(ClusSchema schema, SettingsMercs settings) throws MercsException {
        if (!isValidSchema(schema) || !isValidSettings(settings)) {
            throw new MercsException("Schema or settings is invalid");
        }

        setSchema(schema);
        setSettings(settings);
    }

   /*
    * General data handling
    */

    /**
     * Make the entire dataset available in the MercsRun in the data field of that class.
     * @param mr the MercsRun for which the data is to be loaded
     */
    public void setDataInMercsRun(MercsRun mr) throws IOException, ClusException {
        if(!mr.getReader().hasMoreTokens()) {
            resetData(mr);
        }

        RowData rowData = mr.getView().readData(mr.getReader(), mr.getSchema());                    // Read all data, using methods of the MercsRun itself
        if (mr.getSchema().getSettings().getNormalizeData() == Settings.NORMALIZE_DATA_NUMERIC) {   // Normalize if we have to
            rowData = Clus.returnNormalizedData(rowData);
        }

        MemoryStreamBuilder builder = new MemoryStreamBuilder();                // Make MemoryStreamBuilder
        builder = builder.setMercsRun(mr);                                      // Link it to the MercsRun (set its MercsRun property)
        builder = builder.setData(rowData);                                     // Set the data we just created (=RowData object) as a property of the MemoryStreamBuilder object
        /*
        * Build a MemoryStream with the builder
        * The outcome of this (MemoryStream) is then given as an input to the
        * setData method of the MercsRun object,
        * so in this two-step way, we actually provide the MercsRun with actual data.
        *
        * NB.: MemoryStream extends the more general MercsData class
        */
        mr.setData(builder.build());
    }

    /**
     * Reset the data in the provided MercsRun
     * @param mr The MercsRun for which to reset the data
     */
    public void resetData(MercsRun mr) throws IOException, ClusException {
        // Reset reader
        mr.getReader().reOpen();
        // Skip to data part
        ARFFFile arff = new ARFFFile(mr.getReader());
        arff.read(getSettings());
    }

    /**
     * Create a train and prune set from the reader in the mercs run, afterwards
     * initialize these values in the mercsrun
     */
    public void createPruneSetAndTrainSetInMercsRun(MercsRun mr) throws IOException, ClusException {
        RowData rowData = mr.getView().readData(mr.getReader(), mr.getSchema());            // Read all data, using methods of the MercsRun (and ClusView) itself
        Pair<RowData, RowData> trainAndPrune = makeTrainAndPruneSelection(rowData);         // Make seperate into train and prune (with helper method of this class)

        // Normalize if have to
        RowData trainSet, pruneSet;
        if (mr.getSchema().getSettings().getNormalizeData() == Settings.NORMALIZE_DATA_NUMERIC) {
            trainSet = Clus.returnNormalizedData(trainAndPrune.getValue0());
            pruneSet = Clus.returnNormalizedData(trainAndPrune.getValue1());
        } else {
            trainSet = trainAndPrune.getValue0();
            pruneSet = trainAndPrune.getValue1();
        }

        // Make stream from train set and set this Stream as the data in the MercsRun
        MemoryStreamBuilder builder = new MemoryStreamBuilder();
        builder = builder.setMercsRun(mr);
        builder = builder.setData(trainSet);
        mr.setData(builder.build());

        // Make stream from prune set and set this Stream as the data in the MercsRun
        builder = new MemoryStreamBuilder();
        builder = builder.setMercsRun(mr);
        builder = builder.setData(pruneSet);
        mr.setPruneData(builder.build());
    }

    /**
     * Helper method that is used in the createPruneSetAndTrainSetInMercsRun method
     *
     * This method returns a Pair<RowData RowData> object that contains the train and prune data,
     * based on its input of just one RowData object that contains all the input data in one RowData object.
     *
     * @param rowData The full input data.
     * @return Pair<>(rowData, pruneData);  The training set is the full input, the prune set is a subset.
     *
     * */
    private Pair<RowData, RowData> makeTrainAndPruneSelection(RowData rowData) {
        double vsb = getSettings().getPruneProportion();                // We fetch the prune proportion from the settings file
        int nbtot = rowData.getNbRows();                                // Total number of rows
        int nbsel = (int) Math.round(vsb * nbtot);                      // Number of rows we should use for the prune set
        if (nbsel > getSettings().getPruneSetMax())                     // If, by following the proportion, we exceed an absolute (fixed) max value, we stick with the max value
            nbsel = getSettings().getPruneSetMax();
        RandomSelection prunesel = new RandomSelection(nbtot, nbsel);   // We select the correct amount of rows randomly (Clus has a method for this)
        RowData pruneData = (RowData) rowData.select(prunesel);         // We then select the data that belongs to these selected rows

        return new Pair<>(rowData, pruneData);                          // Our train set is just the complete input, our prune set is a subset
    }

   /*
    * Everything Test (data)
    *
    * Focus on actually returning the test data as a MercsData object.
    * The relevant information is usually present in the settings file.
    *
    */

    /**
     * If a test file has been supplied in the settings, this file is returned
     * as a MemoryStream (this is used for both non-incremental as well as incremental learning).
     *
     * If no test file has been supplied, this method throws a MercsException
     */
    public MercsData getTestData(MercsRun mr) throws IOException, ClusException {
        ClusReader reader = new ClusReader(getSettings().getTestFile(), getSettings());             // Create a reader (~ClusReader object) to read the testdata (location testdata in settings file)
        new ARFFFile(reader).read(getSettings());

        MemoryStreamBuilder builder = new MemoryStreamBuilder();                                    // Create a builder (MemoryStreamBuilder) for the MemoryStream object we want to create
        builder = builder.setMercsRun(mr);                                                          // The builder needs the MercsRun

        RowData testData = mr.getView().readData(reader, getSchema());								// Getting the data, starting from the mercsrun
        if (mr.getSchema().getSettings().getNormalizeData() == Settings.NORMALIZE_DATA_NUMERIC) {   // Normalize if we have to
            testData = Clus.returnNormalizedData(testData);
        }
        builder = builder.setData(testData);														// Setting the data in the builder
        reader.close();                                                                             // Close the reader

        return builder.build();                                                                     // Return the MercsData (MemoryStream is child) object that we wanted to build
    }

    /**
     * getMercsTestData(MercsRun mr, String testSet) -> The actual method that gets the data
     *
     * If a *Mercs* test file has been supplied in the settings, this file is
     * returned as a MemoryStream.
     *
     * If no test file has been supplied, this method throws a MercsException.
     *
     * @param mr This is a MercsRun, from which we fetch getSettings -> getMercsTestSet
     * @param testSet A string that actually identifies which testSet we will use
     *
     * All the other getMercsTestData methods rely on getSettings().getMercsTestSet() to fetch the testSet,
     *                but this actually returns a string which serves as identifier.
     *                Then the other methods just call this one.
     *
     * Remark: This test set should only be used for
     * evaluating a *Mercs model* and not for evaluating single trees during
     * learning.
     *
     * Remark: This method is completely analogue to the non-Mercs fetching of testData, as shown in the method getTestData.
     *
     */
    public MercsData getMercsTestData(MercsRun mr, String testSet) throws IOException, ClusException {
        ClusReader reader = new ClusReader(testSet, getSettings());
        new ARFFFile(reader).read(getSettings());
        MemoryStreamBuilder builder = new MemoryStreamBuilder();
        builder = builder.setMercsRun(mr);

        RowData testData = mr.getView().readData(reader, getSchema());								// Getting the data
        if (mr.getSchema().getSettings().getNormalizeData() == Settings.NORMALIZE_DATA_NUMERIC) {
            testData = Clus.returnNormalizedData(testData);
        }
        builder = builder.setData(testData);														// Giving the data to the builder
        reader.close();

        return builder.build();
    }
    /**
     * getMercsTestData(MercsRun mr) -> Single test set in mercsrun
     *
     * If a *Mercs* test file has been supplied in the settings, this file is
     * returned as a MemoryStream.
     *
     * If no test file has been supplied, this method throws a MercsException.
     *
     * @param mr This is a MercsRun, from which we fetch getSettings -> getMercsTestSet
     *
     * Remark: This test set should only be used for
     * evaluating a *Mercs model* and not for evaluating single trees during
     * learning.
     */
    public MercsData getMercsTestData(MercsRun mr) throws IOException, ClusException {
        return getMercsTestData(mr, getSettings().getMercsTestSet());
    }
    /**
     * getMercsTestData(MercsRun mr, int testIdx) > Multiple test sets in mercsrun
     *
     * If a *Mercs* test file has been supplied in the settings, this file is
     * returned as a MemoryStream.
     *
     * If no test file has been supplied, this method throws a MercsException.
     *
     * @param mr This is a MercsRun, from which we fetch getSettings -> getMercsTestSet
     * @param testIdx Gives the index of the TestSet, so that the method knows which one to choose.
     *
     * Remark: This test set should only be used for
     * evaluating a *Mercs model* and not for evaluating single trees during
     * learning.
     */
    public MercsData getMercsTestData(MercsRun mr, int testIdx) throws IOException, ClusException {
        String testSet = getSettings().getMercsTestSet(testIdx);                    // NB.: A string is the outcome of getMercsTestSet()
        return getMercsTestData(mr, testSet);
    }

    /**
     * The MercsTest section group allows one to specify many different test
     * specifications. This method returns how many (if any) tests are specified.
     */
    public int getNbMercsTests(){
        return getSettings().getNbMercsTests();
    }

    /**
     * Method that returns the ClusSchema of the test we want to run.
     * The schema, as always, gives us relevant information about the problem we actually want to solve.
     *
     * @param idx Index of the test from which we want all the information (~ClusSchema)
     * */
    public ClusSchema getMercsTestSchema(int idx) throws ClusException, IOException {
        // Collect the correct strings from the settings, using the index of the test
        String name = getSettings().getMercsTestName(idx);
        String descriptive = getSettings().getMercsTestDescriptive(idx);
        String target = getSettings().getMercsTestTarget(idx);

        // Using these strings to fix the schema (which is always done this way)
        ClusSchema schema = getSchema().cloneSchema();
        schema.setRelationName(name);
        schema.setDescriptive(new IntervalCollection(descriptive));
        schema.setTarget(new IntervalCollection(target));
        schema.setClustering(new IntervalCollection(target));
        schema.updateAttributeUse();
        schema.addIndices(ClusSchema.ROWS);

        return schema;
    }

    /**
     * Print the info about the MercsTests in the terminal
     * */
    public void printMercsTestInfo(int idx) {
        System.out.format("\nTest %d: Name: \"%s\""
                        + "\n\tTestset: %s"
                        + "\n\tRelation: %s -> %s"
                        + "\n", idx,
                getSettings().getMercsTestName(idx),
                getSettings().getMercsTestSet(idx),
                getSettings().getMercsTestDescriptive(idx),
                getSettings().getMercsTestTarget(idx));
    }

   /*
    * Settings related to actual induction of the Mercs system. (Selection + Induction)
    *
    * Methods here provide access to the correct selection method and type of inducer, based on the available information in the settings.
    * Methods actually return the kind of selection algorithm (algo/selection/SelectionAlgorithm)
    * and the baseType (~algo/type/MercsInductionAlgorithmType) of the inducer that we should use
    */

    /**
     * Returns the right selection algorithm (~SelectionAlgorithm) that should be used when inducing a
     * Mercs model
     *
     * It is with this method that the actual selection of the different options is in fact made
     */
    public SelectionAlgorithm getSelectionAlgorithm() throws MercsException {
        SelectionAlgorithm selectionAlgorithm;

        switch(getSettings().getMercsSelectionMethod()) {
            case SettingsMercs.MERCS_SELECTION_TYPE_NAIVE:
                selectionAlgorithm = new NaiveSelectionAlgorithm(getSchema());
                break;
            case SettingsMercs.MERCS_SELECTION_TYPE_BASE:
                selectionAlgorithm = new BaseSelectionAlgorithm(getSchema());
                break;
            case SettingsMercs.MERCS_SELECTION_TYPE_RANDOM:
                selectionAlgorithm = new RandomSelectionAlgorithm(getSchema());
                break;
            case SettingsMercs.MERCS_SELECTION_TYPE_ITERATE:
                selectionAlgorithm = new IterateRandomSelectionAlgorithm(getSchema(),
                        this, getSettings().getMaxSelectionIterations());
                break;
            // Extend with more if more selection algorithms become available
            default:
                throw new MercsException("No selection algorithm has been set");
        }

        return selectionAlgorithm;
    }

    /**
     * Returns the right base type classifier (algo/type) that should be used when inducing
     * a Mercs model.
     *
     * This method is used in MercsInducer to ensure that we end up in the individual branch of
     * algo/induce. So, quite surprisingly, this method is actually of vital importance in our system
     */
    public MercsInductionAlgorithmType getBaseType() throws MercsException {
        MercsInductionAlgorithmType baseType;

        switch(getSettings().getMercsBaseType()) {
            case SettingsMercs.MERCS_BASE_TYPE_TDIT:
                baseType = new MercsDecisionTreeClassifier();
                break;
            // TODO: Add the incremental option
            //case SettingsMercs.MERCS_BASE_TYPE_VFDT:
            //    baseType = new IncrementalAlgorithmClassifier();
            //    break;
            // Extend with more if more base types become available
            default:
                throw new MercsException("No base type has been set");
        }

        return baseType;
    }

    /*
     * MultiDirectionalEnsembleModel settings (Prediction)
     *
     * Yields output that belongs to algo/prediction/PredictionAlgorithm.
     */

    /**
     * Returns the right base type classifier that should be used when
     * predicting using a multi-directional ensemble model or MDE-model.
     *
     *
     */
    public PredictionAlgorithm getPredictionAlgorithm() throws MercsException {
        PredictionAlgorithm predictionAlgorithm;

        // Extend with more if more base types become available
        switch(getSettings().getMercsPredictionMethod()) {
            case SettingsMercs.MERCS_PREDICTION_TYPE_SIMPLE:
                predictionAlgorithm = new SimplePredictionAlgorithm();
                break;
            case SettingsMercs.MERCS_PREDICTION_TYPE_MODELWEIGHTED:
                predictionAlgorithm = new ModelWeightedPredictionAlgorithm();
                break;
            case SettingsMercs.MERCS_PREDICTION_TYPE_INPUTCAPPED:
                predictionAlgorithm = new InputCappedPredictionAlgorithm();
                break;
            case SettingsMercs.MERCS_PREDICTION_TYPE_PARTICIPATIONCAPPED:
                predictionAlgorithm = new ParticipationCappedPredictionAlgorithm();
                break;
            default:
                throw new MercsException("No prediction algorithm has been set");
        }

        // Configure weighted
        predictionAlgorithm.setPredictWeighted(getSettings().getMercsPredictionWeighted());

        return predictionAlgorithm;
    }

   /*
    * Settings related to outputs (file handling)
    */

   /**
    * Prepare an output directory in the right place
    * */
    public void prepareOutputLocation(int idx) throws IOException {
        String relFname = getMercsTestDestination(idx, "dummy");                // Relative filename of a certain MercsTest
        Path pathToFile = Paths.get(getSettings().getFileAbsolute(relFname));   // Absolute path
        Files.createDirectories(pathToFile.getParent());                        // Create the right directory
    }

    /**
     * Get the MercsTestPrinter, a PrintWriter object.
     *
     * The 'TestPrinter' prints out .csv files
     *
     * We use the getFileAbsoluteWriter method from the SettingsMercs class for this. This current method does some filename
     * manipulation
     * */
    public PrintWriter getMercsTestPrinter(int idx, String basename) throws FileNotFoundException {
        String fname = getMercsTestDestination(idx, basename + ".csv");
        return getSettings().getFileAbsoluteWriter(fname);
    }

    public PrintWriter getMercsTestPrinterSimple(int idx) throws FileNotFoundException {
        String fname = getMercsTestDestinationSimple(idx);
        return getSettings().getFileAbsoluteWriter(fname);
    }

    /**
     * Get the MercsJsonPrinter, a PrintWriter object.
     *
     * The 'JsonPrinter' prints out .json files
     *
     * We use the getFileAbsoluteWriter method from the SettingsMercs class for this.
     * This current method is only concerned with some filename manipulation (using the getMercsTestDestination helper method
     * */
    public PrintWriter getMercsJsonPrinter(int idx, String basename) throws FileNotFoundException {
        String fname = getMercsTestDestination(idx, basename + ".json");
        return getSettings().getFileAbsoluteWriter(fname);
    }

    /**
     * Create a JSONObject that contains the data in the MercsTestDestination .json file.
     * */
    public JSONObject getMercsJsonData(int idx, String basename) throws FileNotFoundException, IOException, ParseException {
        String fname = getMercsTestDestination(idx, basename + ".json");    // Get the destination (relative) filename
        fname = getSettings().getFileAbsolute(fname);                       // Construct the absolute filename

        JSONParser parser = new JSONParser();                               // Create a JSON parser to read the JSON file
        Object obj = parser.parse(new FileReader(fname));                   // Parse the actual file, return a JSONObject

        return (JSONObject) obj;
    }

    /**
     * Additional method that yields a relative filename based on the test index and basename
     * */
    private String getMercsTestDestination(int idx, String baseName) {
        String testName = getSettings().getMercsTestName(idx);
        return "out" + File.separator + testName + File.separator + baseName;
    }

    private String getMercsTestDestinationSimple(int idx) {
        String testName = getSettings().getMercsTestName(idx);
        return testName+".csv";
    }

   /*
    * Settings related to incremental learning
    */

    //TODO: Incremental Settings


}
