package mercs.core;

import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import clus.error.ClusErrorList;
import clus.util.ClusException;

import jeans.resource.ResourceInfo;
import jeans.util.cmdline.CMDLineArgs;

import mercs.algo.induce.DepthFirstInduceDecisionTreeWrapper;
import mercs.algo.induce.MercsInducer;
import mercs.algo.induce.MercsInductionAlgorithm;
import mercs.algo.selection.EnsemblePerformance;
import mercs.algo.type.MultiDirectionalEnsembleClassifier;
import mercs.data.MercsData;
import mercs.model.ClusModelWrapper;
import mercs.model.MercsModel;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.statistics.variabletargetvoting.VTVCombinedStat;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;



/**
 * Test class, that hosts all kinds of experiments with the Mercs system
 */
public class Tests {

    /**
     * Main method, just runs the main method of the core/mercs class.
     * */
    public static void main(String[] args){
        Mercs.main(args);
    }

    /*
    * Actual Tests
    * */

    /**
     * Extremely basic test
     * */
    public static void standardMercsRun(Mercs mercs) throws ClusException, IOException {
        MultiDirectionalEnsembleClassifier clss = new MultiDirectionalEnsembleClassifier();         // Building the classifier

        MercsInductionAlgorithm inducer = clss.createInduce(mercs.getSchema(), mercs.getSettings());// Create inducer based on settings

        MercsModel model = inducer.induce(mercs.getMercsRun());                                     // Start inducing
        System.out.println("XXXXXX DONE INDUCING");
        System.out.println(model.getModelInfo() + "DONE MODELINFO");

        //mercs.finalizeStatistics();
        //MercsRunStatistics.show(inducer);

        //EnsemblePerformance performanceCalculator = new EnsemblePerformance(model, mercs.getSchema());
        //performanceCalculator.testModelwithMajorityVote(inducer.getSettingsManager().getMercsTestData(mercs.getMercsRun()));
        System.out.println("Done");
        System.out.println("===========================================");
    }

    /**
    * Function that runs a fixed number of Mercs inductions.
    * It does not run the explicit tests provided at the end of the settings file,
    * so if you run this function without having specified a test set in the Mercs header, you will see an error.
    *
    * The idea behind this function is just to have a general induction of a Mercs model.
    * */
    public static void multipleMercsTDIDTRuns(Mercs mercs,int runs) throws ClusException, IOException {
        /* In our case we always want to build a
         MultiDirectionalEnsembleClassifier
         */
        long startTime;
        for (int i = 0; i < runs; i++) {
            System.out.println("===========================================");
            System.out.println("Starting run: " + i+1 + " TDIDT");
            System.out.println("===========================================");
            // Create inducer based on settings
            startTime = System.currentTimeMillis();
            MercsInductionAlgorithm inducer = new MercsInducer(mercs.getSchema(), mercs.getSettings());
            // Start inducing
            MercsModel model = inducer.induce(mercs.getMercsRun());
            System.out.println(model.getModelInfo());

            // Finish statistics
            mercs.finalizeStatistics();
            MercsRunStatistics.show(inducer);

            // Check performance
            EnsemblePerformance performanceCalculator = new EnsemblePerformance(model, mercs.getSchema());
            //performanceCalculator.test3(inducer.getSettingsManager().getMercsTestData(mercs.getMercsRun()), mercs.getSchema());
            performanceCalculator.testModelwithMajorityVote(inducer.getSettingsManager().getMercsTestData(mercs.getMercsRun()));

            // Print final time
            System.out.println("===========================================");
            System.out.println("Done in " + Long.toString(System.currentTimeMillis() - startTime) +" millisecs");
            System.out.println("===========================================");
        }
    }

    /**
     * Function that outputs an actual prediction as a csv file. This function puts its outputs straight in the
     * folder of the original dataset.
     * */
    public static void mercsPrediction(Mercs mercs) throws ClusException, IOException {
        // Get the settings manager
        MercsSettingsManager sm = new MercsSettingsManager(mercs.getSchema(), mercs.getSettings());

        MercsInductionAlgorithm inducer = new MercsInducer(mercs.getSchema(), mercs.getSettings());                     // Create the inducer
        MultiDirectionalEnsembleModel model = (MultiDirectionalEnsembleModel) inducer.induce(mercs.getMercsRun());      // Induce the model

        MercsRun mr = mercs.getMercsRun();

        for (int testNum = 0; testNum < sm.getNbMercsTests(); testNum++) {
            // sm.printMercsTestInfo(testNum);                                                                             // Print some info about the current test in the terminal

            // Preparations
            ClusSchema testSchema = sm.getMercsTestSchema(testNum);                                                     // Get schema of the current test (desc.->target)
            MercsData testData = sm.getMercsTestData(mr, testNum);                                                      // Get data of the current test (desc.->target)

            // Actual testing
            PrintWriter pw = sm.getMercsTestPrinterSimple(testNum);                                                     // Create a printwriter, needed to generate the output
            generateTestOutput(model, testSchema, testData, pw);                                                        // Generate the actual prediction

        }
    }

    /**
    * Function that actually outputs actual predictions in the form of a CSV File.
    * */
    public static void multipleMercsPrediction(Mercs mercs, int iterations) throws ClusException, IOException {
        // Get the settings manager
        MercsSettingsManager sm = new MercsSettingsManager(mercs.getSchema(), mercs.getSettings());

        // Initialize output folders and metadata files.
        for (int testNum = 0; testNum < sm.getNbMercsTests(); testNum++) {          // For each test, do:
            sm.prepareOutputLocation(testNum);

            ClusSchema testSchema = sm.getMercsTestSchema(testNum); // Get the schema (i.e. desc.-> target)
            generateRelationInfo(testNum, testSchema, sm);          // Generate the 'relation info' file, e.g. {"descriptive":[1,2],"target":{"nominal":[1],"attr_idxs":[11,12],"numeric":[2]}}
            generateTestManifest(testNum, sm);                      // Generate the 'test manifest' file, e.g. {"mercs_simple":[{"duration":1188,"fold":1,"location":"prediction_0.csv"}]}
        }

        // This is completely analogous to the multipleMercsTDIDTRuns
        long startTime;
        for (int i = 0; i < iterations; i++) {
            System.out.println("===========================================");
            System.out.println("Starting run: " + i+1 + " TDIDT");
            System.out.println("===========================================");
            startTime = System.currentTimeMillis();

            MercsInductionAlgorithm inducer = new MercsInducer(mercs.getSchema(), mercs.getSettings());                 // Create the inducer
            MultiDirectionalEnsembleModel model = (MultiDirectionalEnsembleModel) inducer.induce(mercs.getMercsRun());  // Induce the model

            System.out.println(model.getModelInfo());                                                                   // Print some basic info in the terminal
            mercs.finalizeStatistics();                                                                                 // Fix the statistics
            MercsRunStatistics.show(inducer);                                                                           // Show some info about the inducer

            multipleMercsPrediction(model, sm, mercs.getMercsRun(), i);                                                 // Recursive call, now giving the model and the run parameter as arguments
                                                                                                                        // This is where the ACTUAL predictions will happen.

            System.out.println("===========================================");
            System.out.println("Done in " + Long.toString(System.currentTimeMillis() - startTime)+" milliseconds.");
            System.out.println("===========================================");
        }


    }

    private static void multipleMercsPrediction(MultiDirectionalEnsembleModel model,
                                                MercsSettingsManager sm,
                                                MercsRun mr,
                                                int itNum) throws ClusException, IOException {

        for (int testNum = 0; testNum < sm.getNbMercsTests(); testNum++) {
            sm.printMercsTestInfo(testNum);                                                                             // Print some info about the current test in the terminal

            // Preparations
            ClusSchema testSchema = sm.getMercsTestSchema(testNum);                                                     // Get schema of the current test (desc.->target)
            MercsData testData = sm.getMercsTestData(mr, testNum);                                                      // Get data of the current test (desc.->target)

            // Actual testing
            PrintWriter pw = sm.getMercsTestPrinter(testNum, "mercsPred" + itNum);                                      // Create a printwriter, needed to generate the output
            long deltaT2 = generateTestOutput(model, testSchema, testData, pw);                                         // Generate test output, i.e. the CSV file with ACTUAL predictions

            // Write the contents of the test_manifest file.
            appendTestManifest(testNum, "mercs_simple", itNum+1, "prediction_" + itNum + ".csv", deltaT2, sm);
        }
    }


    /*
    * Helper functions
    * */

    /**
     * Writing the output of the test to a CSV File
     * */
    private static long generateTestOutput(MultiDirectionalEnsembleModel model,
                                           ClusSchema relation, MercsData data,
                                           PrintWriter pw)throws IOException, ClusException {
        long startTime = System.currentTimeMillis();

        data.init();                                                                                                    // Initialize the testdata (input descr. attr.)
        ClusAttrType[] atts = relation.getAllAttrUse(ClusAttrType.ATTR_USE_TARGET);                                     // Get the indices of all the target attributes
        for (ClusAttrType att:atts){
            pw.append("Var"+String.valueOf(att.getIndex()));
        }
        pw.append("\n");

        while (data.hasNextTuple()) {                                                                                   // Loop over all the tuples in the testset

            DataTuple sample = data.readNextTuple();                                                                    // Get the next tuple
            VTVCombinedStat cs = (VTVCombinedStat) model.predict(sample, relation);                                     // Predict the target attributes of this tuple

            // Writing all the target attributes to file

            if (atts.length > 0) {
                boolean firstEl = true;
                int iNom = 0, iNum = 0;

                int[] nomPredValues = cs.getNominalPred();                                                              // Get the nominal predictions (encoded in ints)
                double[] numPredValues = cs.getNumericPred();                                                           // Get the numeric predictions

                for (ClusAttrType att : atts) {                                                                         // FOR EACH (target!) attribute, do all this:

                    if (firstEl) {
                        firstEl = false;
                    } else {
                        pw.append(",");
                    }                                                                       // Do not write a comma if we're at the first element, otherwise do

                    switch (att.getTypeIndex()) {
                        case 0:    // NominalAttrType
                            pw.append(String.valueOf(nomPredValues[iNom++]));
                            break;
                        case 1:    // NumericAttrType
                            pw.append(String.valueOf(numPredValues[iNum++]));
                            break;
                        default:
                            // Do nothing
                    }                                                                   // Write the prediction of the attribute at hand to the file
                }
            }
            pw.append("\n");
        }

        if (pw.checkError()) {
            System.out.println("Error while writing test results to disk.");
        }   // Error message

        pw.close();

        return System.currentTimeMillis() - startTime;
    }

    /**
     * Generate the relationInfo and write the actual file
     * */
    private static void generateRelationInfo(int testNum, ClusSchema relation, MercsSettingsManager sm) throws FileNotFoundException {
        JSONObject rel = new JSONObject();
        JSONArray desc = new JSONArray();
        for (ClusAttrType att : relation.getAllAttrUse(ClusAttrType.ATTR_USE_DESCRIPTIVE)) {
            desc.add(att.getIndex() + 1);
        }

        JSONObject targ = new JSONObject();
        JSONArray attrIdxs = new JSONArray();
        JSONArray nom = new JSONArray();
        JSONArray num = new JSONArray();
        ClusAttrType[] atts = relation.getAllAttrUse(ClusAttrType.ATTR_USE_TARGET);
        for (int i = 0; i < atts.length; i++) {
            switch (atts[i].getTypeIndex()) {
                case 0:    // NominalAttrType
                    nom.add(i + 1);
                    break;
                case 1:    // NumericAttrType
                    num.add(i + 1);
                    break;
                default:
                    // Do nothing
            }
            attrIdxs.add(atts[i].getIndex() + 1);
        }
        targ.put("attr_idxs", attrIdxs);
        targ.put("numeric", num);
        targ.put("nominal", nom);

        rel.put("descriptive", desc);
        rel.put("target", targ);

        PrintWriter pw = sm.getMercsJsonPrinter(testNum, "relation");
        pw.write(rel.toJSONString());
        if (pw.checkError()) {
            System.out.println("Error while writing relation json to disk.");
        }
        pw.close();
    }

    /**
     * Generate the test manifest contents and write the file
     * */
    private static void generateTestManifest(int testNum, MercsSettingsManager sm) throws FileNotFoundException {
        JSONObject manif = new JSONObject();
        PrintWriter pw = sm.getMercsJsonPrinter(testNum, "test_manifest");
        pw.write(manif.toJSONString());
        if (pw.checkError()) {
            System.out.println("Error while writing initial json manifest to disk.");
        }
        pw.close();
    }

    /**
     * Two methods to write an extra line to the test manifest. This because we can choose to more than one iteration.
     * */
    private static void appendTestManifest(int testNum, String testType, String loc, MercsSettingsManager sm) {
        try {
            JSONObject manif = sm.getMercsJsonData(testNum, "test_manifest");
            manif.put(testType, loc);

            PrintWriter pw = sm.getMercsJsonPrinter(testNum, "test_manifest");
            pw.write(manif.toJSONString());
            if (pw.checkError()) {
                System.out.println("Error while amended json manifest to disk.");
            }
            pw.close();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private static void appendTestManifest(int testNum, String testType, int foldNum, String loc, long time, MercsSettingsManager sm) {
        try {
            JSONObject manif = sm.getMercsJsonData(testNum, "test_manifest");
            JSONObject testSingle = new JSONObject();
            testSingle.put("fold", foldNum);
            testSingle.put("duration", time);
            testSingle.put("location", loc);

            JSONArray tests = ((JSONArray) manif.get(testType));
            if (tests == null) {
                tests = new JSONArray();
                manif.put(testType, tests);
            }
            tests.add(testSingle);

            PrintWriter pw = sm.getMercsJsonPrinter(testNum, "test_manifest");
            pw.write(manif.toJSONString());
            if (pw.checkError()) {
                System.out.println("Error while amended json manifest to disk.");
            }
            pw.close();

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }







}
