package mercs.core;

import clus.data.io.ClusReader;
import clus.data.io.ClusView;
import clus.data.type.ClusSchema;
import mercs.data.MercsData;

/**
 * MercsRun
 * Contains ClusView, ClusReader, ClusSchema
 * Contains SettingsMercs and MercsData
 *
 * It serves as an easy access point for information spread across multiple classes
 */

 public class MercsRun {
    /*
     * =======================================
     * FIELDS + PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    public final static long serialVersionUID = SettingsMercs.SERIAL_VERSION_ID;

    private MercsData data;
    private MercsData testData;
    private MercsData pruneData;

    private ClusView view;
    private ClusReader reader;
    private ClusSchema schema;

    private SettingsMercs settings;


    /**
     * Returns the data of this mercsRun, is only initialised after explicitely
     * setting the data (not by constructing this mercsRun)
     */
    public MercsData getData() {
        return data;
    }
    /**
     * Sets the given data of this mercsRun.
     * @param data A MercsData object
     */
    public void setData(MercsData data) throws IllegalArgumentException {
        if (!isValidData(data))
            throw new IllegalArgumentException("Invalid data");
        this.data = data;
    }
    /**
     * Returns the test data
     */
    public MercsData getTestData() {
        return testData;
    }
    /**
     * Sets the test data
     */
    public void setTestData(MercsData testData) throws IllegalArgumentException {
        if (!isValidData(testData)) {
            throw new IllegalArgumentException("Invalid test data");
        }

        this.testData = testData;
    }
    /**
     * Gets the test data
     */
    public MercsData getPruneData() {
        return pruneData;
    }
    /**
     * Sets the test data
     */
    public void setPruneData(MercsData pruneData) throws IllegalArgumentException {
        if (!isValidData(pruneData)) {
            throw new IllegalArgumentException("Invalid prune data");
        }
        this.pruneData = pruneData;
    }

    /**
     * Returns the view of this mercsRun
     */
    public ClusView getView() {
        return view;
    }
    /**
     * Sets the view of this mercsRun
     */
    private void setView(ClusView view) {
        this.view = view;
    }
    /**
     * Returns the reader of this mercsRun
     */
    public ClusReader getReader() {
        return reader;
    }
    /**
     * Sets the reader of this mercsRun
     */
    private void setReader(ClusReader reader) {
        this.reader = reader;
    }
    /**
     * Returns the schema of this mercsRun
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema of this mercsRun
     */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }

    /**
     * Returns the settings of this mercsRun
     */
    public SettingsMercs getSettings() {
        return settings;
    }
    /**
     * Sets the settings of this mercsRun
     */
    private void setSettings(SettingsMercs settings) {
        this.settings = settings;
    }


    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     * Initializes this MercsRun given a Mercs object. It initializes:
     * Clusview, ClusReader, ClusSchema
     * and MercsSettings.
     *
     * Note that the data is not set.
     *
     * @param mercs
     *            The Mercs object that is used to retrieve the view, reader, schema
     *            and settings
     * @throws IllegalArgumentException
     *             Thrown when the given Mercs is invalid
     */
    public MercsRun(Mercs mercs) throws IllegalArgumentException {
        if (!isValidMercs(mercs)) {
            throw new IllegalArgumentException("Given an invalid mercs");
        }

        setView(mercs.getView());
        setReader(mercs.getReader());
        setSchema(mercs.getSchema());

        setSettings(mercs.getSettings());
    }

    /**
     * isValidMercs
     * Checks whether the given Mercs is valid.
     * We need a Mercs, reader, schema and settings to be assigned. Then OK.
     */
    public boolean isValidMercs(Mercs mercs) {
        return mercs != null && mercs.getView() != null && mercs.getReader() != null && mercs.getSchema() != null
                && mercs.getSettings() != null;
    }

    /**
     * isValidData
     * Checks whether the given data is valid. This means checking if data exists.
     */
    public boolean isValidData(MercsData data) {
        return data != null;
    }



}
