package mercs.core;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

import mercs.algo.induce.MercsInductionAlgorithm;

/**
 * A class where runtime statistics can be recorded, such as memory information,
 * time information, samples processed,...
 *
 * @author mattias
 *
 */
public class MercsRunStatistics {

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    public static long initialMemory;
    public static long endMemory;
    public static long beginTime;
    public static long endTime;

    private static MemoryMXBean memoryUsage = ManagementFactory.getMemoryMXBean();


    /**
     * Returns the initial memory used by the system
     */
    public static long getInitialMemory() {
        return initialMemory;
    }
    /**
     * Sets the initial memory of the system
     */
    public static void setInitialMemory(long initialMemory) {
        MercsRunStatistics.initialMemory = initialMemory;
    }

    /**
     * Returns the used memory at the end of the learning
     */
    public static long getEndMemory() {
        return endMemory;
    }
    /**
     * Sets the end of the memory
     */
    public static void setEndMemory(long endMemory) {
        MercsRunStatistics.endMemory = endMemory;
    }

    /**
     * Returns the time before the system started learning
     */
    public static long getBeginTime() {
        return beginTime;
    }
    /**
     * Sets the begin time
     */
    public static void setBeginTime(long beginTime) {
        MercsRunStatistics.beginTime = beginTime;
    }

    /**
     * Returns the time after the system has learned the model
     */
    public static long getEndTime() {
        return endTime;
    }
    /**
     * Sets the end time
     */
    public static void setEndTime(long endTime) {
        MercsRunStatistics.endTime = endTime;
    }


    /**
     * Returns the current memory used
     */
    public static long getCurrentMemory() {
        return memoryUsage.getHeapMemoryUsage().getUsed() / 1000;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Shows general information about the MercsRun
     * @param inducer
     */
    public static void show(MercsInductionAlgorithm inducer) {
        double sum = getEndTime() - getBeginTime();
        System.out.println("Mem usage (KB) [initial, end]: [" + getInitialMemory() + ", " + getEndMemory() + "]");
        System.out.println("Total induction time: " + sum + "ms");
        System.out.println("Total samples processed: " + inducer.getSamplesProcessed());
        System.out.println(
                "Average samples processed per second: " + Math.round(inducer.getSamplesProcessed() * 1000 / sum));
    }

}
