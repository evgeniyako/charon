package mercs.model;

import clus.data.rows.DataTuple;
import clus.data.type.ClusSchema;
import clus.statistic.ClusStatistic;
import clus.util.ClusException;
import jeans.tree.MyNode;
import mercs.algo.prediction.PredictionAlgorithm;
import mercs.algo.selection.CombinedPerformance;
import mercs.data.MercsData;
import mercs.util.MercsException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * EnsembleModel
 *
 * This class collects all the useful information about an EnsembleModel
 */
public abstract class EnsembleModel implements MercsModel {
    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    protected ClusSchema schema;
    protected PredictionAlgorithm predictionAlgorithm;
    protected List<MercsModel> models;

    /**
     * Returns the schema of the model
     */
    @Override
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema of the model to the given schema
     */
    public void setSchema(ClusSchema schema) {
        this.schema = schema;
    }

    /**
     * Returns the prediction algorithm that is used to predict from data tuples
     */
    public PredictionAlgorithm getPredictionAlgorithm() {
        return predictionAlgorithm;
    }
    /**
     * Sets the prediction algorithm to the given algorithm
     */
    public void setPredictionAlgorithm(PredictionAlgorithm predictionAlgorithm) {
        this.predictionAlgorithm = predictionAlgorithm;
    }

    /**
     * returns the MercsModels of the ensemble
     */
    public List<MercsModel> getModels() {
        return models;
    }


    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Returns a prediction of the given data tuple
     *
     * @param tuple
     */
    @Override
    public ClusStatistic predict(DataTuple tuple) {
        return predict(tuple, tuple.getSchema());
    }
    public ClusStatistic predict(DataTuple tuple, ClusSchema relation) {
        return getPredictionAlgorithm().predictWeighted(this, relation, tuple);
    }

    /**
     * Returns the underlying tree of the model
     */
    @Override
    public MyNode getTree() throws MercsException {
        throw new MercsException("A tree can only be returned from a single model, not from an ensemble.");
    }

    /*
     * Some extra methods related to the model itself
     * */

    /**
     * Adds the given model to the ensemble model
     */
    public void addModel(MercsModel model) {
        models.add(model);
    }

    @Override
    public int getModelSize() {
        return models.size();
    }
    /**
     * Returns general information about the model
     */
    @Override
    public String getModelInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        List<? extends MercsModel> models = getModels();

        for (int i = 0; i < models.size(); i++) {
            stringBuilder.append("Tree ").append(i).append(": ");
            stringBuilder.append(models.get(i).getCombinedPerformance().getModelErrorDescriptionByIndex()).append("\n");
        }

        return stringBuilder.toString();
    }
    /**
     * Prints the model to the given writer
     *
     * @param wrt
     */
    @Override
    public void printModel(PrintWriter wrt) {
        for (MercsModel model : getModels()) {
            wrt.write(model.getModelInputAndTargetAttributes() + "\n");
            model.printModel(wrt);
            wrt.write("\n");
        }

        wrt.flush();
    }

    /**
     * =======================================
     * DUMMY METHODS [Derived from interface]
     * =======================================
     */

    /**
     * Returns the combined performance of this model, after the performance has
     * been calculated, otherwise it throws an illegal state exception;
     */
    @Override
    public CombinedPerformance getCombinedPerformance() throws IllegalStateException {
        return null;
    }
    /**
     * Checks whether this model has a valid combined performance initialised.
     */
    @Override
    public boolean hasValidCombinedPerformance() {
        return false;
    }
    /**
     * Calculates the performance of this model, given the data
     *
     * @param data
     */
    @Override
    public void calculatePerformance(MercsData data) throws IOException, ClusException {
        //TODO Auto-generated method stub
    }


    /*
     * Everything related to attributes.
     *
     * The first two methods return a list of integers, that tells us which role the attributes play (e.g.: descriptive, target)
     *
     * The last method (=getModelInputAndTargetAttributes) just gives us a string that informs us about the different attributes
     * */

    /**
     * Returns list of integers that refer to the descriptive attributes
     */
    @Override
    public List<Integer> getModelDescriptiveAttributesList() {
        return null;
    }
    /**
     * Returns list of integers that refer to target attributes
     */
    @Override
    public List<Integer> getModelTargetAttributesList() {
        return null;
    }
    /**
     * Returns the input attributes used for prediction and the target
     * attributes of the prediction, in a string
     */
    @Override
    public String getModelInputAndTargetAttributes() {
        return null;
    }

}
