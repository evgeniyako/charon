package mercs.model;

import java.util.ArrayList;
import java.util.Collections;

import clus.data.type.ClusSchema;
import mercs.core.MercsSettingsManager;
import mercs.util.MercsException;


/**
 * MultiDirectionalEnsembleModel
 *
 * A Multi-Directional Ensemble (MDE) model.
 *
 * This model is able to predict from any given set of descriptive attributes any set of target attributes.
 *
 * It is composed of multiple ensembles of MercsModels (so a MercsModel is the component from which a MDE models consists)
 *
 */
public class MultiDirectionalEnsembleModel extends EnsembleModel{

    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this model with an empty list of models (i.e. MercsModels) and initializes the
     * prediction algorithm.
     *
     * Throws an exception when there was no prediction
     * algorithm specified.
     */
    public MultiDirectionalEnsembleModel(ClusSchema schema, MercsSettingsManager manager) throws MercsException {
        // Make a list of MercsModels, and define the predictionalgorithm and set the Schema.
        models = Collections.synchronizedList(new ArrayList<MercsModel>());
        setPredictionAlgorithm(manager.getPredictionAlgorithm());
        setSchema(schema);
    }

    /**
     * SimpleEnsembleModel
     *
     * This MDE-model just consists of adding all individual MercsModels.
     *
     * */
    public SimpleEnsembleModel simplify() {
        SimpleEnsembleModel simpleModel = new SimpleEnsembleModel(schema, predictionAlgorithm);
        simpleModel.getModels().addAll(models);

        return simpleModel;
    }

}
