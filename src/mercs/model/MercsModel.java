package mercs.model;

import clus.data.rows.DataTuple;
import clus.data.type.ClusSchema;
import clus.statistic.ClusStatistic;
import clus.util.ClusException;
import jeans.tree.MyNode;
import mercs.algo.selection.CombinedPerformance;
import mercs.data.MercsData;
import mercs.util.MercsException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Interface for MercsModel
 *
 * Inducers learn the model,
 * the model can be used to make predictions afterwards.
 */

public interface MercsModel {
    /**
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    /**
     * Returns the model schema.
     */
    public ClusSchema getSchema();

    /**
     * Returns the combined performance of this model, after the performance has
     * been calculated, otherwise it throws an illegal state exception;
     */
    public CombinedPerformance getCombinedPerformance() throws IllegalStateException;

    /**
     * Checks whether this model has a valid combined performance initialised.
     */
    public boolean hasValidCombinedPerformance();



    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Returns a prediction of the given data tuple
     */
    public ClusStatistic predict(DataTuple tuple);

    /**
     * Calculates the performance of this model, given the data
     */
    public void calculatePerformance(MercsData data) throws IOException, ClusException;

    /**
     * Returns the underlying tree of the model
     */
    public MyNode getTree() throws MercsException;

    /**
     * Everything related to attributes. These are not properties of the object itself, but have to be collected elsewhere.
     * */

    /**
     * Returns list of integers that refer to the descriptive attributes
     */
    public List<Integer> getModelDescriptiveAttributesList();

    /**
     * Returns list of integers that refer to target attributes
     */
    public List<Integer> getModelTargetAttributesList();

    /**
     * Returns the input attributes used for prediction and the target
     * attributes of the prediction, in a string
     */
    public String getModelInputAndTargetAttributes();

    /**
     * Some extra methods related to the model itself
     * */

    /**
     * Returns general information about the model
     */
    public String getModelInfo();
    /**
     * Returns the size of the model
     */
    public int getModelSize();
    /**
     * Prints the model to the given writer
     */
    public void printModel(PrintWriter wrt);

}
