package mercs.model;

import clus.data.type.ClusSchema;
import mercs.algo.prediction.PredictionAlgorithm;

import java.util.ArrayList;

/**
 * This class is created as an MDE-like class that can be processed through Kryo.
 * The only real change is that the models list is not thread-safe.
 *
 * NB: Be critical about this class, it does not really seem to me as a super crucial one.
 */
public class SimpleEnsembleModel extends EnsembleModel {


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this model with an empty list of models (i.e. MercsModels) and initializes the
     * prediction algorithm.
     *
     * Throws an exception when there was no prediction
     * algorithm specified.
     */
    public SimpleEnsembleModel(ClusSchema schema, PredictionAlgorithm predictionAlgorithm) {
        models = new ArrayList<>();
        setPredictionAlgorithm(predictionAlgorithm);
        setSchema(schema);
    }

}
