package mercs.model;

import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import clus.error.ClusErrorList;
import clus.model.ClusModel;
import clus.statistic.ClusStatistic;
import clus.util.ClusException;
import jeans.tree.MyNode;
import mercs.algo.selection.CombinedPerformance;
import mercs.data.MercsData;
import mercs.util.MercsException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for Clus class ClusModel
 */
public class ClusModelWrapper implements MercsModel{

    /**
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    private ClusModel model;
    private ClusSchema schema;
    private CombinedPerformance combinedPerformance;
    private boolean hasValidPerformance = false;

    /**
     * Returns the clus model
     */
    public ClusModel getModel() {
        return model;
    }
    /**
     * Sets the clus model to the given model
     */
    private void setModel(ClusModel model) {
        this.model = model;
    }
    /**
     * Checks whether the given model is valid. It is valid if and only if the
     * given model is not equal to null.
     */
    public boolean isValidModel(ClusModel model) {
        return model != null;
    }

    /**
     * Returns the schema of the model
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema of the model to the given schema
     */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Checks whether the given schema is valid. It is valid if and only if the
     * given schema is not equal to null.
     */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }

    /**
     * Returns the combinedPerformance of the model
     * */
    @Override
    public CombinedPerformance getCombinedPerformance() throws IllegalStateException {
        if (!hasValidCombinedPerformance()) {
            throw new IllegalStateException("Performance not initialised");
        }
        return combinedPerformance;
    }
    /**
     * Sets the combinedPerformance of the model
     * NB.: Also changes the value of hasValidPerformance!
     * */
    public void setCombinedPerformance(CombinedPerformance perf) {
        combinedPerformance = perf;
        hasValidPerformance = true;
    }

    /**
     * Returns the value of the boolean property hasValidPerformance
     * */
    public boolean hasValidCombinedPerformance() {
        return hasValidPerformance;
    }


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this wrapper (=set model and schema) with the given Clus model (~ClusModel) if it is a valid model and schema.
     *
     * If these conditions are not met, we throw an exception.
     */
    public ClusModelWrapper(ClusModel model, ClusSchema schema) throws MercsException {
        if (!isValidModel(model) || !isValidSchema(schema)) {
            throw new MercsException("Invalid clus model or schema");
        }
        setModel(model);
        setSchema(schema);
    }

    /**
     * Returns a prediction of the given data tuple
     */
    @Override
    public ClusStatistic predict(DataTuple tuple) {
        return getModel().predictWeighted(tuple);
    }

    /**
     * Calculates the performance of this model, given the data
     */
    @Override
    public void calculatePerformance(MercsData data) throws IOException, ClusException {
        data.init();
        DataTuple sample;
        ClusErrorList errorList = new ClusErrorList();
        CombinedPerformance combinedError = new CombinedPerformance(errorList,
                getSchema().getNumericAttrUse(ClusAttrType.ATTR_USE_TARGET),
                getSchema().getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET));
        errorList.addError(combinedError);

        while (data.hasNextTuple()) {
            // Get the next data entry
            sample = data.readNextTuple();
            // Compare fact and prediction
            errorList.addExample(sample, predict(sample));
        }

        setCombinedPerformance(combinedError);
    }

    /**
     * Returns the underlying tree of the model
     */
    @Override
    public MyNode getTree() throws MercsException {
        if(model instanceof MyNode) {
            return ((MyNode)model).getRoot();
        } else {
            throw new MercsException("Model is not a tree");
        }
    }


    /*
     * Everything related to attributes.
     *
     * The first two methods return a list of integers, that tells us which role the attributes play (e.g.: descriptive, target)
     *
     * The last method (=getModelInputAndTargetAttributes) just gives us a string that informs us about the different attributes
     * */

    /**
     * Returns list of integers that refer to the descriptive attributes
     */
    @Override
    public List<Integer> getModelDescriptiveAttributesList() {
        List<Integer> returnList = new ArrayList<Integer>();
        for (ClusAttrType at : getSchema().getDescriptiveAttributes()) {
            returnList.add(at.getIndex() + 1);
        }

        return returnList;
    }

    /**
     * Returns list of integers that refer to target attributes
     */
    @Override
    public List<Integer> getModelTargetAttributesList() {
        List<Integer> returnList = new ArrayList<Integer>();
        for (ClusAttrType at : getSchema().getTargetAttributes()) {
            returnList.add(at.getIndex() + 1);
        }

        return returnList;
    }

    /**
     * Returns the input attributes used for prediction and the target
     * attributes of the prediction, in a string
     */
    @Override
    public String getModelInputAndTargetAttributes() {
        StringBuilder builder = new StringBuilder();
        builder.append("Input: ");
        for (ClusAttrType at : getSchema().getDescriptiveAttributes()) {
            builder.append(at.getName());
            builder.append(", ");
        }
        builder.append("Output: ");
        for (ClusAttrType at : getSchema().getTargetAttributes()) {
            builder.append(at.getName());
            builder.append(", ");
        }
        builder.delete(builder.length() - 2, builder.length());

        return builder.toString();
    }


    /*
     * Some extra methods related to the model itself
     * */

    /**
     * Returns the size of the model
     */
    @Override
    public int getModelSize() {
        return getModel().getModelSize();
    }
    /**
     * Returns general information about the model
     */
    @Override
    public String getModelInfo() {
        return getModel().getModelInfo();
    }
    /**
     * Prints the model to the given writer
     */
    @Override
    public void printModel(PrintWriter wrt) {
        getModel().printModel(wrt);
    }




}
