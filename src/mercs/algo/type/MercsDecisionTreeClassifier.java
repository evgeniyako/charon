package mercs.algo.type;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.algo.induce.DepthFirstInduceDecisionTreeWrapper;
import mercs.core.SettingsMercs;

import java.io.IOException;

/**
 * MercsDecisioTreeClassifier
 *
 * Class representing the TDIT-inducution type.
 *
 * In the MercsSettingsManagers, a choice is made regarding 'types'. This refers to
 * several choices that have to be made in the Mercs system
 *
 * Firstly how to SELECT which model (~trees) to learn.
 *      This is dealt with in the algo/selection package. I.e. SelectionAlgorithmis the blueprint of this.
 *
 * -->  Secondly, how to INDUCE these models (i.e. VFDT or TDIT or ...)
 *          This is dealt with here, the MercsInductionAlgorithmType is the blueprint.
 *          Basically, it is a TDIT-VFDT dilemma.
 *          This then couples to the algo/induce package, where specific implementations reside.
 *
 * Thirdly, how to PREDICT using the constructed model
 *      This is dealt with in the algo/prediction package, where PredictionAlgorithm is the blueprint.
 *
 *TODO: Figure out why this illogical(?) structure happened. (Why do we refer in this package to the induce package?)
 */
public class MercsDecisionTreeClassifier extends MercsInductionAlgorithmType {

    /**
     * This method returns a DepthFirstInduceDecisionTreeWrapper, present in the Clus.
     *
     * This method creates an MercsInductionAlgorithm that is selected based on
     * the settings.
     *
     * @param schema
     *            The schema that is used to instantiate the induction algorithm
     * @param sett
     *            The settings to decide which algorithm should be returned
     * @return Returns a MercsInductionAlgorithm with the given schema.
     * @throws ClusException
     *             Thrown when a problem occurred during instantiation of the
     *             algorithm
     * @throws IOException
     *             Thrown when an IOException occurred during instantiation of
     *             the algorithm
     */
    @Override
    public DepthFirstInduceDecisionTreeWrapper createInduce(ClusSchema schema, SettingsMercs sett)
            throws ClusException, IOException {
        init(schema, sett);
        return new DepthFirstInduceDecisionTreeWrapper(schema, sett);
    }

}
