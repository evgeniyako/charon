package mercs.algo.type;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.algo.induce.MercsInducer;
import mercs.algo.induce.MercsInductionAlgorithm;
import mercs.core.SettingsMercs;
import mercs.util.MercsException;

// import mercs.algo.induce.MercsVfdtInducer;

import java.io.IOException;

/**
 * MultiDirectionalEnsembleClassifier
 *
 * This class is what makes the first of three important choices in the Mercs system (i.e. Basetype, Selection algo and Prediction algo).
 * The first choice is concerned with the base type, which de facto means a choice regarding
 * static learning (learning over all available data) or dynamic learning, from a stream. The base type makes this choice,
 * because it is a choice between TDIT or VFDT inducer.
 *
 * The inducer is the thing that builds (or induces) the models(~decision trees). Which models that have to be built is another important
 * question, adressed in the selection algorithms. But the first choice is HOW to build those models, at once (TDIT) or incremental (VFDT). This
 * class is concerned by building the RIGHT KIND of inducer.
 *
 * TODO: I currently ignore the VFDT-option, also in import statements
 *
 * */
public class MultiDirectionalEnsembleClassifier extends MercsInductionAlgorithmType {

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * This method creates an MercsInductionAlgorithm that is selected based on
     * the settings.
     *
     * @param schema
     *            The schema that is used to instantiate the induction algorithm
     * @param settings
     *            The settings to decide which algorithm should be returned
     * @return Returns a MercsInductionAlgorithm with the given schema.
     * @throws ClusException
     *             Thrown when a problem occurred during instantiation of the
     *             algorithm
     * @throws IOException
     *             Thrown when an IOException occurred during instantiation of
     *             the algorithm
     */
    @Override
    public MercsInducer createInduce(ClusSchema schema, SettingsMercs settings) throws ClusException, IOException {
        init(schema, settings);

        // Check if the settings contain information about Mercs
        if (!getSettings().isSectionMercsEnabled()) {
            throw new MercsException("Wrong initialisation for MERCS");
        }

        // Select the proper kind of Mercs algorithm. If more kinds become available, this needs to be edited.
        switch(getSettings().getMercsBaseType()) {
            case SettingsMercs.MERCS_BASE_TYPE_TDIT:
                return new MercsInducer(schema, settings);
            //case SettingsMercs.MERCS_BASE_TYPE_VFDT:                                          //VFDT Here
            //    return new MercsVfdtInducer(schema, settings);
            default:
                throw new MercsException("Unknown MERCS base type");
        }
    }


}
