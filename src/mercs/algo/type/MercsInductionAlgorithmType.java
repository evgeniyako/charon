package mercs.algo.type;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.algo.induce.MercsInductionAlgorithm;
import mercs.core.SettingsMercs;

import java.io.IOException;

/**
 * MercsInductionAlgorithmType
 *
 * Abstract class that describes an algorithm type (~kind).
 *
 * Each type (e.g. incremental learners) has different algorithms that can be used (e.g. VFDT, ...) in that paradigm.
 *
 * This class serves as a template to create such a type, based on settings.
 *
 */
public abstract class MercsInductionAlgorithmType {
    /**
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    private ClusSchema schema;
    private SettingsMercs settings;

    /**
     * Returns the Schema
     * */
    protected ClusSchema getSchema() {
        return schema;
    }
    /**
     * Returns the settings
     * */
    protected SettingsMercs getSettings() {
        return settings;
    }


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * This method creates an MercsInductionAlgorithm that is selected based on
     * the settings.
     *
     * @param schema
     *            The schema that is used to instantiate the induction algorithm
     * @param settings
     *            The settings to decide which algorithm should be returned
     * @return Returns a MercsInductionAlgorithm with the given schema.
     * @throws ClusException
     *             Thrown when a problem occurred during instantiation of the
     *             algorithm
     * @throws IOException
     *             Thrown when an IOException occurred during instantiation of
     *             the algorithm
     */
    public abstract MercsInductionAlgorithm createInduce(ClusSchema schema, SettingsMercs settings)
            throws ClusException, IOException;

    /**
     * Fill in the ClusSchema and SettingsMercs of the MercsInductionAlgorithmType
     * */
    protected void init(ClusSchema schema, SettingsMercs settings) {
        this.schema = schema;
        this.settings = settings;
    }

    /**
     * Prints the info of the classifier
     */
    public void printInfo() {
        System.out.println("Classifier: " + getClass().getName());
    }


}
