package mercs.algo.induce;

import java.io.IOException;

import clus.algo.tdidt.ClusNode;
import clus.algo.tdidt.DepthFirstInduce;
import clus.algo.tdidt.DepthFirstInduceSparse;
import clus.data.ClusData;
import clus.data.rows.RowData;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import clus.main.ClusStatManager;
import clus.pruning.CartPruning;
import clus.pruning.SequencePruningVSB;
import clus.statistic.ClusStatistic;
import clus.util.ClusException;
import mercs.core.MercsRun;
import mercs.core.SettingsMercs;
import mercs.data.MemoryStream;
import mercs.model.ClusModelWrapper;
import mercs.model.MercsModel;

/**
 * wrapper for relevant Clus class.
 *
 * This class is a Depth-First Decision Tree (DF-DT) algorithm. This induces a
 * single directional decision tree (~classic decision tree).
 *
 * NB.: This algorithm is opposed to the VFDT algorithm, that works on streaming data.
 *
 */
public class DepthFirstInduceDecisionTreeWrapper extends MercsInductionAlgorithm {

    /**
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    // DepthFirstInduce comes from Clus
    private DepthFirstInduce inducer;

    /**
     * Returns the depth first inducer of this wrapper
     */
    public DepthFirstInduce getInducer() {
        return inducer;
    }
    /**
     * Sets the inducer of this wrapper
     */
    private void setInducer(DepthFirstInduce inducer) {
        this.inducer = inducer;
    }

    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this object with the given schema (ClusSchema) and settings (SettingsMercs). Furthermore
     * it creates a depth first decision tree inducer (from Clus).
     */
    public DepthFirstInduceDecisionTreeWrapper(ClusSchema schema, SettingsMercs sett)
            throws ClusException, IOException {
        super(schema, sett);		//Classifier of the superclass (MercsInductionAlgorithm)

        // Two options depending on sparse or not
        if (schema.isSparse()) {
            setInducer(new DepthFirstInduceSparse(schema, sett));
        } else {
            setInducer(new DepthFirstInduce(schema, sett));
        }
    }


    /**
     * Prunes a model given the mercsRun to access the prune data
     *
     * @param model
     *            The model to prune
     * @param mr
     *            The mercsRun containing the prune data
     * @return Returns a pruned model
     */
    @Override
    public MercsModel prune(MercsModel model, MercsRun mr) {
        try {
            if (mr.getPruneData().hasNextTuple()) {                                 // As long as we can prune

                // Collect the pruning data, put it MemoryStream object
                RowData pruneData = ((MemoryStream) mr.getPruneData()).getData();
                pruneData.setSchema(getInducer().getSchema());

                // Make the pruner (this is what DOES the actual work)
                SequencePruningVSB pruner = new SequencePruningVSB(((MemoryStream) mr.getPruneData()).getData(),
                        getInducer().getStatManager().getClusteringWeights());

                // CART pruning is used as it is compatible with regression and classification
                pruner.setSequencePruner(new CartPruning(getInducer().getStatManager().getClusteringWeights(),
                        getSettings().isMSENominal()));
                pruner.prune((ClusNode) ((ClusModelWrapper) model).getModel());
            }
            return model;
        } catch (ClusException | IOException e) {
            return model;
        }
    }

    /**
     * Induces a single unpruned model using this algorithm (abstract method)
     */
    @Override
    public ClusModelWrapper induceSingleUnpruned(MercsRun mr) throws ClusException, IOException {
        if (getData() == null) {
            createData(mr);
        }
        // Initialization procedure for DepthFirstInduce class of Clus
        this.inducer.initialize();
        initializeAttributeWeights(getData().getData());
        this.inducer.initializeHeuristic();

        /*
        The RowData of the memoryStream is used to learn upon.
        Thus this learner does not learn on a stream.

        ClusModelWrapper(ClusModel, ClusSchema), so that is what we pass to this constructor
        */
        return new ClusModelWrapper(getInducer().induceSingleUnpruned(getData().getData()), getSchema());
    }

    /**
     * Method copied from Clus, to initialize Clus statistics and heuristics.
     *
     * @param data
     *            The data used to normalize the weights in the ClusStatManager
     *            (see clus for more info)
     */
    private final void initializeAttributeWeights(ClusData data) throws IOException, ClusException {
        ClusStatManager mgr = getInducer().getStatManager();
        ClusStatistic allStat = mgr.createStatistic(ClusAttrType.ATTR_USE_ALL);
        ClusStatistic[] stats = new ClusStatistic[1];
        stats[0] = allStat;
        mgr.initNormalizationWeights(allStat, data);
        mgr.initClusteringWeights();
        mgr.initDispersionWeights();
        mgr.initHeuristic();
        mgr.initStopCriterion();
        mgr.initSignifcanceTestingTable();
        mgr.check();
    }


    /**
     * Initialises the data (Prune, Train and Test) that is to be used for learning.
     * What kind of stream it is, depends on the settings.
     */
    protected void createData(MercsRun mr) throws IOException, ClusException {
        if (mr.getData() == null) {
            getSettingsManager().createPruneSetAndTrainSetInMercsRun(mr);
        }
        this.data = mr.getData();
        mr.setTestData(getSettingsManager().getTestData(mr));
    }

    /**
     * Return a MemoryStream obkect that contains the data.
     * This comes from the property of the superclass (=MercsInductionAlgorithm)
     * */
    @Override
    public MemoryStream getData() {
        return (MemoryStream) super.getData();
    }





}
