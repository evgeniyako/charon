package mercs.algo.induce;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.core.MercsRun;
import mercs.core.MercsSettingsManager;
import mercs.core.SettingsMercs;
import mercs.data.MercsData;
import mercs.model.MercsModel;
import mercs.util.MercsException;

import java.io.IOException;

/**
 * MercsInductionAlgorithm
 *
 * Abstract class, representing this type (~kind) of algorithm.
 */
public abstract class MercsInductionAlgorithm {
    /**
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    protected ClusSchema schema;
    protected SettingsMercs settings;
    protected MercsSettingsManager manager;
    protected MercsData data;

    private long samplesProcessed;

    /**
     * Returns the schema of this object
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema to the given schema
     */
    protected void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Checks whether the given schema is valid. It is valid if and only if the
     * given schema is not null
     */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }

    /**
     * Returns the settings given in the settings file (.s).
     *
     * @return The settings object.
     */
    public SettingsMercs getSettings() {
        return settings;
    }
    /**
     * Sets the settings to the given settings
     */
    protected void setSettings(SettingsMercs settings) {
        this.settings = settings;
    }
    /**
     * Checks whether the given settings are valid. It is valid if and only if
     * it is not null
     */
    public boolean isValidSettings(SettingsMercs settings) {
        return settings != null;
    }

    /**
     * Returns the settings manager
     */
    public MercsSettingsManager getSettingsManager() {
        return manager;
    }
    /**
     * Sets the settings manager to the given manager
     */
    protected void setSettingsManager(MercsSettingsManager manager) {
        this.manager = manager;
    }

    /**
     * Returns the data that will be used to induce a model
     */
    public MercsData getData() {
        return data;
    }

    /**
     * Returns the samples processed
     */
    public long getSamplesProcessed() {
        return samplesProcessed;
    }
    /**
     * Sets the amount of samples processed
     */
    protected void setSamplesProcessed(long samplesProcessed) {
        this.samplesProcessed = samplesProcessed;
    }


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Initializes the induction algorithm with the given ClusSchema and SettingsMercs, meaning:
     *
     * Setting the ClusSchema and MercsSettings properties
     * Setting samples processed to 0
     * Creating a MercsSettingsManager (which is also a property of this class)
     */
    public MercsInductionAlgorithm(ClusSchema schema, SettingsMercs sett) throws MercsException {
        if (!isValidSchema(schema) || !isValidSettings(sett)) {
            throw new MercsException("Invalid schema or settings");
        }
        setSchema(schema);
        setSettings(sett);
        setSamplesProcessed(0);
        setSettingsManager(new MercsSettingsManager(schema, sett));
    }

    /**
     * Induces a MercsModel, using the provided data and this algorithm.
     *
     * As input, we have a MercsRun. (MercsRun provides easy access to relevant information)
     *
     * First, we induce an unpruned MercsModel, by using induceSingleUnpruned method.
     *
     * Then we prune, using the prune method of this class.
     */
    public MercsModel induce(MercsRun mr) throws ClusException, IOException {
        MercsModel unpruned = induceSingleUnpruned(mr);
        return prune(unpruned, mr);
    }

    /**
     * Induces a single unpruned model using this algorithm (abstract method)
     */
    public abstract MercsModel induceSingleUnpruned(MercsRun mr) throws ClusException, IOException;

    /**
     * Prunes a model given the mercsRun to access the prune data
     * TODO: mercsRun does not contain prune set yet
     * TODO: This pruning is not implemented currently
     *
     * @param model
     *            The model to prune
     * @param mr
     *            The mercsRun containing the prune data
     * @return Returns a pruned model
     */
    public MercsModel prune(MercsModel model, MercsRun mr) {
        // Should be overloaded when pruning is implemented in subclasses
        return model;
    }

    /**
     * Increments the amount of samples processed
     */
    public void incrementSamplesProcessed() {
        setSamplesProcessed(getSamplesProcessed() + 1);
    }

}
