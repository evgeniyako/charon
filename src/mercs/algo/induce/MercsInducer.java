package mercs.algo.induce;

import java.io.IOException;
import java.util.List;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.algo.selection.SelectionAlgorithm;
import mercs.algo.type.MercsInductionAlgorithmType;
import mercs.core.MercsRun;
import mercs.core.SettingsMercs;
import mercs.data.MercsData;
import mercs.model.MercsModel;
import mercs.model.MultiDirectionalEnsembleModel;

/**
 * A multi-directional ensemble of decision tree learners.
 *
 * This is the *actual* Mercs so to say.
 *
 * This class guides the induction of all the individual models that compose
 * the Mercs system.
 *
 * This class is relevant on the level of the entire Mercs system, and kind of directs how the MDE-model
 * is to be built.
 *
 * VFDT Omitted for now
 *
 * TODO: Fix the settings, the verbosity value does not make it from file to actual settings!
 *
 */
public class MercsInducer extends MercsInductionAlgorithm {

    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    /**
     * Two extra compared to superclass MercsInductionAlgorithm
     * */
    private MercsInductionAlgorithmType baseType;   //TDIT (or VFDT in the future)
    private SelectionAlgorithm selectionAlgorithm;  // How to select the different models

    /**
     * Returns the base type of the ensembles
     */
    public MercsInductionAlgorithmType getBaseType() {
        return baseType;
    }
    /**
     * Sets the base type of the ensembles
     */
    private void setBaseType(MercsInductionAlgorithmType baseType) {
        this.baseType = baseType;
    }

    /**
     * Returns the selection algorithm that is used
     */
    public SelectionAlgorithm getSelectionAlgorithm() {
        return selectionAlgorithm;
    }
    /**
     * Sets the selection algorithm of the ensembles
     */
    private void setSelectionAlgorithm(SelectionAlgorithm selectionAlgorithm) {
        this.selectionAlgorithm = selectionAlgorithm;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this algorithm with the given schema and settings. Based on
     * these settings the right selection algorithm and base type (e.g. TDIT or VFDT) of the
     * ensemble are chosen.
     */
    public MercsInducer(ClusSchema schema, SettingsMercs sett) throws ClusException, IOException {
        super(schema, sett); 								                    // Calling the constructor of the super class

        setBaseType(getSettingsManager().getBaseType());                        // Use settingsManager to set BaseType (which type on inducer do we use, eg.: TDIT or VFDT)
        setSelectionAlgorithm(getSettingsManager().getSelectionAlgorithm());    // Use settingsManager to set SelectionAlgorithm (how does the selection of models go)
    }

    /**
     * createData
     *
     * Initialises the data that is to be used for learning, (i.e. filling the data property of this object).
     *
     * What kind of stream it is, depends on the settings.
     *
     * This method relies in a great part on the MercsSettingsManager and its methods.
     */
    protected void createData(MercsRun mr) throws IOException, ClusException {
        // If we have not got data, we create the sets ourselves (this happens when we stream)
        if (mr.getData() == null) {
            getSettingsManager().createPruneSetAndTrainSetInMercsRun(mr);
        }
        // Otherwise, we just load it from the data already present in the MercsRun
        this.data = mr.getData();
        mr.setTestData(getSettingsManager().getTestData(mr));
    }

    /**
     * induceSingleUnpruned
     *
     * Induces a single, unpruned model using this algorithm.
     *
     * Extends the abstract method of the parent.
     */
    @Override
    public MultiDirectionalEnsembleModel induceSingleUnpruned(MercsRun mr)
            throws ClusException, IOException
    {
        // If there is no data, create some data
        if (getData() == null) { createData(mr); }

        // Create an MDE Model, and fill it up with individual models
        MultiDirectionalEnsembleModel MDEModel = new MultiDirectionalEnsembleModel(getSchema(), getSettingsManager());

        while (!getSelectionAlgorithm().isModelFinished(MDEModel)) {
            /*
            The selection algorithm provides (builds!) us with the trees (~models) that the Mercs model should contain. Notice that some selection
            algorithms work in an iterative way, so this while-loop can be executed multiple times, each time with a different list of trees to be induced.
            */
            List<ClusSchema> nextTrees = getSelectionAlgorithm().getNextTrees(MDEModel);

            // Load test data to evaluate the tree; if no test data is provided, use training data
            MercsData testData = getSettings().isNullTestFile() ? mr.getData() :
                    getSettingsManager().getTestData(mr);

            /*
            The inducer is what actually builds the trees. We are currently in an inducer for the Mercs model. This means that
            rather then inducing the actual trees, we coordinate this process. We are managing the induction process on the Mercs-level. Classes of the same package
            as the current class take care of that lower-level induction.
            */
            MercsInductionAlgorithm inducer;

            for (ClusSchema newSchema : nextTrees) {
                System.out.println("Start inducing one of the models");
                /*
                    Start a new inducer to build the models.
                    This obviously depends of the baseType, and needs schema and settings.

                    Now, just by looking at the classes, two things could theoretically happen.
                    1) Either a MercsInducer is created, that handles stuff on the ensemble level (which is the same as the current object) or
                    2) A DepthFirstInduceDecisionTreeWrapper is created, that is concerned with individual trees.

                    These two levels happen, perhaps surprisingly in exactly the same place in the code. However, the solution
                    is managed -perhaps surprisingly- in the getBaseType method. This method automatically ensures that we end up in the individual
                    induction level. In much the same way, the startRun() method of the main class of the Mercs system is what actually ensures that we first end up in
                    this Mercs-level. So to logical order 1) mercs 2) individual trees is always guaranteed, but the way in which this is done is rather intricate.

                    To create a Mercs-level inducer like this one, it should be done explicitly, which is exactly what happens in
                    the ModelBuilder class.

                                        MercsInductionAlgorithmType                                 [algo/type]
                                        |                       |
                        MercsDecisionTreeClassifier         MultiDirectionalEnsembleClassifier      [algo/type] (one of these two gets returned by getBaseType)
                                creates                                 creates
                        DepthFirstInduceDecisionTreeWrapper         MercsInducer (current class)    [algo/induce]

                    So, the first of the three choices in the Mercs system (base, selection, prediction) has been distributed
                    over these two packages.
                */
                inducer = getBaseType().createInduce(newSchema, getSettings());         // Create the single-model inducer (DepthFirstInduceDecisionTreeWrapper or VFDT counterpart)
                MercsModel newModel = inducer.induce(mr);                               // Build an individual model (because of getBaseType, this is the individual case!)
                newModel.calculatePerformance(testData);                                // Calculate and store performance of tree
                MDEModel.addModel(newModel);                                            // Add the model to the ensembleModel
            }

        }

        // Apply pruning (or at least some kind of pruning)
        MDEModel = getSelectionAlgorithm().finalPrune(MDEModel);


        // Print out more information, if wanted. This is a tiny summary of the different models and their accuracy.
        if(settings.getVerbose() > 0) {
            System.out.println("\n" + "List of models and their respective accuracies, using the getModelError method of CombinedPerformance class." + "\n" );
            for (MercsModel model : MDEModel.getModels()) {
                //System.out.println(model.getCombinedPerformance().getModelError() + " " + model.getModelTargetAttributesList());
                System.out.println("Model {"+ model.getModelDescriptiveAttributesList() + "-->" + model.getModelTargetAttributesList()
                        + "} has acc = " + model.getCombinedPerformance().getModelError());
            }
        }

        // return the constructed model (MultiDirectionalEnsembleModel)
        return MDEModel;
    }
}
