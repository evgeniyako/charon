package mercs.algo.selection;

import java.io.IOException;
import java.util.List;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import jeans.util.IntervalCollection;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.util.MercsException;


/**
 * This selection algorithm is the most basic selection possible. It returns a
 * ClusSchema for every possible pair of attribute combinations, one input
 * variable and one target variable. Thus the selection algorithm will give
 * N*(N-1)/2 ClusSchemas in total.
 *
 *
 */

public class NaiveSelectionAlgorithm implements SelectionAlgorithm{

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    private ClusSchema schema;
    /**
     * Used to track the next input-target pair
     */
    private int input, target;

    /**
     * Returns the schema
     */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the schema of this algorithm to the given schema
     */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Checks whether the given schema is valid. It is valid if and only if the
     * given schema is not equal to null.
     */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this algorithm with the given schema if it is valid,
     * otherwise a MercsException is thrown.
     *
     * Sets input   = 1
     *      output  = 2
     *
     *      As a starting point for the first iteration
     *
     */
    public NaiveSelectionAlgorithm(ClusSchema schema) throws MercsException {
        if (!isValidSchema(schema)) {
            throw new MercsException("Invalid schema");
        }
        setSchema(schema);
        input = 1;
        target = 2;
    }

    /**
     * Returns a ClusSchema containing one input and one target attribute.
     *
     * This ClusSchema is what more or less constitutes the model, so this method basically translates
     * our situation, given by the input and target integers to a ClusSchema that we can actually work with
     *
     */
    public ClusSchema getNextInputToTargetAttributes(MultiDirectionalEnsembleModel currentModel)
            throws ClusException, IOException {
        if (isModelFinished(currentModel)) {                                        // This protects us from to many iterations
            throw new MercsException("Model is already finished");
        }
        ClusSchema nextSchema = getSchema().cloneSchema();                          // We obtain the ClusSchema from the current object
        nextSchema.clearAttributeStatusClusteringAndTarget();                       // We wipe the attribute statuses (they are not up to date)
        nextSchema.setDescriptive(new IntervalCollection(Integer.toString(input))); // We set the descriptive (input) attributes    (from the properties of this object)
        nextSchema.setTarget(new IntervalCollection(Integer.toString(target)));     // We set the target (output) attributes        (from the properties of this object)
        nextSchema.setClustering(new IntervalCollection(Integer.toString(target))); // We set the clustering (output) attributes    (identical to target in this case)
        nextSchema.updateAttributeUse();                                            // We update the schema
        nextSchema.addIndices(ClusSchema.ROWS);

        updateInputAndTarget();                                                     // We update our in and output indices.

        return nextSchema;
    }

    /**
     * Updates the input and target variables for the next algorithm iteration.
     *
     * TODO: I think this method can go to impossible values of target and input.
     */
    private void updateInputAndTarget() {
        if (target == getSchema().getNbAttributes()) {      // If target is at maximum value, increase the input and reset target to 1
            ++input;
            target = 1;
        } else {                                            // Else, we increase the target
            ++target;
        }

        if (target == input) {          // If they're equal, do a recursive call.
            updateInputAndTarget();     // They cannot be equal, so we refrain from action and instead just jump to the next step, updating the target again.
        }
    }

    /**
     * The model is finished if and only if each pair of attributes (one input,
     * one target) has been generated. Thus after N*(N-1)/2 calls to
     * getNextInputToTargetAttributes.
     *
     * In practice, this means that the input property of this object should already have been set to an impossible value
     *
     * TODO: This seems like a weird way to verify this. This can already be done at the level of the updateInputAndTarget method
     */
    @Override
    public boolean isModelFinished(MultiDirectionalEnsembleModel currentModel) {
        return input == getSchema().getNbAttributes() + 1;
    }

    /*
     * =======================================
     * DUMMY METHODS
     * =======================================
     */

    /**
     * Get the next trees in de MDE-model.
     * TODO: No implementation yet
     * */
    public List<ClusSchema> getNextTrees(MultiDirectionalEnsembleModel currentModel) throws ClusException, IOException {
        return null;
    }

    /**
     * Do a final pruning of the MDE-model
     * TODO: No implementation yet
     * */
    public MultiDirectionalEnsembleModel finalPrune(MultiDirectionalEnsembleModel currentModel){
        return currentModel;
    }


}
