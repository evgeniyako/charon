package mercs.algo.selection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import clus.data.type.NominalAttrType;
import clus.data.type.NumericAttrType;
import clus.error.ClusErrorList;
import clus.statistic.ClusStatistic;
import clus.util.ClusException;
import mercs.algo.selection.metrics.F1Score;
import mercs.data.MercsData;
import mercs.model.ClusModelWrapper;
import mercs.model.MercsModel;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.statistics.variabletargetvoting.VTVCombinedStat;

/**
 * Class that gives a performance measure for the Mercs-model as a whole.
 * */
public class EnsemblePerformance {

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    protected MultiDirectionalEnsembleModel model;
    protected NominalAttrType[] nomAttrs;
    protected NumericAttrType[] numAttrs;
    protected Map<NominalAttrType, F1Score> nomPerformance;
    protected ClusErrorList errorList;
    protected F1Score f1Score;
    protected ExplainedVarianceScore explainedVariance;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     * */
    public EnsemblePerformance(MercsModel mercsModel, ClusSchema schema) {
        model = (MultiDirectionalEnsembleModel) mercsModel;                     // The MDE-model that we want to assess
        nomAttrs = schema.getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET);      // Fetch the TARGET nominal attributes (which is a (list of) ClusAttrType objects)
        numAttrs = schema.getNumericAttrUse(ClusAttrType.ATTR_USE_TARGET);      // Fetch the TARGET numerical attributes

        ClusErrorList errorList = new ClusErrorList();                          // Errorlist is what is needed to actually compute the scores below
        f1Score = new F1Score(errorList, nomAttrs);                             // F1 Score - metric
        explainedVariance = new ExplainedVarianceScore(errorList, numAttrs);    // Explained variance - metric
    }

    /*
    * Mapping indices of attributes to predictions, weights or both
    * These methods build the structures to hold those items
    * */

    /**
     * Returns a map to hold the ensemble predictions for numeric attributes.
     * The returned structure maps attribute indexes to an other map. This other
     * map has predictions and their corresponding weight, higher weight for
     * predictions coming from better trees.
     */
    private Map<Integer, HashMap<Integer, Double>> initPredictions() {
        HashMap<Integer, HashMap<Integer, Double>> predictions = new HashMap<Integer, HashMap<Integer, Double>>(
                nomAttrs.length);
        for (int i = 0; i < nomAttrs.length; i++) {
            predictions.put(nomAttrs[i].getIndex(), new HashMap<Integer, Double>());
        }

        return predictions;
    }

    /**
     * Returns a map to hold the ensemble predictions for numeric attributes.
     * The returned structure maps attribute indexes to their corresponding predictions
     */
    private Map<Integer, ArrayList<Double>> initNumPredictions() {
        HashMap<Integer, ArrayList<Double>> numPredictions = new HashMap<Integer, ArrayList<Double>>(numAttrs.length);
        for (int i = 0; i < numAttrs.length; i++) {
            numPredictions.put(numAttrs[i].getIndex(), new ArrayList<Double>());
        }

        return numPredictions;
    }

    /**
     * Returns a map to hold the ensemble weights for certain predictions of numeric attributes.
     * The returned structure maps attribute indexes to the weights of the corresponding prediction.
     */
    private Map<Integer, ArrayList<Double>> initNumPredictionsWeights() {
        HashMap<Integer, ArrayList<Double>> numPredictionsWeights = new HashMap<Integer, ArrayList<Double>>();
        for (int i = 0; i < numAttrs.length; i++) {
            numPredictionsWeights.put(numAttrs[i].getIndex(), new ArrayList<Double>());
        }

        return numPredictionsWeights;
    }

    /*
    * Performance tests
    * */

    /**
     * Weighted majority vote for nominal attributes.
     * */
    public void testModelwithMajorityVote(MercsData data) throws IOException, ClusException {

        data.init();                                                                        // Get our data ready

        while (data.hasNextTuple()) {                                                       // While we still have some data, do:
            /*
            These maps come from the dedicated methods to collect relevant info about the
            predictions that our MercsModel makes on the selected tuple. We need to collect
            these predictions and their weights in these structures because there are many components.
            */
            Map<Integer, HashMap<Integer, Double>> predictions = initPredictions();
            Map<Integer, ArrayList<Double>> numPredictions = initNumPredictions();
            Map<Integer, ArrayList<Double>> numPredictionsWeights = initNumPredictionsWeights();

            DataTuple sample = data.readNextTuple();                                        // Fetch one tuple

            // System.out.println(sample);
            for (int j = 0; j < model.getModels().size(); j++) {
                MercsModel tree = model.getModels().get(j);

                ClusStatistic pred = tree.predict(sample);
                NominalAttrType[] tNomAttrs = tree.getSchema().getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET);
                if (tNomAttrs.length > 0) {
                    int[] predicted = pred.getNominalPred();
                    for (int i = 0; i < tNomAttrs.length; i++) {
                        int tNomAttrIdx = tNomAttrs[i].getIndex();
                        double currentValue = predictions.get(tNomAttrIdx).containsKey(predicted[i])
                                ? predictions.get(tNomAttrIdx).get(predicted[i]) : 0.0;
                        double newValue = currentValue
                                + tree.getCombinedPerformance().getModelErrorByAttributeIndex(tNomAttrIdx);
                        // System.out.println("Current value: " + currentValue +
                        // ", added " +
                        // t.getCombinedPerformance().getModelErrorByAttributeIndex(tNomAttrIdx));
                        predictions.get(tNomAttrIdx).put(predicted[i], newValue);
                        // System.out.println("Added " + predicted[i] + " to
                        // index " + tNomAttrIdx + " while real value is " +
                        // tNomAttrs[i].getNominal(sample));
                    }
                }

                NumericAttrType[] tNumAttrs = tree.getSchema().getNumericAttrUse(ClusAttrType.ATTR_USE_TARGET);
                if (tNumAttrs.length > 0) {
                    double[] predicted = pred.getNumericPred();
                    for (int i = 0; i < tNumAttrs.length; i++) {
                        int tNumAttrIdx = tNumAttrs[i].getIndex();
                        numPredictions.get(tNumAttrIdx).add(predicted[i]);
                        numPredictionsWeights.get(tNumAttrIdx)
                                .add(tree.getCombinedPerformance().getModelErrorByAttributeIndex(tNumAttrIdx));
                    }
                }
            }

			/*
			 * Aggregate nominal predictions.
			 */
            HashMap<Integer, Integer> finalPredictions = new HashMap<Integer, Integer>();
            for (int i = 0; i < nomAttrs.length; i++) {
                double max = 0;
                int maxValue = 0;
                for (int val : predictions.get(nomAttrs[i].getIndex()).keySet()) {
                    if (predictions.get(nomAttrs[i].getIndex()).get(val) > max) {
                        maxValue = val;
                        max = predictions.get(nomAttrs[i].getIndex()).get(val);
                    }
                }
                finalPredictions.put(nomAttrs[i].getIndex(), maxValue);
            }

            int[] trueValues = new int[nomAttrs.length];
            int[] predValues = new int[nomAttrs.length];
            for (int i = 0; i < nomAttrs.length; i++) {
                trueValues[i] = nomAttrs[i].getNominal(sample);
                predValues[i] = finalPredictions.get(nomAttrs[i].getIndex());
            }

            f1Score.addExample(trueValues, predValues);

			/*
			 * Aggregate numeric predictions.
			 */
            HashMap<Integer, Double> finalNumPredictions = new HashMap<Integer, Double>();
            for (int i = 0; i < numAttrs.length; i++) {
                int numAttrIdx = numAttrs[i].getIndex();
                double sum = 0.0;
                double sumWeights = 0.0;
                for (int j = 0; j < numPredictions.get(numAttrIdx).size(); j++) {
                    double weight = numPredictionsWeights.get(numAttrIdx).get(j);
                    sum += numPredictions.get(numAttrIdx).get(j) * weight;
                    sumWeights += weight;
                    j++;
                }
                if (sumWeights == 0) {
                    sumWeights = 1;
                }
                double avg = sum / sumWeights;
                finalNumPredictions.put(numAttrIdx, avg);
            }

            double[] trueNumValues = new double[numAttrs.length];
            double[] predNumValues = new double[numAttrs.length];
            for (int i = 0; i < numAttrs.length; i++) {
                trueNumValues[i] = numAttrs[i].getNumeric(sample);
                predNumValues[i] = finalNumPredictions.get(numAttrs[i].getIndex());
            }

            explainedVariance.addExample(trueNumValues, predNumValues);
        }

        // EDIT VAN ELIA
        System.out.println("Explained Variance");
        for (Integer i : explainedVariance.getAllModelErrorsByIndex().keySet()) {
            System.out.println(i + ": " + explainedVariance.getModelErrorByAttributeIndex(i));
        }
        System.out.println("f1score");
        for (Integer i : f1Score.getAllModelErrorsByIndex().keySet()) {
            System.out.println(i + ": " + f1Score.getModelErrorByAttributeIndex(i));
        }

    }

    public void testModel(MercsData data) throws IOException, ClusException {
        data.init();

        while (data.hasNextTuple()) {                                                   // While we still have datatuples to predict
            HashMap<Integer, ArrayList<Integer>> predictions =                          // Hashmap of data predictions
                    new HashMap<Integer, ArrayList<Integer>>(nomAttrs.length);
            for (int i = 0; i < nomAttrs.length; i++) {
                predictions.put(nomAttrs[i].getIndex(), new ArrayList<Integer>());
            }

            DataTuple sample = data.readNextTuple();                                    // Collect one sample of data
            // System.out.println(sample);
            for (MercsModel tree : model.getModels()) {
                ClusModelWrapper t = (ClusModelWrapper) tree;
                ClusStatistic pred = tree.predict(sample);                              // Use one of the components of the Mercs-Model (it is called tree here but can be an ensemble too) to predict
                NominalAttrType[] tNomAttrs = t.getSchema().getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET);
                if (tNomAttrs.length > 0) {
                    int[] predicted = pred.getNominalPred();
                    for (int i = 0; i < tNomAttrs.length; i++) {
                        int tNomAttrIdx = tNomAttrs[i].getIndex();
                        predictions.get(tNomAttrIdx).add(predicted[i]);
                        System.out.println("Added " + predicted[i] + " to index " + tNomAttrIdx + " while real value is " + tNomAttrs[i].getNominal(sample));
                    }
                } else {
                    // System.out.println("No nominal attributes");
                }
            }

            // System.out.println(predictions);
            HashMap<Integer, Integer> finalPredictions = new HashMap<Integer, Integer>();
            for (int i = 0; i < nomAttrs.length; i++) {
                int[] predictionCount = new int[nomAttrs[i].getNbValues()];
                ArrayList<Integer> attrPredictions = predictions.get(nomAttrs[i].getIndex());
                for (int attrPrediction : attrPredictions) {
                    predictionCount[attrPrediction]++;
                }
                int max = 0;
                int maxValue = 0;
                for (int j = 0; j < nomAttrs[i].getNbValues(); j++) {
                    if (predictionCount[j] > max) {
                        max = predictionCount[j];
                        maxValue = j;
                    }
                }
                finalPredictions.put(nomAttrs[i].getIndex(), maxValue);
            }
            // System.out.println("Final predictions: " + finalPredictions);
            int[] trueValues = new int[nomAttrs.length];
            int[] predValues = new int[nomAttrs.length];
            for (int i = 0; i < nomAttrs.length; i++) {
                trueValues[i] = nomAttrs[i].getNominal(sample);
                predValues[i] = finalPredictions.get(nomAttrs[i].getIndex());
                // System.out.println("Prediction: " +
                // finalPredictions.get(nomAttrs[i].getIndex()));
                // System.out.println("Real value: " +
                // nomAttrs[i].getNominal(sample));
            }
            // System.out.println(Arrays.toString(trueValues));
            // System.out.println(Arrays.toString(predValues));
            f1Score.addExample(trueValues, predValues);
        }

        System.out.print(f1Score.getModelErrorComponent(0));
        System.out.print(f1Score.getModelErrorComponent(1));
        System.out.println(f1Score.getModelErrorComponent(2));
    }



    public void test3(MercsData data, ClusSchema relation) throws IOException, ClusException {

        data.init();
        while (data.hasNextTuple()) {

            // Get data tuple.
            DataTuple sample = data.readNextTuple();

            // Predict target attributes.
            VTVCombinedStat cs = (VTVCombinedStat) model.predict(sample, relation);

            // Aggregate nominal predictions.
            NominalAttrType[] nom = relation.getNominalAttrUse(ClusAttrType.ATTR_USE_TARGET);
            if (nom.length > 0) {
                assert nom == cs.getNominalAttr();

                int[] trueValues = new int[nom.length];
                int[] predValues = cs.getNominalPred();

                for (int i = 0; i < nom.length; i++) {
                    trueValues[i] = nom[i].getNominal(sample);
                }

                f1Score.addExample(trueValues, predValues);
            }

            // Aggregate numeric predictions.
            NumericAttrType[] num = relation.getNumericAttrUse(ClusAttrType.ATTR_USE_TARGET);
            if (num.length > 0) {
                assert num == cs.getNumericAttr();

                double[] trueNumValues = new double[num.length];
                double[] predNumValues = cs.getNumericPred();
                for (int i = 0; i < num.length; i++) {
                    trueNumValues[i] = num[i].getNumeric(sample);
                }

                explainedVariance.addExample(trueNumValues, predNumValues);
            }
        }

        if (nomAttrs.length > 0) {
            System.out.println("F1 results:");
            for (ClusAttrType at : nomAttrs) {
                System.out.println("\tAttr " + (at.getIndex() + 1) + ": "
                        + f1Score.getModelErrorByAttributeIndex(at.getIndex()));
            }
        }
        if (numAttrs.length > 0) {
            System.out.println("Explained variance results:");
            for (ClusAttrType at : numAttrs) {
                System.out.println("\tAttr " + (at.getIndex() + 1) + ": "
                        + explainedVariance.getModelErrorByAttributeIndex(at.getIndex()));
            }
        }

    }




}
