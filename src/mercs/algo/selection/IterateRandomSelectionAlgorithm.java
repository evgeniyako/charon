package mercs.algo.selection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import clus.data.type.ClusSchema;
import clus.main.Settings;
import clus.util.ClusException;
import mercs.algo.selection.metrics.AvgAttributePerformance;
import mercs.algo.selection.metrics.MercsModelPerformance;
import mercs.core.MercsSettingsManager;
import mercs.core.SettingsMercs;
import mercs.model.MercsModel;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.util.MercsException;

/**
 * IterateRandomSelectionAlgorithm
 *
 * This selection algorithm selects M random trees.
 *
 * A random tree = A tree for which a random selection of input and out attributes happened.
 *
 * After selecting M trees, we check if every attribute appears at least once as
 * output attribute. When not all possible output attributes are present,
 * we add the absent ones to the set of output attributes of randomly selected trees.
 *
 * For every randomly selected tree, a ClusSchema is returned.
 *
 * The maximum amount of output attributes is 3. There is no limit on the amount
 * of input attributes. The algorithm selects M = 2 * N trees, where N is the
 * amount of attributes.
 *
 */
public class IterateRandomSelectionAlgorithm implements SelectionAlgorithm{

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    private MercsSettingsManager mercsSettingsManager;
    private ClusSchema schema;

    private Random random;
    private int M, maxInputAttributes, maxOutputAttributes, totalAttributes;
    private int iteration, maxIterations;
    private HashSet<SelectionAlgorithmTree> modelTrees;
    private double[] attrWeights, equalWeights;
    private double weightPerformancePower, weightOutputAppearancePower;
    private boolean shouldBaseWeightsOnPerformance, shouldUseRelativeError, shouldUpdateWeights,
            shouldKeepAtLeastOneOutput, shouldHaveAllAtStart, shouldDivideWeightsByOutputAppearance;

    /**
     * Return the current schema (ClusSchema)
     * */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Set the current schema (ClusSchema)
     * */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Check validity of the current schema
     * */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }

    /**
     * Get the MercsSettinsManager
     * */
    public MercsSettingsManager getMercsSettingsManager() {
        return mercsSettingsManager;
    }
    /**
     * Set the MercsSettinsManager
     * */
    private void setMercsSettingsManager(MercsSettingsManager mgr) {
        this.mercsSettingsManager = mgr;
    }
    /**
     * Check validity the MercsSettinsManager
     * */
    public boolean isValidMercsSettingsManager(MercsSettingsManager mgr) {
        return mgr != null;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this algorithm with the given schema if it is valid (and valid settingsmanager),
     * otherwise a MercsException is thrown.
     */
    public IterateRandomSelectionAlgorithm(ClusSchema schema, MercsSettingsManager mgr, int iterations)
            throws MercsException {

        if (!isValidSchema(schema))
            throw new MercsException("Invalid schema");
        if (!isValidMercsSettingsManager(mgr))
            throw new MercsException("Invalid mercs settings manager");

        // Settings all the properties
        setSchema(schema);
        setMercsSettingsManager(mgr);
        totalAttributes = schema.getNbAttributes();
        M = 2 * totalAttributes;                            // If there are N attributes, we select M = 2 * N trees

        maxInputAttributes = totalAttributes - 1;
        maxOutputAttributes = 3;
        iteration = 0;
        maxIterations = iterations;
        modelTrees = new HashSet<SelectionAlgorithmTree>();
        attrWeights = new double[totalAttributes];          // The real weight vector (initialize with dummy values)
        Arrays.fill(attrWeights, 1.0);
        equalWeights = new double[totalAttributes];          // The dummy weight vector
        Arrays.fill(equalWeights, 1.0);

		/* Options */
        shouldKeepAtLeastOneOutput = true;
        shouldHaveAllAtStart = true;
        shouldUseRelativeError = true;

        shouldUpdateWeights = true;
        shouldBaseWeightsOnPerformance = false;
        weightPerformancePower = 2;
        shouldDivideWeightsByOutputAppearance = true;
        weightOutputAppearancePower = 4;

        // If we have provided a random seed in the Settings, we fetch it here.
        if (getMercsSettingsManager().getSettings().getRandomSeed() != 0) {
            random = new Random(getMercsSettingsManager().getSettings().getRandomSeed());
        } else {
            random = new Random();
        }
    }

    /*
    * Methods directly related to the trees
    * */

    /**
     * Method that returns the next (~next iteration) of the algorithm trees in the MDE-model.
     * It returns them as an ArrayList of ClusSchemas, which for most purposes ARE the actual trees.
     *
     * The ClusSchema's give all the necessary information for the actual learning of these trees later on.
     * In that sense, the ClusSchema's define the trees.
     */
    public List<ClusSchema> getNextTrees(MultiDirectionalEnsembleModel currentModel) throws ClusException, IOException {
        MercsModelPerformance modelPerf = new AvgAttributePerformance();    // Some performance measures.
        modelPerf.getMercsModelPerformance(currentModel);

        if (isModelFinished(currentModel)) {
            return new ArrayList<ClusSchema>();
        }                           // If the model is finished (no next trees), we return an empty ArrayList

        /*
        - Analyse (check size)
        - Prune model (helper method:  pruneMercsModel)
        - Update the weights (helper method: updateWeights)

            NB.: TODO: When using stream: check how many samples you have seen, do you need to analyse the model?
        */
        if (currentModel.getModelSize() > 0) {              // If the model has a non-zero size, we prune the MDE-model.
            pruneMercsModel(currentModel);
            if (shouldUpdateWeights)                        // Check if the weights need updating
                updateWeights(currentModel);
        }

        ++iteration;

        if (mercsSettingsManager.getSettings().getVerbose() > 2) {
            System.out.println("Selecting new trees to learn...");
        }       // Say something in the terminal if necessary.

        boolean allAttributesAsOutput = (shouldHaveAllAtStart && iteration == 1) ? true : false;            // In the first iteration, every att. should be the output at least once.
        List<ClusSchema> returnList = selectRandomTrees(M - currentModel.getModelSize(), maxInputAttributes,// Select new random trees, keeping number total at M
                maxOutputAttributes, allAttributesAsOutput);

        return returnList;
    }

    /**
     * selectRandomTrees
     *
     * Important method of this class, this is where we actually select a collection of trees in a random fashion.
     *
     * These trees also get added to the modelTrees property of this class.
     * */
    private List<ClusSchema> selectRandomTrees(int M, int maxIn, int maxOut, boolean allAttributesAsOutput) {
        List<SelectionAlgorithmTree> treeList = new ArrayList<SelectionAlgorithmTree>(M);                    // List of abstract representation (~keeping inputs and outputs as ints) of trees

        // 1) Build M trees

        for (int i = 0; i < M; i++) {                                                   // For each of our M =2N trees, do:
            SelectionAlgorithmTree tree = new SelectionAlgorithmTree();                 // New abstract representation of a tree

            double[] weights = attrWeights.clone();                                     // Each attr has a weight, that determines how likely the selection of this attr is.
                                                                                        // NB.: In initialization, all weights are equal!

            int out = randomBetween(1, maxOut);                                         // Select output attributes (using helper method randomBetween to fix amount of outputs)
            for (int j = 0; j < out; j++) {
                int selectedAttribute = selectRandomAttributeWithRoulette(weights);     // For the actual selection, we rely on helper method (selectRandomAttributeWithRoulette(weights))
                tree.addOutAttribute(selectedAttribute + 1);                            // Add this attribute to the output of the tree (~SelectionAlgorithmTree object)
                weights[selectedAttribute] = 0;                                         // Set the weight to zero, so that this output cannot be repicked (eg: no tree [x]->[2,2])
            }

            double[] inWeights = equalWeights.clone();                                  // Create an Array for the input weights, clone the equalWeights Array.
            for (int idx : tree.getOutAttributes()) {                                   // If an attribute is already an output attribute, we must make sure that its weight is zero
                inWeights[idx - 1] = 0;                                                 // Eg.: No tree [2]->[2]
            }

            int in = randomBetween(1, Math.min(maxIn, totalAttributes - out));          // Select input attributes (using helper method randomBetween to fix amount of inputs)
            for (int j = 0; j < in; j++) {
                int selectedAttribute = selectRandomAttributeWithRoulette(inWeights);   // For the actual selection, we rely on helper method (selectRandomAttributeWithRoulette(weights)
                tree.addInAttribute(selectedAttribute + 1);                             // Add this attribute to the input of the tree (~SelectionAlgorithmTree object)
                inWeights[selectedAttribute] = 0;                                       // Set the weight to zero, so that this input cannot be repicked (eg: no tree [1,1]->[x])
            }

            // do not learn a tree twice
            if (treeList.contains(tree)) {                                              // If the current selection (the treelist) has it, we throw it away
                i--;
                // System.out.println("Tree already in current selection: " + tree.getStringDescription());
            } else if (modelTrees.contains(tree)) {                                     // If it is in the modelTrees (if it is selected before), we throw it away
                i--;
                // System.out.println("Tree already in current selection: " + tree.getStringDescription());
            } else {                                                                    // If none of the above, we add it to the list of trees (treeList, local variable)
                treeList.add(tree);
                // System.out.println("Added: " + tree.getStringDescription());
            }
        }

        // 2) Assign attributes which are not yet selected as output to a random tree, as output

        if (allAttributesAsOutput) {
            // assign attributes which are not yet selected as target randomly
            // to a tree,
            // if attribute appears as input for the selected tree, remove it as
            // input (only keep it as output)
            List<Integer> notSelectedAsOutput = getAttributesNotSelectedAsOutput(treeList); // We fetch these overlooked attributes by a dedicated helper method (getAttributesNotSelectedAsOutput(treeList))
            // System.out.println("Not selected as output: " + notSelectedAsOutput.toString());
            if (notSelectedAsOutput.size() > 0) {
                for (int i = 0; i < notSelectedAsOutput.size(); i++) {
                    int randomTreeNumber = randomBetween(0, treeList.size() - 1);
                    // System.out.println("Adding " + notSelectedAsOutput.get(i) + " to tree " + randomTreeNumber);
                    SelectionAlgorithmTree selectedRandomTree = treeList.get(randomTreeNumber);
                    selectedRandomTree.addOutAttribute(notSelectedAsOutput.get(i));         // Add the overlooked output to a randomly selected tree

                    if (selectedRandomTree.hasInAttribute(notSelectedAsOutput.get(i))) {    // If this overlooked output happens to be in the input, we delete it
                        selectedRandomTree.removeInAttribute(notSelectedAsOutput.get(i));
                        if (selectedRandomTree.getInAttributes().size() == 0) {             // If that means we have no more inputs, we fix that (attr 1 or 2 will be chosen)
                            if (notSelectedAsOutput.get(i) == 1)
                                selectedRandomTree.addInAttribute(2);
                            else
                                selectedRandomTree.addInAttribute(1);
                        }
                        // System.out.println("Removing " + notSelectedAsOutput.get(i) + " as input from tree " + randomTreeNumber);
                    }
                }
            }
        }

        // System.out.println("Selected trees: " + treeList.toString());

        // 3) Finalize: Save the abstract trees en then create a list of ClusSchema's from the abstract trees

        modelTrees.addAll(treeList);

        List<ClusSchema> returnList = new ArrayList<ClusSchema>(M);
        for (SelectionAlgorithmTree tree : treeList) {
            ClusSchema newSchema = tree.toClusSchema(getSchema());
            returnList.add(newSchema);
        }

        return returnList;
    }

    /**
     * We count the amount of trees that has a certain output.
     * If the number is larger than one (i.e. more than one tree still predicts this output)
     * we drop the tree, because it *can* be dropped in principle (some other tree will still manage the output attribute)
     * */
    private boolean canTreeBeDropped(List<Integer> outAttributes) {
        Map<Integer, Integer> attributesAsOutputCount = getAttributesAsOutputOccurencesMap();       // Get the map output attr <-> amount of trees that predicts it
        for (int attr : outAttributes) {
            if (attributesAsOutputCount.get(attr) <= 1) {
                // System.out.println("This is last tree for attribute " + attr + ", it can't be dropped.");
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the amount of possible trees we can construct.
     * Cf. Thesis for more explanation or common sense.
     * */
    private double amountOfPossibleTrees() {
        return Math.pow(3, totalAttributes) - Math.pow(2, totalAttributes + 1) + 1;
    }

    /*
    * Helper methods (mainly concerned with attribute selection and weights)
    * */

    /**
     * Selects a random attribute based on the weights in the given array.
     * Attributes with a higher weight have a higher probability to get selected.
     * This is achieved using Roulette Wheel Selection.
     *
     * NB.: This method is identical to the one in RandomSelectionAlgorithm
     */
    private int selectRandomAttributeWithRoulette(double[] weights) {
        double[] cumulativeWeights = new double[weights.length];
        cumulativeWeights[0] = weights[0];
        for (int i = 1; i < weights.length; i++) {
            cumulativeWeights[i] = cumulativeWeights[i - 1] + weights[i];
        }

        double val = random.nextDouble() * cumulativeWeights[cumulativeWeights.length - 1];
        /*
        * binarySearch(double[] a, double key)
        * index of the search key, if it is contained in the array;
        * otherwise, (-(insertion point) - 1).
        * The insertion point is defined as the point at which the key would be inserted into the array:
        * the index of the first element greater than the key, or a.length if all elements in the array are less than the specified key.
        * Note that this guarantees that the return value will be >= 0 if and only if the key is found.
        * */
        int index = Arrays.binarySearch(cumulativeWeights, val);
        if (index < 0) {
            index = Math.abs(index + 1);
        }

        return index;
    }

    /**
     * Returns a list containing the attributes which are not yet used as output
     * attributes.
     */
    private List<Integer> getAttributesNotSelectedAsOutput(List<SelectionAlgorithmTree> treeList) {
        boolean[] selectedAsOutput = new boolean[totalAttributes];
        Arrays.fill(selectedAsOutput, false);

        for (SelectionAlgorithmTree tree : treeList) {
            for (int outAttr : tree.getOutAttributes()) {
                selectedAsOutput[outAttr - 1] = true;
            }
        }

        List<Integer> returnList = new ArrayList<Integer>();
        for (int i = 0; i < totalAttributes; i++) {
            if (!selectedAsOutput[i])
                returnList.add(i + 1);
        }

        return returnList;
    }

    /**
     * Get a map that connects the amount of times a given attribute has appeared as an output
     * */
    private Map<Integer, Integer> getAttributesAsOutputOccurencesMap() {
        Map<Integer, Integer> returnMap = new HashMap<Integer, Integer>();
        for (int i = 1; i <= totalAttributes; i++) {                    // Initialize the map with all zeroes
            returnMap.put(i, 0);
        }
        for (SelectionAlgorithmTree t : modelTrees) {                   // For every tree
            for (int attr : t.getOutAttributes()) {                     // For every output attribute
                returnMap.put(attr, returnMap.get(attr) + 1);           // Increase the counter with one in the correct attribute
            }
        }

        return returnMap;
    }

    /**
     * Per attribute, retrieve its best performance
     * */
    private Map<Integer, Double> getBestPerformancePerAttribute(MultiDirectionalEnsembleModel model) {
        Map<Integer, Double> returnMap = new HashMap<Integer, Double>();
        AvgAttributePerformance modelPerf = new AvgAttributePerformance();
        Map<Integer, ArrayList<Double>> idxToPerformancesMap = modelPerf.getMercsModelPerformance(model);   // This method returns all performances of a given attribute
        // System.out.println(idxToPerformancesMap);
        for (int key : idxToPerformancesMap.keySet()) {                                                     // We retrieve the maximum value out of the list of performances
            ArrayList<Double> attrPerformances = idxToPerformancesMap.get(Integer.valueOf(key));
            if (attrPerformances.size() > 0)
                returnMap.put(key, Collections.max(attrPerformances));
            else // if attribute does not appear in ensemble
                returnMap.put(key, 0.0);
        }
        // System.out.println(returnMap);
        // model.getSchema()
        return returnMap;
    }

    /**
     * Set new, updated weights for the different attributes
     * */
    private void updateWeights(MultiDirectionalEnsembleModel currentModel) {
        double[] newWeights = new double[totalAttributes];                                                      // Initialize the new weights array
        Arrays.fill(newWeights, 1.0);

        AvgAttributePerformance perf = new AvgAttributePerformance();                                           // Initialize a Hashmap from attribute to its performance
        HashMap<Integer, Double> avgAttributePerformanceMap = perf.getAvgAttributePerformanceMap(currentModel);

        // Option 1: Weights updated based on the performance of the attribute under consideration
        if (shouldBaseWeightsOnPerformance) {
            // System.out.println(perf.getMercsModelPerformance(currentModel));
            for (int key : avgAttributePerformanceMap.keySet()) {
                // System.out.println("Attribute " + key + " has avg performance
                // " + avgAttributePerformanceMap.get(key));
                newWeights[key] = Math.pow(1.0 - avgAttributePerformanceMap.get(key), weightPerformancePower);
            }
        }

        // System.out.println("After performance adjustment" +
        // Arrays.toString(newWeights));

        // Option 2: Weights updated based on their frequency of selection as an output attribute
        if (shouldDivideWeightsByOutputAppearance) {
            Map<Integer, Integer> attributesAsOutputCount = getAttributesAsOutputOccurencesMap();
            // System.out.println(attributesAsOutputCount);
            // System.out.println(avgAttributePerformanceMap);
            for (int key : avgAttributePerformanceMap.keySet()) {
                if (attributesAsOutputCount.get(key + 1) > 0)
                    newWeights[key] = newWeights[key]
                            / Math.pow(attributesAsOutputCount.get(key + 1), weightOutputAppearancePower);
            }
        }

        attrWeights = newWeights;
        // System.out.println("After output appearance adjustment: " +
        // Arrays.toString(newWeights));
    }

    /**
     * Returns a random number between min (inclusive) and max (inclusive)
     */
    private int randomBetween(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }


    /*
    * Methods about other things
    * */

    /**
     * If we have been through all the iterations of the algorithm, the MDE-model is finished.
     * */
    @Override
    public boolean isModelFinished(MultiDirectionalEnsembleModel currentModel) {
        return iteration == maxIterations;
    }

    /**
     * Prune the MDE-model (MultiDirectionalEnsembleModel)
     * */
    @Override
    public MultiDirectionalEnsembleModel finalPrune(MultiDirectionalEnsembleModel currentModel) {
        try {
            pruneMercsModel(currentModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentModel;
    }

    /**
     * Prune the MDE-model (MultiDirectionalEnsembleModel)
     * */
    public void pruneMercsModel(MultiDirectionalEnsembleModel currentModel) throws IOException, ClusException {
        Map<Integer, Double> bestPerformances = getBestPerformancePerAttribute(currentModel);                   // Get a map attribute -> Best Performance
        // prune based on accuracy of trees in the model
        // System.out.println("Pruning current model...");
        double lowestAccuracy = 1.0;
        int lowestAccuracyIndex = 0;
        boolean found = false;

        for (int i = 0; i < currentModel.getModelSize(); i++) {
            MercsModel model = currentModel.getModels().get(i);                                                 // Get one particular model from the MDE model
            // System.out.println("Relative model error: " +
            // model.getCombinedPerformance().getRelativeModelError(getBestPerformancePerAttribute(currentModel)));
            double accuracy;

            if (shouldUseRelativeError)                                                                         // Set the accuracy. Either relative or absolute
                accuracy = model.getCombinedPerformance().getRelativeModelError(bestPerformances);
            else
                accuracy = model.getCombinedPerformance().getModelError();

            if (mercsSettingsManager.getSettings().getVerbose() > 2)
                System.out.println(i + ": New Relative Accuracy: " + accuracy);

            if (accuracy < lowestAccuracy) {                                                                    // Check whether or not the current model can be discarded
                if (!shouldKeepAtLeastOneOutput || canTreeBeDropped(model.getModelTargetAttributesList())) {
                    lowestAccuracy = accuracy;
                    lowestAccuracyIndex = i;
                    found = true;
                }
            }
        }

        if (found) {                                                                                            // If we have found a model that could be removed
            MercsModel modelToRemove = currentModel.getModels().get(lowestAccuracyIndex);
            SelectionAlgorithmTree treeToRemove = new SelectionAlgorithmTree(                                   // Translate this model into a tree
                    modelToRemove.getModelDescriptiveAttributesList(), modelToRemove.getModelTargetAttributesList());
            if (mercsSettingsManager.getSettings().getVerbose() > 2) {
                System.out.println("Dropping tree " + lowestAccuracyIndex + ": " + treeToRemove + "\n Acc: " + lowestAccuracy);
            }

            modelTrees.remove(treeToRemove);                            // Remove the worst tree

            // remove the least good tree from the model
            currentModel.getModels().remove(lowestAccuracyIndex);       // Remove the worst tree
        } else {
            // System.out.println("None of the trees can be dropped...");
        }
    }

}
