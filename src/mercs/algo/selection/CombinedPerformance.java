package mercs.algo.selection;

import java.util.HashMap;
import java.util.Map;

import clus.data.rows.DataTuple;
import clus.data.type.NominalAttrType;
import clus.data.type.NumericAttrType;
import clus.error.ClusError;
import clus.error.ClusErrorList;
import clus.main.Settings;
import clus.statistic.ClusStatistic;
import mercs.algo.selection.metrics.F1Score;

/**
 * CombinedPerformance
 *
 * Combined Performance is am extension of the ClusError class.
 *
 * It is a performance measure of a tree which can have both numeric and nominal target attributes.
 * The resulting performance is an average of the performance of each target attribute.
 * For nominal attributes, the performance is the multi-class macro F1 score.
 * For numeric attributes, the performance is the explained variance score.
 *
 */

public class CombinedPerformance extends ClusError{

    /**
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    public final static long serialVersionUID = Settings.SERIAL_VERSION_ID;

    protected int num_length;
    protected int nom_length;

    // Stuff from Mercs
    protected ExplainedVarianceScore numericError;
    protected F1Score nominalError;

    // Stuff from Clus
    protected NumericAttrType[] numAttributes;
    protected NominalAttrType[] nomAttributes;
    protected ClusErrorList errList;

    /**
     * Returns the NomialError (which is an F1-score)
     * */
    public F1Score getNominalError() {
        return nominalError;
    }


    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /*
    * Basic methods
    * */

    /**
     * Constructor
     *
     * Initializes the superclass (ClusError) and sets own properties.
     *
     * Also creates a ClusErrorList, which in turn is used to initialize the two error properties (numeric and nominal).
     * These properties are instances of resp. ExplainedVarianceScore and F1Score classes.
     *
     *
     * */
    public CombinedPerformance(ClusErrorList par, NumericAttrType[] num, NominalAttrType[] nom) {
        super(par, num.length + nom.length);
        numAttributes = num;
        nomAttributes = nom;
        num_length = num.length;
        nom_length = nom.length;

        errList = new ClusErrorList();
        numericError = new ExplainedVarianceScore(errList, num);
        nominalError = new F1Score(errList, nom);
    }

    /**
     * Passes an example (~DataTuple) to the numeric and nominal error metric.
     *
     * This tuple gets handeled by the two specific error classes (i.e.: ExplainedVarianceScore and F1Score) themselves,
     * using their addExample methods.
     *
     */
    @Override
    public void addExample(DataTuple tuple, ClusStatistic pred) {
        if(num_length > 0)
            numericError.addExample(tuple, pred);
        if(nom_length > 0)
            nominalError.addExample(tuple, pred);
    }


    /*
    * Error calculation
    * */

    /**
     * Returns a map which maps all indices of the attributes in this error measure
     * to their corresponding performance.
     *
     */
    public HashMap<Integer, Double> getAllModelErrorsByIndex() {
        HashMap<Integer, Double> returnMap = new HashMap<Integer, Double>();
        returnMap.putAll(numericError.getAllModelErrorsByIndex());
        returnMap.putAll(nominalError.getAllModelErrorsByIndex());

        return returnMap;
    }

    /**
     * Compose a map that maps index to the relative performance of for that attribute.
     *
     * @param maxPerformances HashMap with the maximum performances mapped to the indices.
     * */
    public HashMap<Integer, Double> getAllRelativeModelErrorsByIndex(Map<Integer, Double> maxPerformances) {
        // Get the HashMap of our own performance
        HashMap<Integer, Double> returnMap = getAllModelErrorsByIndex();
        // Compare to the provided maximum performance
        for(int key : returnMap.keySet()) {
            double relativePerformance = 1.0 - (maxPerformances.get(key) - returnMap.get(key));
            returnMap.put(key, relativePerformance);
        }

        return returnMap;
    }

    /**
     * Gives a description of all components of this error measure by index.
     *
     * Returns a string of the following form: [index_1:performance_1, index_2:performance_2, ...]
     */
    public String getModelErrorDescriptionByIndex() {
        // Get the HashMap of our model's performance
        HashMap<Integer, Double> map = getAllModelErrorsByIndex();

        // Write it to a string
        StringBuilder stringBuilder = new StringBuilder("[");
        for(int key : map.keySet()) {
            stringBuilder.append(key + ":" + map.get(key) + ", ");
        }
        stringBuilder.append("]");

        return stringBuilder.toString();
    }

    /**
     * Returns the relative error of the entire model,
     * averaged across all the attributes.
     * */
    public double getRelativeModelError(Map<Integer, Double> maxPerformances) {
        Map<Integer, Double> allRelativeErrors = getAllRelativeModelErrorsByIndex(maxPerformances);
        double avg = 0.0;
        for(int key : allRelativeErrors.keySet()) {
            avg += allRelativeErrors.get(key);
        }
        return (avg / allRelativeErrors.size());
    }

    /**
     * Averages the individual performance measure of each nominal and numeric target attribute.
     *
     * To get the individual error for one attribute, we rely on the getModelErrorComponent method of the
     * ExplainedVarianceScore and F1Score classes.
     *
     */
    @Override
    public double getModelError() {
        double avg = 0.0;
        for(int i = 0; i < num_length; i++) {
            avg += numericError.getModelErrorComponent(i);
        }
        for(int i = 0; i < nom_length; i++) {
            avg += nominalError.getModelErrorComponent(i);
        }

        return avg/m_Dim;
    }

    /**
     * Returns the single error component corresponding to the given index.
     */
    public double getModelErrorByAttributeIndex(int idx) {
        if(numericError.hasAttributeIndex(idx))
            return numericError.getModelErrorByAttributeIndex(idx);
        else
            return nominalError.getModelErrorByAttributeIndex(idx);
    }


    /*
    * Additional methods
    * */

    /**
     * Checks whether this error measure has performance component for the given index.
     * Returns true when the given index is in the numerical error measure or in the nominal error measure.
     */
    public boolean hasAttributeIndex(int idx) {
        return numericError.hasAttributeIndex(idx) || nominalError.hasAttributeIndex(idx);
    }

    /**
     * Clone this object
     * */
    @Override
    public ClusError getErrorClone(ClusErrorList par) {
        return new CombinedPerformance(par, numAttributes, nomAttributes);
    }

    /**
     * Return the classname of this object
     * */
    @Override
    public String getName() {
        return "Combined Performance";
    }

}
