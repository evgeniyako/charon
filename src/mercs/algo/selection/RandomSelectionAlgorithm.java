package mercs.algo.selection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.util.MercsException;
/**
 * RandomSelectionAlgorithm
 *
 * This selection algorithm selects M random trees. This means that for each of
 * the M trees, a random selection of input and out attributes is selected.
 * After selecting M trees, we check if every attribute appears at least once as
 * output attribute. If this is not the case, it is added to the output
 * attributes of a randomly picked tree.
 *
 * For every randomly selected tree, a ClusSchema is returned.
 *
 * The maximum amount of output attributes is 3. There is no limit on the amount
 * of input attributes. The algorithm selects M = 2 * N trees, where N is the
 * amount of attributes.
 *
 */
public class RandomSelectionAlgorithm implements SelectionAlgorithm{

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    private ClusSchema schema;

    private int M, maxInputAttributes, maxOutputAttributes, totalAttributes;
    private int iteration;
    private Random random;

    /**
     * Return the current schema (ClusSchema)
     * */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Set the current schema (ClusSchema)
     * */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Check validity of the current schema
     * */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }


    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this algorithm with the given schema if it is valid,
     * otherwise a MercsException is thrown.
     *
     * Initialization means filling in some concrete values for the properties
     */
    public RandomSelectionAlgorithm(ClusSchema schema) throws MercsException {
        if (!isValidSchema(schema)) {
            throw new MercsException("Invalid schema");
        }
        setSchema(schema);

        iteration = 0;
        totalAttributes = schema.getNbAttributes();
        M = 2 * totalAttributes;                        // If there are N attributes, we select M = 2 * N trees
        maxInputAttributes = totalAttributes - 1;
        maxOutputAttributes = 3;
        random = new Random();
    }

    /*
    * Methods about the trees
    * */

    /**
     * Method that returns the next (~next iteration of the algorithm, although THIS algorithm does only one iteration)
     * trees in the MDE-model. It returns them as an ArrayList of ClusSchema's, which for most purposes ARE the actual trees.
     *
     * We only do one iteration, so we return an empty ArrayList if we exceed one iteration.
     *
     * This method just returns all the trees we have constructed, in a single iteration.
     * */
    public List<ClusSchema> getNextTrees(MultiDirectionalEnsembleModel currentModel) throws ClusException, IOException {
        if (iteration > 0) {                        // This comes down to a isModelFinished-check
            return new ArrayList<ClusSchema>();
        }
        ++iteration;

        return selectRandomTrees(M, maxInputAttributes, maxOutputAttributes);
    }

    /**
     * selectRandomTrees
     *
     * Important method of this class, this is where we actually select a collection of trees in a random fashion.
     * */
    private List<ClusSchema> selectRandomTrees(int M, int maxIn, int maxOut) {
        List<SelectionAlgorithmTree> treeList = new ArrayList<SelectionAlgorithmTree>(M);                // List of abstract representation (~keeping inputs and outputs as ints) of trees

        // 1) Build M trees
        for (int i = 0; i < M; ++i) {                                               // For each of our M =2N trees, do:
            SelectionAlgorithmTree tree = new SelectionAlgorithmTree();             // new abstract representation of a tree

			/* Each attribute has a weight, the higher the weight,
			 * the higher the probability it will be selected.
			 * For now, all attributes have equal weight.
			 */
            double[] weights = new double[totalAttributes];
            Arrays.fill(weights, 1);

            int out = randomBetween(1, maxOut);                                     // select output attributes (using helper method randomBetween to fix amount of outputs)
            for (int j = 0; j < out; j++) {
                int selectedAttribute = selectRandomAttributeWithRoulette(weights); // For the actual selection, we rely on this helper method (selectRandomAttributeWithRoulette(weights))
                tree.addOutAttribute(selectedAttribute + 1);                        // Add this attribute to the output of the tree (~SelectionAlgorithmTree object)
                weights[selectedAttribute] = 0;                                     // Set the weight to zero, so that this output cannot be repicked (eg: no tree [x]->[2,2])
            }

            int in = randomBetween(1, Math.min(maxIn, totalAttributes - out));      // select input attributes
            for (int j = 0; j < in; j++) {
                int selectedAttribute = selectRandomAttributeWithRoulette(weights); // For the actual selection, we rely on this helper method (selectRandomAttributeWithRoulette(weights))
                tree.addInAttribute(selectedAttribute + 1);                         // Add this attribute to the output of the tree (~SelectionAlgorithmTree object)
                weights[selectedAttribute] = 0;                                     // Set the weight to zero, so that this input cannot be repicked (eg: no tree [1,1]->[x])
            }

            treeList.add(tree);                                                     // Of course, we add our newly constructed tree to the full tree list.
        }

        // 2) Assign attributes which are not yet selected as output to a random tree, as output
        List<Integer> notSelectedAsOutput = getAttributesNotSelectedAsOutput(treeList);     // We fetch these overlooked attributes by a dedicated helper method (getAttributesNotSelectedAsOutput(treeList))
        System.out.println("Not selected as output: " + notSelectedAsOutput.toString());
        if (notSelectedAsOutput.size() > 0) {
            for (Integer notSelected : notSelectedAsOutput) {
                int randomTreeNumber = randomBetween(0, M - 1);
                treeList.get(randomTreeNumber).addOutAttribute(notSelected + 1);            // For each of these attr, we choose a random tree and the attr to its output (addOutAttribute).
            }
        }

        // 3) Create a list of ClusSchema's from the abstract trees
        List<ClusSchema> returnList = new ArrayList<ClusSchema>(M);
        for (SelectionAlgorithmTree tree : treeList) {
            ClusSchema newSchema = tree.toClusSchema(getSchema());                          // Convert the abstract tree (SelectionAlgorithmTree) to its canonical ClusSchema form
            returnList.add(newSchema);
        }

        return returnList;
    }

    /*
    * Helper methods (mainly concerned with attribute selection)
    * */

    /**
     * Selects a random attribute based on the weights in the given array.
     * Attributes with a higher weight have higher probability to get selected.
     * This is achieved using Roulette Wheel Selection.
     *
     */
    private int selectRandomAttributeWithRoulette(double[] weights) {
        // We first build a cumulative weights vector
        double[] cumulativeWeights = new double[weights.length];
        cumulativeWeights[0] = weights[0];
        for (int i = 1; i < weights.length; ++i) {
            cumulativeWeights[i] = cumulativeWeights[i - 1] + weights[i];
        }

        double val = random.nextDouble() * cumulativeWeights[cumulativeWeights.length - 1];
        /*
        * binarySearch(double[] a, double key)
        * index of the search key, if it is contained in the array;
        * otherwise, (-(insertion point) - 1).
        * The insertion point is defined as the point at which the key would be inserted into the array:
        * the index of the first element greater than the key, or a.length if all elements in the array are less than the specified key.
        * Note that this guarantees that the return value will be >= 0 if and only if the key is found.
        * */
        int index = Arrays.binarySearch(cumulativeWeights, val);
        if (index < 0) {
            index = Math.abs(index + 1);
        }

        return index;
    }

    /**
     * Returns a list containing the attributes which are not yet used as output
     * attributes.
     */
    private List<Integer> getAttributesNotSelectedAsOutput(List<SelectionAlgorithmTree> treeList) {
        boolean[] selectedAsOutput = new boolean[totalAttributes];
        Arrays.fill(selectedAsOutput, false);

        for (SelectionAlgorithmTree tree : treeList) {
            for (int outAttr : tree.getOutAttributes()) {
                selectedAsOutput[outAttr - 1] = true;
            }
        }

        List<Integer> returnList = new ArrayList<Integer>();
        for (int i = 0; i < totalAttributes; ++i) {
            if (!selectedAsOutput[i])
                returnList.add(i);
        }

        return returnList;
    }

    /**
     * Returns a random number between min (inclusive) and max (inclusive)
     */
    private int randomBetween(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    /*
    * Methods about other things
    * */

    /**
     * The model is finished if and only if each attribute has its tree with the
     * attribute as output. Thus after N calls to
     * getNextInputToTargetAttributes.
     *
     * This gets done within a single iteration, so we only need to check that.
     */
    @Override
    public boolean isModelFinished(MultiDirectionalEnsembleModel currentModel) {
        return iteration > 0;
    }

    /*
     * =======================================
     * DUMMY METHODS
     * =======================================
     */

    /**
     * Do a final pruning of the MDE-model
     * TODO: No implementation yet
     * */
    public MultiDirectionalEnsembleModel finalPrune(MultiDirectionalEnsembleModel currentModel){
        return currentModel;
    }

}


