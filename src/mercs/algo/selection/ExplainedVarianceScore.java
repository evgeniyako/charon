package mercs.algo.selection;

import java.util.Arrays;
import java.util.HashMap;

import clus.data.rows.DataTuple;
import clus.data.type.NumericAttrType;
import clus.error.ClusError;
import clus.error.ClusErrorList;
import clus.error.ClusNumericError;
import clus.main.Settings;
import clus.statistic.ClusStatistic;

/**
 * ExplainedVarianceScore
 *
 * Computes the explained variance regression score as described at:
 * http://scikit-learn.org/stable/modules/model_evaluation.html#regression-
 * metrics.
 *
 * This is a performance measure for numeric attributes. In this way it is not surprising that it extends
 * the corresponding Clus class for the same purpose, i.e.: ClusNumericError
 *
 * explained_variance(y,y_pred) = 1 - Var{y-y_pred} / Var{y}
 * (where y_pred is the predicted output and y the correct output)
 *
 * The variance of y and y-y_pred is computed incrementally using Knuth's algorithm
 * as described at:
 * https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#
 * Online_algorithm
 *
 * Obviously, this is an advantage when we will use Mercs in an incremental setting.
 *
 */
public class ExplainedVarianceScore extends ClusNumericError {

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    public final static long serialVersionUID = Settings.SERIAL_VERSION_ID;

    protected int m_N;
    protected double[] m_TrueMean;
    protected double[] m_TrueM2;
    protected double[] m_DeltaMean;
    protected double[] m_DeltaM2;
    private HashMap<Integer, Integer> indexMap;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes the superclass (ClusNumericError)
     *
     * Initialize the properties of this class. Fill all the arrays with zeroes.
     *
     * Create a HashMap that connects the index of the attribute to the i of the NumericAttrType[] array.
     *
     * */
    public ExplainedVarianceScore(ClusErrorList par, NumericAttrType[] num) {
        super(par, num);
        m_N = 0;
        m_TrueMean = new double[m_Dim];
        Arrays.fill(m_TrueMean, 0.0);
        m_DeltaMean = new double[m_Dim];
        Arrays.fill(m_DeltaMean, 0.0);
        m_TrueM2 = new double[m_Dim];
        Arrays.fill(m_TrueM2, 0.0);
        m_DeltaM2 = new double[m_Dim];
        Arrays.fill(m_DeltaM2, 0.0);
        indexMap = new HashMap<Integer, Integer>(num.length);
        for (int i = 0; i < num.length; i++) {
            indexMap.put(num[i].getIndex(), i);
        }
        // System.out.println("Explained Variance index map: " + indexMap);
    }

    /**
     * Recalculate the summarizig properties of this object,
     * based on the addition of this new example.
     * */
    public void addExample(double[] real, double[] predicted) {
        //System.out.println("Adding example: " + Arrays.toString(real) + ", pred: " + Arrays.toString(predicted));
        m_N++;

        for (int i = 0; i < m_Dim; i++) {
            double diff_true = real[i] - m_TrueMean[i];
            m_TrueMean[i] += diff_true / m_N;
            m_TrueM2[i] += diff_true * (real[i] - m_TrueMean[i]);

            double diff_delta = (real[i] - predicted[i]) - m_DeltaMean[i];
            m_DeltaMean[i] += diff_delta / m_N;
            m_DeltaM2[i] += diff_delta * (real[i] - predicted[i] - m_DeltaMean[i]);
        }
        //System.out.println("m_N = " + m_N);
        //System.out.println("m_TrueMean = " + m_TrueMean[0]);
        //System.out.println("m_TrueM2 = " + m_TrueM2[0]);
    }

    /*
    * All the methods that compute certain performance measures
    * */

    /**
     * Returns the error of the attribute that belongs to the given index
     * */
    public double getModelErrorByAttributeIndex(int idx) {
        return getModelErrorComponent(indexMap.get(idx));
    }

    /**
     * Returns a map that contains all the errors of the numeric attributes.
     * */
    public HashMap<Integer, Double> getAllModelErrorsByIndex() {
        HashMap<Integer, Double> returnMap = new HashMap<Integer, Double>();
        for (Integer key : indexMap.keySet()) {
            returnMap.put(key, getModelErrorByAttributeIndex(key));
        }

        return returnMap;
    }

    /*
    * Additional methods
    * */

    /**
     * Tells us if a certain attribute, belonging to index idx is contained in the indexMap
     * I.e.: If this is an numeric attribute.
     * */
    public boolean hasAttributeIndex(int idx) {
        return indexMap.containsKey(idx);
    }

    /**
     * Returns the true variance
     * */
    private double getTrueVariance(int dim) {
        return m_TrueM2[dim] / m_N;
    }

    /**
     * Returns the delta variance
     * */
    private double getDeltaVariance(int dim) {
        return m_DeltaM2[dim] / m_N;
    }

    /**
     * If called, we return false because we do not return a summary.
     * */
    public boolean hasSummary() {
        return false;
    }


    /*
    *  Extra overrides of the superclass (MercsNominalError or ClusNumericError)
    *  In this particular case only overrides of ClusNumericError, and even more ClusError
    * */

    /**
     * Returns the error of attribute i
     * */
    @Override
    public double getModelErrorComponent(int i) {
        return 1.0 - getDeltaVariance(i) / getTrueVariance(i);
    }
    /**
     * Returns the mean of errors over all attributes i
     * */
    @Override
    public double getModelError() {
        double mean = 0.0;
        for (int i = 0; i < m_Dim; i++) {
            mean += getModelErrorComponent(i);
        }

        return mean / m_Dim;
    }


    /**
     * Method from the superclass (ClusNumericError),
     * rewritten in such way that it just uses our new addExample method.
     * */
    @Override
    public void addExample(DataTuple tuple, ClusStatistic pred) {
        double[] predicted = pred.getNumericPred();
        double[] real = new double[m_Dim];
        for (int i = 0; i < m_Dim; i++)
            real[i] = getAttr(i).getNumeric(tuple);
        addExample(real, predicted);
    }
    /**
     * Method from the superclass (ClusNumericError),
     * rewritten in such way that it just uses our new addExample method.
     * */
    @Override
    public void addExample(DataTuple real, DataTuple pred) {
        double[] predictedArr = new double[m_Dim];
        double[] realArr = new double[m_Dim];
        for (int i = 0; i < m_Dim; i++) {
            realArr[i] = getAttr(i).getNumeric(real);
            predictedArr[i] = getAttr(i).getNumeric(pred);
        }
        addExample(realArr, predictedArr);
    }


    @Override
    public void addInvalid(DataTuple tuple) {
    }

    @Override
    public void add(ClusError other) {
        System.err.println(getName() + ": add() not implemented!");
    }

    /**
     * If this score is 1, all variance is explained which means the model fits the data perfectly
     * */
    @Override
    public boolean shouldBeLow() {
        return false;
    }

    /**
     * Create a fresh ExplainedVarianceScore object
     * */
    @Override
    public ClusError getErrorClone(ClusErrorList par) {
        return new ExplainedVarianceScore(par, m_Attrs);
    }

    /**
     * Wipe all the properties (set them to zero)
     * */
    @Override
    public void reset() {
        for (int i = 0; i < m_Dim; i++) {
            m_N = 0;
            m_TrueMean[i] = 0.0;
            m_TrueM2[i] = 0.0;
            m_DeltaMean[i] = 0.0;
            m_DeltaM2[i] = 0.0;
        }
    }

    /**
     * Return the class name of this object
     * */
    @Override
    public String getName() {
        return "Explained Variance Score";
    }
}
