package mercs.algo.selection;

import java.io.IOException;
import java.util.List;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.model.MultiDirectionalEnsembleModel;

/**
 * This interface describes the methods any selection algorithm should implement.
 *
 * The selection algorithm is used when inducing MERCS.
 *
 * The goal of the selection algorithm is to find the best input to target attribute sets
 * for the next base type of the MERCS model (i.e.: which trees should I induce?).
 *
 */

public interface SelectionAlgorithm {

    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Given the current MultiDirectionalEnsembleModel this method returns the set of
     * ClusSchema's that should be used to learn the next base type classifiers.
     * The ClusSchemas that are generated, depend on the implementing interface.
     */
    public List<ClusSchema> getNextTrees(MultiDirectionalEnsembleModel currentModel) throws ClusException, IOException;

    /**
     * Prune the MDE-model (MultiDirectionalEnsembleModel)
     * */
    public MultiDirectionalEnsembleModel finalPrune(MultiDirectionalEnsembleModel currentModel);

    /**
     * Decides whether the current model is complete in the sense that no new
     * base classifiers should be learned.
     */
    public boolean isModelFinished(MultiDirectionalEnsembleModel currentModel);

}
