package mercs.algo.selection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.util.MercsException;

/**
 * BaseSelectionAlgorithm
 *
 * The most basic selection algorithm.
 *
 * Selects one tree per attribute namely the one with this attribute
 * as its target (i.e. outputs) and the rest as decriptive (i.e. inputs).
 *
 * It returns one ClusSchema for every tree. This means N (=number of attr.) schemas in total.
 *
 * E.g.: For attributes {X1,X2,X3,X4}, this algorithm will return 4 trees:
 * {X1,X2,X3} -> X4
 * {X1,X2,X4} -> X3
 * {X1,X3,X4} -> X2
 * {X2,X3,X4} -> X1
 *
 */
public class BaseSelectionAlgorithm implements SelectionAlgorithm{
    /**
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    /**
     * Tracker for the amount of trees in the model
     * */
    private int M;
    /**
     * Tracker for the amount of times that getNextTrees has been called
     * */
    private int iteration;
    private ClusSchema schema;

    /**
     * Returns the ClusSchema of this BaseSelectionAlgorithm
     * */
    public ClusSchema getSchema() {
        return schema;
    }
    /**
     * Sets the ClusSchema of this BaseSelectionAlgorithm
     * */
    private void setSchema(ClusSchema schema) {
        this.schema = schema;
    }
    /**
     * Checks the existence of the ClusSchema of this BaseSelectionAlgorithm
     * */
    public boolean isValidSchema(ClusSchema schema) {
        return schema != null;
    }

    /**
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes this algorithm with the given schema if it is valid,
     * otherwise a MercsException is thrown.
     */
    public BaseSelectionAlgorithm(ClusSchema schema) throws MercsException {
        if (!isValidSchema(schema)) {
            throw new MercsException("Invalid schema");
        }
        setSchema(schema);
        iteration = 0;
        // Set the desired amount of trees
        M = getSchema().getNbAttributes();
    }

    /**
     * Given the current MultiDirectionalEnsembleModel this method returns the set of
     * ClusSchema's that should be used to learn the next base type classifiers.
     * The ClusSchemas that are generated, depend on the implementing interface.
     */
    @Override
    public List<ClusSchema> getNextTrees(MultiDirectionalEnsembleModel currentModel) throws ClusException, IOException {
        // if we already returned a set of trees (~ we have already done an iteration), return empty set
        if (iteration > 0)
            return new ArrayList<ClusSchema>();
        iteration++;

        // list to hold abstract representation of trees
        List<SelectionAlgorithmTree> treeList = new ArrayList<SelectionAlgorithmTree>(M);

        for (int i = 1; i <= M; i++) {                                  // Attributes start at 1, not 0

            SelectionAlgorithmTree tree = new SelectionAlgorithmTree(); // New abstract representation of a tree

            tree.addOutAttribute(i);                                    // Set target attribute
            for (int j = 1; j <= M; j++) {
                if (j != i)
                    tree.addInAttribute(j);                             // Set input attributes (i.e. all the others)
            }

            treeList.add(tree);                                         // Add the new tree to our list of trees
        }

        List<ClusSchema> returnList = new ArrayList<ClusSchema>(M);             // Create ClusSchema's from abstract representations
        for (int i = 0; i < treeList.size(); i++) {
            ClusSchema newSchema = treeList.get(i).toClusSchema(getSchema());   // Create a real ClusSchema per (abstract) tree
            returnList.add(newSchema);
        }
        return returnList;
    }

    /**
     * Last pruning of the model. In this algorithm, this method does not do anything.
     * */
    @Override
    public MultiDirectionalEnsembleModel finalPrune(MultiDirectionalEnsembleModel currentModel){
        return currentModel;
    }

    /**
     * The model is finished if and only if each attribute has its tree with the
     * attribute as output. Thus after N calls to
     * getNextInputToTargetAttributes.
     */
    @Override
    public boolean isModelFinished(MultiDirectionalEnsembleModel currentModel) {
        return iteration > 0;
    }




}
