package mercs.algo.selection.metrics;

import java.util.ArrayList;
import java.util.HashMap;

import mercs.model.MultiDirectionalEnsembleModel;

/**
 * MercsModelPerformance
 *
 * Abstract class that provides the blueprint for different performance metrics of a mercs model.
 * */
public abstract class MercsModelPerformance {

    /**
     * Get a performance of the entire Mercs model.
     *
     * We create a map from each attribute to its prediction performance by the Mercs model.
     * */
    public abstract HashMap<Integer, ArrayList<Double>> getMercsModelPerformance(MultiDirectionalEnsembleModel model);

    /**
     * Method to asses the performance of a Mercsmodel
     * */
    public abstract double getPerformance(MultiDirectionalEnsembleModel model);
}