package mercs.algo.selection.metrics;

import java.util.HashMap;
import java.util.Map;

import clus.data.type.NominalAttrType;
import clus.error.ClusErrorList;
import clus.error.ClusNominalError;
import clus.main.Settings;

/**
 * MercsNominalError
 *
 * Abstract class that extends the ClusNominalError class
 *
 */

public abstract class MercsNominalError extends ClusNominalError {

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    public final static long serialVersionUID = Settings.SERIAL_VERSION_ID;

    private Map<Integer, Integer> indexMap;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes the superclass (ClusNominalError) with a ClusErrorList and nominal attributes
     * @param par ClusErrorList
     * @param nom NominalAttrType[] array (also from Clus)
     *
     * And then we also initialize the indexmap, which maps the indices to their location in the array.
     *
     * */
    public MercsNominalError(ClusErrorList par, NominalAttrType[] nom) {
        super(par, nom);
        indexMap = new HashMap<Integer, Integer>(nom.length);
        for(int i = 0; i < nom.length; i++) {
            indexMap.put(nom[i].getIndex(), i);
        }
    }

    /**
     * Returns the error of the attribute that belongs to the given index
     * */
    public double getModelErrorByAttributeIndex(int idx) {
        return getModelErrorComponent(indexMap.get(idx));
    }

    /**
     * Returns a map that contains all the errors of the nominal attributes.
     * */
    public Map<Integer, Double> getAllModelErrorsByIndex() {
        Map<Integer, Double> returnMap = new HashMap<Integer, Double>();
        for (Integer key : indexMap.keySet()) {
            returnMap.put(key, getModelErrorByAttributeIndex(key));
        }

        return returnMap;
    }

    /**
     * Check whether or not the provided index is in the NominalAttrType[] property. (Property of superclass)
     *
     * This means is the attribute that belongs to the given index a nominal attribute.
     * */
    public boolean hasAttributeIndex(int idx) {
        return indexMap.containsKey(idx);
    }
}