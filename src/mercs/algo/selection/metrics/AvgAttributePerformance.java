package mercs.algo.selection.metrics;

import java.util.ArrayList;
import java.util.HashMap;

import clus.data.type.ClusAttrType;
import mercs.model.MercsModel;
import mercs.model.MultiDirectionalEnsembleModel;

/**
 * Average Attribute Performance is a metric for measuring the performance of a
 * MERCS model. Each attribute can appear as a target attribute in multiple
 * trees in the model, every time with a different performance.
 *
 * The Average Attribute Performance is calculated as follows: - for each
 * attribute, get its performance in the trees where it appears as output, and
 * average these performances - average the average performance of each
 * attribute
 *
 * This measure can be used to compare different MERCS models. It is assumed
 * that a higher average attribute performance leads to a better overall model.
 *
 * @author tomasmertens
 *
 */
public class AvgAttributePerformance extends MercsModelPerformance {

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * We create a map from each attribute to its prediction performance by the Mercs model.
     * The performance of each individual model is an entry in an ArrayList belonging to a certain attribute.
     * */
    @Override
    public HashMap<Integer, ArrayList<Double>> getMercsModelPerformance(MultiDirectionalEnsembleModel model) {
        HashMap<Integer, ArrayList<Double>> idxToPerformancesMap = new HashMap<Integer, ArrayList<Double>>();

        if (model.getModelSize() == 0)                                                      // If there are no models present in the MDE-model, return the empty hashmap
            return idxToPerformancesMap;

        ClusAttrType[] attrs = model.getSchema().getAllAttrUse(ClusAttrType.ATTR_USE_ALL);  // Get *all* the attributes (~ClusAttrType) from the schema

        for (ClusAttrType attr : attrs) {                                                   // Enter all attr indices in the hashmap
            idxToPerformancesMap.put(attr.getIndex(), new ArrayList<Double>());
        }

        for(int i = 0; i < model.getModels().size(); i++) {                                                             // For each individual model in the Mercs model
            MercsModel clusTree = model.getModels().get(i);                                                             // Get the individual model
            HashMap<Integer, Double> treePerformances = clusTree.getCombinedPerformance().getAllModelErrorsByIndex();   // Get the errors (performance) per index
            for (int key : treePerformances.keySet()) {
                idxToPerformancesMap.get(key).add(treePerformances.get(key));                                           // Add this performance to arraylist belonging to a certain attr
            }
        }

		/*
		for (MercsModel tree : model.getModels()) {
			MercsModel clusTree = tree;
			HashMap<Integer, Double> treePerformances = clusTree.getCombinedPerformance().getAllModelErrorsByIndex();
			for (int key : treePerformances.keySet()) {
				idxToPerformancesMap.get(key).add(treePerformances.get(key));
			}
		}
		*/
        return idxToPerformancesMap;
    }

    /**
     * Return a string that yields the Mercs model performance
     * */
    public String getMercsModelPerformanceDescription(MultiDirectionalEnsembleModel model) {
        HashMap<Integer, ArrayList<Double>> idxToPerformancesMap = getMercsModelPerformance(model);

        StringBuilder stringBuilder = new StringBuilder();
        double total_avg = 0.0;
        for (int i = 0; i < model.getSchema().getNbAttributes(); i++) {
            if (idxToPerformancesMap.get(i).size() > 0) {
                double avg = 0.0;
                double min = 1.0;
                double max = 0.0;
                for (double d : idxToPerformancesMap.get(i)) {
                    if (d < min)        // Keeping track of min
                        min = d;
                    if (d > max)        // Keeping track of max
                        max = d;

                    avg += d;
                }
                avg /= idxToPerformancesMap.get(i).size();
                total_avg += avg;
                stringBuilder.append(avg);
                stringBuilder.append(",");
            } else {
                stringBuilder.append("NaN,");
            }
        }
        stringBuilder.insert(0, total_avg/model.getSchema().getNbAttributes());
        stringBuilder.insert(1, ",");

        // TODO: Min and Max do not get appended to the string. Replace 0 and ! w/ min/max?
        return stringBuilder.toString();
    }


    /**
     * For each attribute, we count the amount of models that predicts this attribute.
     * This method has a rather confusing name.
     * */
    public HashMap<Integer, Integer> getMercsModelPerformanceAttributeCount(MultiDirectionalEnsembleModel model) {
        HashMap<Integer, ArrayList<Double>> idxToPerformancesMap = getMercsModelPerformance(model);
        HashMap<Integer, Integer> attributeCount = new HashMap<Integer, Integer>();

        for (int i = 0; i < model.getSchema().getNbAttributes(); i++) {
            attributeCount.put(i, idxToPerformancesMap.get(i).size());
        }

        return attributeCount;
    }

    /**
     * Same as previous method, but now instead of a Hashmap, we build a string that hosts this information.
     * */
    public String getMercsModelPerformanceAttributeCountString(MultiDirectionalEnsembleModel model) {
        HashMap<Integer, ArrayList<Double>> idxToPerformancesMap = getMercsModelPerformance(model);
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < model.getSchema().getNbAttributes(); i++) {
            stringBuilder.append(idxToPerformancesMap.get(i).size());
            stringBuilder.append(",");
        }

        return stringBuilder.toString();
    }


    /**
     * Create a Hashmap (attribute_index) <--> (average performance over all models that predict this attribute)
     * */
    public HashMap<Integer, Double> getAvgAttributePerformanceMap(MultiDirectionalEnsembleModel model) {
        HashMap<Integer, ArrayList<Double>> idxToPerformancesMap = getMercsModelPerformance(model);
        HashMap<Integer, Double> returnMap = new HashMap<Integer, Double>();
        for(int key : idxToPerformancesMap.keySet()) {
            ArrayList<Double> attrPerformances = idxToPerformancesMap.get(Integer.valueOf(key));    // Get the performance ArrayList for this one attribute
            double avg = 0.0;
            for(double d : attrPerformances)
                avg += d;
            avg /= attrPerformances.size();                                                         // Calculate the average performance for this attribute
            returnMap.put(key, avg);                                                                // Put this result in the returnMap as a key-value pair
        }
        return returnMap;
    }

    /**
     * A method to get a single double as performance assessment of the MDE-model.
     * A coarse-grained version of the previous method.
     * */
    @Override
    public double getPerformance(MultiDirectionalEnsembleModel model) {
        HashMap<Integer, ArrayList<Double>> idxToPerformancesMap = getMercsModelPerformance(model);
        double total_avg = 0.0;
        for (int i = 0; i < model.getSchema().getNbAttributes(); i++) {     // For each attribute
            if (idxToPerformancesMap.get(i).size() > 0) {
                double avg = 0.0;
                for (double d : idxToPerformancesMap.get(i)) {              // For each performance (of the models that predict this attribute)
                    avg += d;
                }
                avg /= idxToPerformancesMap.get(i).size();                  // Average over all the models that contained this attribute
                total_avg += avg;
            } else {
                // no tree containing attribute i as output...
            }
        }
        return (total_avg/model.getSchema().getNbAttributes());             // And get the average of all these numbers in a meaningful way.
    }


}
