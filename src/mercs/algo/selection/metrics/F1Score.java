package mercs.algo.selection.metrics;

import clus.data.rows.DataTuple;
import clus.data.type.NominalAttrType;
import clus.error.ClusError;
import clus.error.ClusErrorList;
import clus.main.Settings;
import clus.statistic.ClusStatistic;

/**
 * F1Score
 *
 * F1Score is an implementation of the multi-class F1 score.
 * This class contains methods to calculate the following performance measures for multi-class attributes:
 *  - Macro Precision
 *  - Micro Precision
 *  - Macro Recall
 *  - Micro Recall
 *  - Macro F1 Score
 *  - Micro F1 Score
 *  - Macro F_beta score
 *  - Micro F_beta score
 *
 * The overall model error averages the performance of each attribute.
 *
 * Precision for class i in the multi-class setting is defined as follows:
 * 		precision_i = tp_i / (tp_i + fp_i)
 * E.g. in the weather example, for attribute "outlook", 'i' could refer to "sunny", "rainy" or "overcast".
 * Precision = How much % of my positive predictions was actually correct?
 *
 * Recall for class i in the multi-class setting is defined as follows:
 * 		recall_i = tp_i / (tp_i + fn_i)
 * Recall = How much % of my positive facts was actually predicted correcly?
 *
 * To get a performance for an attribute, the precision / recall metrics of the individual values have to be averages somehow.
 * This can be done in macro fashion or micro fashion.
 * Check out following resources for more info:
 *  - http://scikit-learn.org/stable/modules/model_evaluation.html#precision-recall-f-measure-metrics
 *  - A systematic analysis of performance measures for classification tasks by Marina Sokolova and Guy Lapalme
 *
 * Following assumptions are made:
 *  - If for a class i, there are no examples predicted to be i, precision is 0.0
 *  - If for a class i, there are no examples which have true class i, recall is 0.0
 *
 */
public class F1Score extends MercsNominalError{
    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    public final static long serialVersionUID = Settings.SERIAL_VERSION_ID;

    /**
     * Contingency table
     *
     * Example contingency table for attribute "outlook" with three values {sunny, rainy, overcast}
     *
     *REAL/PRED | sunny | rainy | overcast |
     *----------|-------|-------|----------|
     *    sunny |       |       |          |
     *    rainy |       |       |          |
     * overcast |       |       |          |
     *----------|-------|-------|----------|
     *
     * */
    protected int[][][] cont_table;


    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes the superclass (=MercsNominalError),
     * and generates a contingency table. This comes down to constructing
     * a three-dimensional array of size:
     *
     * a x s(a) x s(a)
     *
     * a~attribute
     * s(a)~size of that attribute (possible values)
     *
     * First dimension = all the attributes
     * Every element of this first dimension contains a two-dimensional array which is the contingency table for this attr.
     *
     *
     * */
    public F1Score(ClusErrorList par, NominalAttrType[] nom) {
        super(par, nom);
        cont_table = new int[m_Dim][][];
        for(int i = 0; i < m_Dim; i++) {
            int size = getAttr(i).getNbValues();
            cont_table[i] = new int[size][size];
        }
    }

    /**
     * Add one instance of prediction and true value to the contingency table
     * */
    public void addExample(int[] trueValues, int[] predValues) {
        for (int i = 0; i < m_Dim; i++) {
            // update row corresponding to real value in predicted column
            cont_table[i][trueValues[i]][predValues[i]]++;
        }
    }

    /**
     * Add one instance of prediction and true value to the contingency table
     * */
    public void addExample(DataTuple tuple, ClusStatistic pred) {
        int[] predicted = pred.getNominalPred();
        for (int i = 0; i < m_Dim; i++) {
            // update row corresponding to real value in predicted column
            cont_table[i][getAttr(i).getNominal(tuple)][predicted[i]]++;
        }
    }

    /*
    * All the methods that compute certain performance measures
    * */

    /**
     * Gives the precision for value i of attribute dim.
     *
     * precision_i = tp_i / (tp_i + fp_i)
     *
     * E.g. in the weather example, 'dim' could refer to "outlook" while 'i' could refer to "sunny", if this is
     * the case, this method returns the precision of sunny.
     *
     * Precision = How much % of my positive predictions was actually correct?
     */
    public double getPrecision(int dim, int i) {
        if(sumColumn(cont_table[dim], i) == 0)
            return 0.0;
        return 1.0 * cont_table[dim][i][i] / sumColumn(cont_table[dim], i);
    }
    /**
     * Gives macro precision,
     * which is basically the average of the precisions across all values of a certain attribute (denoted by dim).
     * */
    public double getMacroPrecision(int dim) {
        double avg = 0.0;
        for(int i = 0; i < cont_table[dim].length; i++) {
            avg += getPrecision(dim, i);
        }
        return avg / cont_table[dim].length;
    }
    /**
     * Gives the micro precision,
     * which calculates averages of tp and fp, across all values of a certain attribute and THEN calculates the precision.
     * */
    public double getMicroPrecision(int dim) {
        int sum_tp = 0;
        int sum_fp = 0;
        for(int i = 0; i < cont_table[dim].length; i++) {
            sum_tp += cont_table[dim][i][i];
            sum_fp += ( sumColumn(cont_table[dim], i) - cont_table[dim][i][i] );
        }
        return 1.0 * sum_tp / (sum_tp + sum_fp);
    }

    /**
     * Gives the recall for value i of attribute dim.
     *
     * recall_i = tp_i / (tp_i + fn_i)
     *
     * E.g. in the weather example, 'dim' could refer to "outlook" while 'i' could refer to "sunny", if this is
     * the case, this method returns the precision of sunny.
     *
     * Recall = How much % of my positive facts was actually predicted correcly?
     */
    public double getRecall(int dim, int i) {
        if(sumRow(cont_table[dim], i) == 0)
            return 0.0;
        return 1.0 * cont_table[dim][i][i] / sumRow(cont_table[dim], i);
    }
    /**
     * Gives macro precision,
     * which is basically the average of the recalls across all values of a certain attribute (denoted by dim).
     * */
    public double getMacroRecall(int dim) {
        double avg = 0.0;
        for(int i = 0; i < cont_table[dim].length; i++) {
            avg += getRecall(dim, i);
        }
        return avg / cont_table[dim].length;
    }
    /**
     * Gives the micro recall
     * which calculates averages of tp and fn, across all values of a certain attribute and THEN calculates the recall.
     * */
    public double getMicroRecall(int dim) {
        int sum_tp = 0;
        int sum_fn = 0;
        for(int i = 0; i < cont_table[dim].length; i++) {
            sum_tp += cont_table[dim][i][i];
            sum_fn += ( sumRow(cont_table[dim], i) - cont_table[dim][i][i] );
        }
        return 1.0 * sum_tp / (sum_tp + sum_fn);
    }

    /**
     * Returns macro F_beta score
     * */
    public double getMacroFBetaScore(int dim, double beta) {
        //System.out.println("Macro Precision for dim " + dim + " is " + getMacroPrecision(dim));
        //System.out.println("Macro Recall for dim " + dim + " is " + getMacroRecall(dim));
        return (1.0 + beta * beta) * getMacroPrecision(dim) * getMacroRecall(dim) / (beta * beta * getMacroPrecision(dim) + getMacroRecall(dim));
    }
    /**
     * Returns micro F_beta score
     * */
    public double getMicroFBetaScore(int dim, double beta) {
        return (1.0 + beta * beta) * getMicroPrecision(dim) * getMicroRecall(dim) / (beta * beta * getMicroPrecision(dim) + getMicroRecall(dim));
    }

    /**
     * Returns macro F_1 score
     * */
    public double getMacroF1Score(int dim) {
        return getMacroFBetaScore(dim, 1);
    }
    /**
     * Returns micro F_1 score
     * */
    public double getMicroF1Score(int dim) {
        return getMicroFBetaScore(dim, 1);
    }

    /*
    * Additional methods
    * */

    /**
     * Returns sum column j of table.
     * */
    public int sumColumn(int[][] table, int j) {
        int sum = 0;
        for (int i = 0; i < table.length; i++)
            sum += table[i][j];
        return sum;
    }
    /**
     * Returns sum row i of table.
     * */
    public int sumRow(int[][] table, int i) {
        int sum = 0;
        for (int j = 0; j < table.length; j++)
            sum += table[i][j];
        return sum;
    }

    /**
     * A method that prints our entire contigency table for a given attribute.
     * */
    public void showRawTable(int dim) {
        if(cont_table.length > dim) {
            int [][] table = cont_table[dim];
            for(int i = 0; i < table.length; i++) {
                for(int j = 0; j < table.length; j++) {
                    System.out.print(table[i][j] + ",");
                }
                System.out.println();
            }
            System.out.println("Precision " + dim + ": ");
            for(int i = 0; i < table.length; i++) {
                System.out.println("\t"+ i + ": " + getPrecision(dim,i) + ", ");
            }
            System.out.println();

            System.out.println("Recall " + dim + ": ");
            for(int i = 0; i < table.length; i++) {
                System.out.println("\t"+ i + ": " + getRecall(dim,i) + ", ");
            }
            System.out.println();
        }
        else {
            System.out.println("No table to show...");
        }
    }

    /*
    *  Overrides of the superclass (MercsNominalError or ClusError)
    *  In this particular case only overrides of ClusError
    * */

    /**
     * Returns the error (= macro F1-score) of a particular attribute.
     * */
    @Override
    public double getModelErrorComponent(int dim) {
        return getMacroF1Score(dim);
    }
    /**
     * Returns the error (= macro F1-score) averaged over all attributes.
     * */
    @Override
    public double getModelError() {
        double avg = 0.0;
        for (int i = 0; i < m_Dim; i++) {
            avg += getModelErrorComponent(i);
        }
        return avg / m_Dim;
    }

    /**
     * Returns a string which says 'F1-score'
     * */
    public String toString() {
        return getName();
    }
    /**
     * Returns a string which says 'F1-score'
     * */
    @Override
    public String getName() {
        return "F1 Score";
    }

    /**
     * Create a fresh F1Score object
     * */
    @Override
    public ClusError getErrorClone(ClusErrorList par) {
        return new F1Score(par, m_Attrs);
    }

    /**
     * Wipe the old and create an empty contigency table.
     * */
    @Override
    public void reset() {
        cont_table = new int[m_Dim][][];
        for(int i = 0; i < m_Dim; i++) {
            int size = getAttr(i).getNbValues();
            cont_table[i] = new int[size][size];
        }
    }

}
