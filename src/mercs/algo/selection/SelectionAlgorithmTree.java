package mercs.algo.selection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import clus.data.type.ClusSchema;
import clus.util.ClusException;
import jeans.util.IntervalCollection;

/**
 * This class holds an abstract representation of a decision tree.
 *
 * What this means is that it keeps track of input and output attributes of a certain tree. This
 * class is used in the selection algorithms, when we decide which trees (i.e. input/output attributes selection)
 * we want.
 *
 * Afterwards, using the methods of this class, this abstract representation of a tree can be converted into
 * their operational counterparts, the ClusSchemas.
 */
public class SelectionAlgorithmTree {
    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    /**
     * Indices of the input (descriptive) attributes
     * */
    private List<Integer> inAttributes;
    /**
     * Indices of the output (target) attributes
     * */
    private List<Integer> outAttributes;

    /**
     * Returns the input attributes.
     * */
    public List<Integer> getInAttributes() {
        return inAttributes;
    }
    /**
     * Sets the input attributes. (all at once)
     * */
    public void setInAttributes(List<Integer> inAttributes) {
        this.inAttributes = inAttributes;
    }

    /**
     * Returns the output attributes.
     * */
    public List<Integer> getOutAttributes() {
        return outAttributes;
    }
    /**
     * Sets the output attributes.(all at once)
     * */
    public void setOutAttributes(List<Integer> outAttributes) {
        this.outAttributes = outAttributes;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Initializes both the properties (i.e. inAttr and outAttr) to an empty ArrayList
     * */
    public SelectionAlgorithmTree() {
        inAttributes = new ArrayList<Integer>();
        outAttributes = new ArrayList<Integer>();
    }

    /**
     * Constructor 2
     *
     * Initializes the properties to the provided input ArrayLists (in-and output attributes)
     * */
    public SelectionAlgorithmTree(List<Integer> inAttr, List<Integer> outAttr) {
        inAttributes = inAttr;
        outAttributes = outAttr;

        Collections.sort(inAttributes);
        Collections.sort(outAttributes);
    }

    /**
     * Converts our abstract tree, given by its lists of in-and output attributes,
     * to the useable form of ClusSchema's, that encode the same information in such a way
     * that our classes can understand.
     * */
    public ClusSchema toClusSchema(ClusSchema originalSchema) {
        ClusSchema nextSchema = originalSchema.cloneSchema();
        nextSchema.clearAttributeStatusClusteringAndTarget();

        nextSchema.setDescriptive(new IntervalCollection(intervalStringFromAttributes(inAttributes)));
        nextSchema.setTarget(new IntervalCollection(intervalStringFromAttributes(outAttributes)));
        nextSchema.setClustering(new IntervalCollection(intervalStringFromAttributes(outAttributes)));

        try {
            nextSchema.updateAttributeUse();
            nextSchema.addIndices(ClusSchema.ROWS);
        } catch (IOException | ClusException e) {
            e.printStackTrace();
        }

        return nextSchema;
    }

    /**
     * Checks if a given tree corresponds to the current tree (as defined by the lists of inputs and outputs)
     * */
    public boolean equalsToTree(SelectionAlgorithmTree otherTree) {
        if(! otherTree.getInAttributes().equals(inAttributes))
            return false;
        else if(! otherTree.getOutAttributes().equals(outAttributes))
            return false;
        return true;
    }

    /*
     * Methods related to altering the lists of in-and output attributes
     * */

    /**
     * Checks whether the given attribute index corresponds to an input attribute.
     * */
    public boolean hasInAttribute(int index) {
        return getInAttributes().contains(index);
    }
    /**
     * Adds the given attribute (by index) to the list of input attributes
     * */
    public void addInAttribute(int index) {
        inAttributes.add(index);
        Collections.sort(inAttributes);
    }
    /**
     * Removes the given attribute (by index) from the list of output attributes
     * */
    public void removeInAttribute(int index) {
        if(hasInAttribute(index)) {
            getInAttributes().remove(Integer.valueOf(index));
        }
    }

    /**
     * Checks whether the given attribute index corresponds to an output attribute.
     * */
    public boolean hasOutAttribute(int index) {
        return getOutAttributes().contains(index);
    }
    /**
     * Adds the given attribute (by index) to the list of output attributes
     * */
    public void addOutAttribute(int index) {
        outAttributes.add(index);
        Collections.sort(outAttributes);
    }
    /**
     * Removes the given attribute (by index) from the list of output attributes
     * */
    public void removeOutAttribute(int index) {
        if(hasOutAttribute(index))
            getOutAttributes().remove(Integer.valueOf(index));
    }


    /*
     * Additional (helper) methods
     * */

    /**
     * Important helper method for the method toClusSchema, which translates this abstract tree to a ClusSchema
     *
     * Returns a compact representation of the given attributes list.
     * The returned string contains numbers and can contain 2 special characters: ',' and '-'.
     *
     * The ClusSchema class uses an IntervalCollection. IntervalCollection requires a string.
     * This string is built here, based on the given list of attributes.
     *
     * Example
     * 		input: 0,1,2,5,6
     * 		output: 0-2,5-6
     *
     * '-' is used to indicate an interval.
     * ',' is used to indicate a concatenation of several intervals.
     */
    private String intervalStringFromAttributes(List<Integer> attributes) {
        Collections.sort(attributes);
        String returnString = "";
        int first = attributes.get(0); // the first number of the current interval
        int prev = attributes.get(0);
        for(int i = 1; i < attributes.size(); i++) {
            int current = attributes.get(i);
            if(current == prev + 1) { // we are in an interval
                prev = current;
            }
            else{ // interval has ended, make string of previous interval and update first and prev
                returnString += intervalString(first,prev) + ",";
                first = current;
                prev = current;
            }
        }
        returnString += intervalString(first,prev);
        return returnString;
    }

    /**
     * Returns a string representing the interval between low_limit and high_limit.
     * If low_limit == high_limit, the returned string is just low_limit (or high_limit since they are the same).
     * If low_limit != high_limit, the returned string is "low_limit-high_limit".
     *
     * Example:
     * 		input: 0,3
     * 		output: 0-3
     *
     * 		input: 2,2
     * 		output: 2
     */
    private String intervalString(int low_limit, int high_limit){
        return low_limit == high_limit ? ("" + low_limit) : (low_limit + "-" + high_limit);
    }

    /**
     * Yields a string that tells us the inputs and the outputs.
     * */
    public String getStringDescription() {
        return "In: " + inAttributes + ", out: " + outAttributes;
    }

    /**
     * Same thing as the above method, but now overriding another method.
     * */
    @Override
    public String toString() {
        return "In: " + inAttributes + ", out: " + outAttributes;
    }

    /**
     * A method that yields us a HashCode (Look into this!)
     * */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                // if deriving: appendSuper(super.hashCode()).
                        append(inAttributes).
                        append(outAttributes).
                        toHashCode();
    }

    /**
     * A redefinition of the standard equals method
     * */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SelectionAlgorithmTree))
            return false;
        if (obj == this)
            return true;

        SelectionAlgorithmTree rhs = (SelectionAlgorithmTree) obj;
        return new EqualsBuilder().
                // if deriving: appendSuper(super.equals(obj)).
                        append(inAttributes, rhs.getInAttributes()).
                        append(outAttributes, rhs.getOutAttributes()).
                        isEquals();
    }
}
