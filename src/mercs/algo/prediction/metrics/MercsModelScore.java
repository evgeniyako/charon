package mercs.algo.prediction.metrics;

import mercs.algo.selection.CombinedPerformance;


/**
 * MercsModelScore
 *
 * An object of this class contains two properties,
 * being the CombinedPerformance of the model it refers to and the weight of this model.
 */
public class MercsModelScore {

    private CombinedPerformance perf;
    private double modelWeight = 1.0;

    /**
     * Set the weight of the model
     * */
    public void setModelWeight(double weight) {
        this.modelWeight = weight;
    }

    /**
     * Return the weight of the model
     * */
    public double getModelWeight() {
        return modelWeight;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * Sets the CombinedPerformance (algo.selection)
     *
     * */
    public MercsModelScore(CombinedPerformance perf) {
        this.perf = perf;
    }

    /**
     * Get a attribute-specific weight for your prediction, based on the formula mentioned below.
     * */
    public double getPredictionWeightForAttribute(int idx) {
        return modelWeight * perf.getModelErrorByAttributeIndex(idx);
    }

}
