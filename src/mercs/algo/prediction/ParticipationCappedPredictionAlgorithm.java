package mercs.algo.prediction;

import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import mercs.model.EnsembleModel;
import mercs.model.MercsModel;

/**
 *                                  PredictionAlgorithm
 *                              IterativePredictionAlgorithm
 *                               SimplePredictionAlgorithm
 *                               |                  |
 * ModelWeightedPredictionAlgorithm                 InputCappedPredictionAlgorithm
 *                                                  |
 *                                                  ParticiPationCappedPredictionAlgorithm
 *
 * Class that implements a certain kind of prediction algorithm.
 * Here, a target can only be selected if the ratio of available models vs all potential models is high enough.
 *
 * */
public class ParticipationCappedPredictionAlgorithm extends InputCappedPredictionAlgorithm {

    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    private int[] modelsForTarget;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    @Override
    protected void init(EnsembleModel ensembleModel, ClusSchema relation, DataTuple input) {
        super.init(ensembleModel, relation, input);                             // Start the big initialisation method from the IterativePredictionAlgorithm

        modelsForTarget = new int[getNbAttributes()];                           // Array that keeps track how many models predict a given attribute
        for (MercsModel model : models) {
            for (ClusAttrType att : model.getSchema().getTargetAttributes()) {  // For every target attribute, of every model, increase count in modelsForTarget array.
                modelsForTarget[att.getIndex()]++;
            }
        }
    }

    /**
     * Attribute may start waiting if there's if this target is predicted by enough available models compared to
     * the total amount of models that COULD predict this attribute.
     */
    @Override
    protected boolean targetCanStartWaiting(int attributeIndex) {
        if (availableAttributes[attributeIndex]) {                                                                  // If this attribute is already available (predicted previously), then it should not go to the waiting list
            return false;
        } else {
            int numAvailable = activeModelTargetIndex.get(attributeIndex).cardinality();                            // The amount of models that have the given attribute as a target
            return ((double) numAvailable) / modelsForTarget[attributeIndex] >= modelInputAvailabilityThreshold;    // The ratio of the amount of available models vs all the potential models has to be large enough
        }
    }

    /**
     * An empty method
     * */
    @Override
    protected void afterPrediction() {
        // Nothing.
    }

}
