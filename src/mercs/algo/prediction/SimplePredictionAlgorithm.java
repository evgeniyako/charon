package mercs.algo.prediction;

import java.util.Arrays;

import clus.data.rows.DataTuple;
import clus.data.type.ClusSchema;
import clus.statistic.ClusStatistic;
import mercs.model.EnsembleModel;
import mercs.model.MercsModel;

public class SimplePredictionAlgorithm extends IterativePredictionAlgorithm{

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    protected boolean includeTrivialModels;
    private boolean terminateAfterIteration;

    /*
     * =======================================
     * METHODS
     * =======================================
     */


    /**
     * Returns a weighted prediction of the missing target attributes of the
     * given tuple.
     *
     * This differs only very little from the implementation in the Iterative Prediction Algorithm
     *
     * @param ensembleModel The ensembleModel that does the prediction
     * @param relation      A ClusSchema, this tells us what to predict from what.
     * @param input         A DataTuple object used as input data
     * @return This method yields a ClusStatistic, which is the typical output format for a Clus, and hence Mercs prediction
     */
    @Override
    public ClusStatistic predictWeighted(EnsembleModel ensembleModel, ClusSchema relation, DataTuple input) {
        includeTrivialModels = false;
        terminateAfterIteration = false;
        return super.predictWeighted(ensembleModel, relation, input);
    }

    /**
     * Model may activate if at least one of its source attributes is
     * known.
     */
    @Override
    protected boolean modelCanActivate(int modelIndex) {
        return includeTrivialModels || getNumberOfAvailableSourceAttributes(models.get(modelIndex)) > 0;
    }

    /**
     * Attribute may start waiting if there's at least one active model
     * that has this attribute as a target.
     */
    @Override
    protected boolean targetCanStartWaiting(int attributeIndex) {
        if (availableAttributes[attributeIndex]) {                                  // If this attribute is already available (predicted previously), then it should not go to the waiting list
            return false;
        } else {
            return activeModelTargetIndex.get(attributeIndex).cardinality() > 0;    // If at least one model has this attribute as a target
        }
    }

    /**
     * Waiting targets are predicted by default. This means that
     * missing attributes are imputed by distribution (this happens
     * automatically in Clus' implementation of decision trees.)
     */
    @Override
    protected boolean[] selectNextTargets() {
        printDebug("-- Selected all waiting targets.", 1);
        return Arrays.copyOf(waitingAttributes, waitingAttributes.length);
    }

    /**
     * Prediction is finished if all target attributes are predicted, or
     * if no additional prediction steps are possible.
     *
     * @return True if all attributes were predicted
     */
    @Override
    protected boolean predictionFinished() {
        if (terminateAfterIteration) return true;

        for (int i = 0; i < finalTargetAttributes.length; i++) {
            if (finalTargetAttributes[i] && !availableAttributes[i]) {
                printDebug("-- Found at least 1 unpredicted attribute: " + i + ".", 1);
                return false;
            }
        }
        return true;
    }

    /**
     * If there are no targets selected,
     * we try again with trivial models and afterwards we just stop after the final iteration.
     * */
    @Override
    protected void noTargetsSelected() {
        if (includeTrivialModels) {
            terminateAfterIteration = true;
        } else {
            includeTrivialModels = true;
        }
    }

    /**
     * What to do after prediction
     * */
    @Override
    protected void afterPrediction() {
        super.afterPrediction();
        printDebug("-- Clear previously active models & enable trivial inclusion.", 1);

        // Enable trivial model activation (this includes the trivial distribution-
        // based imputation of attributes that cannot otherwise be predicted with
        // chaining (i.e. value-based imputation).
        includeTrivialModels = true;
    }
}
