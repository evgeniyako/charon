package mercs.algo.prediction;

import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.ClusSchema;
import clus.data.type.NominalAttrType;
import clus.data.type.NumericAttrType;
import clus.statistic.ClusStatistic;

import mercs.algo.prediction.metrics.MercsModelScore;
import mercs.model.EnsembleModel;
import mercs.model.MercsModel;
import mercs.statistics.variabletargetvoting.VTVCombinedStat;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * IterativePredictionAlgorithm
 *
 * This algorithm is a first, general, implementation of a prediction algorithm.
 */
public abstract class IterativePredictionAlgorithm implements PredictionAlgorithm {
    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */
    protected ClusSchema relation;                          // The relation/schema (~ClusSchema)
    protected int iterationCount;                           // The number of the current iteration.
    protected int debug = 0;                                // Whether to print debug statements.


    protected ArrayList<MercsModel> models;                 // General list of all available models.
    /**
     * Monotonically shrinking set of inactive models.
     *
     * NB.: A linked list is a specific data structure that allows for easy insertion/removal
     * of elements at any place in the list.
     * */
    protected LinkedList<Integer> waitingModelIndex;
    protected ArrayList<MercsModelScore> modelScores;       // Contains statistics and weights for available models.
    /**
     * BitSet = Array of bits, which implies an array of boolean values
     *
     * Here, we have an ArrayList of multiple BitSets.
     * Bit i of set j is set to 1 if model i is active & attr. j is a target of that model
     */
    protected ArrayList<BitSet> activeModelTargetIndex;


    protected int nbAttributes;                             // Number of attributes.
    protected boolean[] descriptiveAttributes;              // Lists the descriptive attributes.
    protected boolean[] availableAttributes;                // i=true if i is available (~given or imputed)
    protected boolean[] targetsToCheck;                     // targets that may be checked for activation
    protected boolean[] waitingAttributes;                  // i=true if i is MAY (or COULD) be predicted in the next step
    protected boolean[] finalTargetAttributes;              // i=true if attribute i is a final target of the prediction.
    protected DataTuple inputAttributes;                    // The actual input data (~DataTuple object)

    protected VTVCombinedStat runningPrediction;            // Contains the imputed attributes.
    protected ArrayList<ClusStatistic> attrStats;           // Statistics per attribute (~ClusStatistic object)

    private boolean weightedPreds = false;

    /**
     * Returns the nbAttributes
     */
    protected int getNbAttributes() {
        return nbAttributes;
    }

    /**
     * Return the VTVCombinedStat object runningPrediction, that contains the imputed attributes.
     */
    protected ClusStatistic getPrediction() {
        return runningPrediction;
    }

    /**
     * Sets the boolean weightedPreds
     */
    public void setPredictWeighted(boolean predictWeighted) {
        this.weightedPreds = predictWeighted;
    }

    /**
     * Returns the boolean weightedPreds
     */
    public boolean isPredictWeighted() {
        return weightedPreds;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * init
     * Initialization of this object
     *
     * @param relation      The ClusSchema we provide
     * @param ensembleModel The ensemblemodel we will use, an EnsembleModel object
     * @param input         DataTuple object, the input object
     */
    protected void init(EnsembleModel ensembleModel, ClusSchema relation, DataTuple input) {
        // Fixing some properties
        printDebug("- Initialisation", 1);

        this.iterationCount = 0;
        this.nbAttributes = relation.getNbAttributes();
        this.inputAttributes = input.deepCloneTuple();  // Predicted attribute values will be overwritten. (so we copy the input)

        /*
        * We want to identify the role of the different attributes (different for every schema, which comes with every model). To do this,
        * we use the getAllAttrUse methode from the ClusSchema, which does exactly that.
        *
        * Given the int code ATTR_USE_ALL (0) or ATTR_USE_DESCRIPTIVE (1) properties of ClusAttrType (class that contains all kinds of information about a certain attribute),
        * it fetches the 0th or 1st row of the m_AllAttrUse property of the ClusSchema, which is a
        * ClusAttrType [][] two dimensional array. Hence the result is a one-dimensional ClusAttrType array.
        *
        * The method getAllAttrUse returns a specific row of the 2D m_AllAttrUse property of ClusSchema,
        * hence, given a certain attribute use, it returns an array of ClusAttrTypes for that specific kind of attribute (target, descriptive, etc)
        *
        * */
        ClusAttrType[] allAttrs = relation.getAllAttrUse(ClusAttrType.ATTR_USE_ALL);
        ClusAttrType[] descAttrs = relation.getAllAttrUse(ClusAttrType.ATTR_USE_DESCRIPTIVE);
        int otherInd = 0;

        /*
        * This loop sets Nominal and Numeric (the targets) attributes with methods from the ClusAttrType class
        * It does not set the real values, since we have to *predict* the targets, so we set the corresponding input (the DataTuple object) values to dummy values.
        * NB.:This is what the comment at the inputAttributes line in this method already referred to, the override of the targets.
        *
        * */
        for (ClusAttrType allAttr : allAttrs) {     // For each element (~index of an attribute in our problem) in allAttr
            /*
            * If the otherInd-th item in descAttr is the same as the current item in allAttr (~refers to the same attribute)
            * This means, if the current attribute from allAttr IS a descriptive one,
            * we add one to otherInd (~we look further)
            *
            * NB.: An allAttr object or an descAttr object have different properties, for instance here we fetch the index
            * of the actual (original) attribute where it refers to. If these indices are the same, then we know for a fact that
            * the attribute is a descriptive one.
            * */
            if (otherInd < descAttrs.length && allAttr.getIndex() == descAttrs[otherInd].getIndex()) {
                otherInd++;
            } else {                                // If the current attribute is a target (i.e. non-descriptive) attribute
                switch (allAttr.getTypeIndex()) {
                    case 0:    // NominalAttrType
                        /*
                        * Set the actual value of the attribute in the input DataTuple object to a dummy value (i.e. # nominal values)
                        * */
                        ((NominalAttrType) allAttr).setNominal(inputAttributes, ((NominalAttrType) allAttr).getNbValues());
                        break;
                    case 1:    // NumericAttrType
                        /*
                        * Set the actual value of the attribute in the input DataTuple object to a dummy value (i.e. missing = +infty)
                        * */
                        ((NumericAttrType) allAttr).setNumeric(inputAttributes, NumericAttrType.MISSING);
                        break;
                    default:
                        // Do nothing
                }
            }
        }

        /*
        Set the ClusSchema and create a VTVCombinedStat object, given a ClusAttrType[] array,
        which is the m_AllAttrUse ClusAttrType [ATTR_USE_TARGET][] row.
        This to keep statistics on all the target attributes in this model.
        */
        this.relation = relation;
        this.runningPrediction = new VTVCombinedStat(relation.getAllAttrUse(ClusAttrType.ATTR_USE_TARGET));

        models = new ArrayList<MercsModel>(ensembleModel.getModels());                      // Compile an ArrayList of all my MercsModels contained in the ensembleModel
        modelScores = new ArrayList<MercsModelScore>(models.size());
        waitingModelIndex = new LinkedList<Integer>();

        /**
         * For each model in the ensemble, do this loop
         * */
        for (int i = 0; i < models.size(); i++) {
            waitingModelIndex.add(i);
            modelScores.add(new MercsModelScore(models.get(i).getCombinedPerformance()));       // add a new MercsModelScore, which is based on the combinedPerformance for model index i

            activeModelTargetIndex = new ArrayList<BitSet>(getNbAttributes());                  // Initialize an ArrayList of BitSets, one for each attribute
            for (int j = 0; j < getNbAttributes(); j++)
                activeModelTargetIndex.add(new BitSet(models.size()));                          // For each attribute, a BitSet with size # models


            targetsToCheck = new boolean[getNbAttributes()];                                    // Attributes that need to be checked for activation. Initially none.
            waitingAttributes = new boolean[getNbAttributes()];                                 // Attributes waiting to be predicted. Initially none.
            availableAttributes = new boolean[getNbAttributes()];                               // Attributes available. Initially all descriptive attributes of the relation.
            descriptiveAttributes = new boolean[getNbAttributes()];                             // Descriptive attributes of the relation.
            finalTargetAttributes = new boolean[getNbAttributes()];                             // Attributes needed to predict. All target attributes of the relation.

            for (ClusAttrType at : relation.getTargetAttributes()) {                            // For each element of the ClusAttrType[] array of target attributes (i.e. for each target attr.)
                finalTargetAttributes[at.getIndex()] = true;                                    // Set the corresponding element in the boolean array finalTargetAttributes[] to one
            }
            for (ClusAttrType at : relation.getDescriptiveAttributes()) {                       // For each element of the ClusAttrType[] array of descriptive attributes (i.e. for each descr. attr.)
                availableAttributes[at.getIndex()] = true;                                      // Indicate that it is an available attribute
                descriptiveAttributes[at.getIndex()] = true;                                    // Indicate that it is a descriptive attribute
            }
        }

    }

    /**
     * predictNextTargets
     *
     *
     * */
    private void predictNextTargets(boolean[] selectedTargets) {
        // Collect models and targets.
        LinkedList<ClusAttrType> attrs = new LinkedList<>();
        BitSet selectedModels = new BitSet(models.size());
        /*
        * This loop composes a list (BitSet) of models, that are appropriate for the given collection of targets.
        */
        for (int i = 0; i < selectedTargets.length; i++) {
            if (selectedTargets[i]) {                                   // If the target was indeed selected
                BitSet matchingModels = activeModelTargetIndex.get(i);  // Get the bitset belonging to a certain target (BitSet size is # models)
                selectedModels.or(matchingModels);                      // Perform an or with both of these bitsets (1 if it is selected or matching)
                attrs.add(relation.getAttrType(i));                     // Add this attribute (~ClusAttrType) to the linkedlist
            }
        }



        // Print debug stuff.
        printDebug("-- Casting predictions.", 1);
        if (debug >= 2) {
            String debugSuffix = "";
            for (int i = selectedModels.nextSetBit(0); i >= 0; i = selectedModels.nextSetBit(i + 1)) {
                if (!debugSuffix.equals("")) debugSuffix += ",";
                debugSuffix += i;
            }
            printDebug("--- Selected participating models (" + debugSuffix + ")", 2);
        }
        printDebug("--- Running component predictions.", 2);

        // Collect statistics
        ArrayList<ClusStatistic> componentStats = new ArrayList<>();
        ArrayList<MercsModelScore> componentPerformances = null;
        if (isPredictWeighted()) componentPerformances = new ArrayList<>();

        /*
         * nextSetBit: Returns the index of the first bit that is set to true that occurs on or after the specified starting index.
         * This implies that this loop gets executed for each one of the selectedModels.
         *
         *
         */
        for (int i = selectedModels.nextSetBit(0); i >= 0; i = selectedModels.nextSetBit(i + 1)) {
            MercsModel currentModel = models.get(i);                    // Select the current model
            componentStats.add(currentModel.predict(inputAttributes));  // Do a prediction from input with this model, add it to componentStats
            if (isPredictWeighted()) {
                assert componentPerformances != null;
                componentPerformances.add(getModelScore(i));            // If we have to weighted prediction, we also need to collect the performance
            }
        }

        // Cast votes using Variable Target Voting statistic
        printDebug("--- Casting votes.", 2);

        // Create combStat, a VTVCombinedStat object, that keeps a prediction based on all the individual models' prediction
        VTVCombinedStat combStat = new VTVCombinedStat(attrs.toArray(new ClusAttrType[0]));
        if (isPredictWeighted()) {
            combStat.voteWeighted(componentStats, componentPerformances);
        } else {
            combStat.vote(componentStats);
        }

        // Extend datatuple & running prediction.
        printDebug("--- Merging statistics.", 2);
        combStat.predictTuple(inputAttributes);         // Do a real prediction
        runningPrediction.mergeStatistics(combStat);    // Merge this combStat object with another VTVCombinedStat object (i.e. runningPrediction)

        for (int i = 0; i < selectedTargets.length; i++) {
            if (selectedTargets[i]) {
                waitingAttributes[i] = false;
                availableAttributes[i] = true;          // Since we have now predicted all these targets, they have become available attributes
            }
        }
    }

    /**
     * Returns a weighted prediction of the missing target attributes of the
     * given tuple.
     *
     * How this prediction is done exactly, depends on the implementing subclass.
     *
     * @param ensembleModel The ensembleModel that does the prediction
     * @param input A DataTuple object used as input data
     * @param relation A ClusSchema, this tells us what to predict from what.
     * @return This method yields a ClusStatistic, which is the typical output format for a Clus, and hence Mercs prediction
     */
    @Override
    public ClusStatistic predictWeighted(EnsembleModel ensembleModel, ClusSchema relation,
                                         DataTuple input) {
        // Printing general info for debug purposes.
        printDebug("\nStarted prediction", 1);
        printDebug(" Input:", 2);
        printDebug("  " + ensembleModel.getModelSize() + " component models:", 2);
        for (MercsModel m : ensembleModel.getModels()) {
            printDebug("   " + m.getModelInputAndTargetAttributes(), 2);
        }
        printDebug("  relationship: " + relation.getDescriptive().toString()
                + " -> " + relation.getTarget().toString(), 2);

        // Initialization (calling the other important method of this class)
        init(ensembleModel, relation, input);

        do {
            iterationCount++;
            printDebug("- Iteration " + iterationCount, 1);


            /**
             * A model being activated means that enough is known about the descriptive
             * attributes of this model to justify using the model.
             *
             * This is a LOCAL condition.
             * */
            extendActiveModels();

            /**
             * A target that is waiting means that enough models can predict it,
             * so that it makes sense to predicting this target using the current ensemble of relevant active models.
             * This is a GLOBAL condition
             * */
            extendWaitingTargets();

            /**
             * Selection implies that it will be predicted in this iteration.
             * These targets are selected from the waiting list of targets.
             * */
            boolean[] toPredict = selectNextTargets();

            // Check if any target was selected. If not, we might need to intervene
            boolean selected = false;
            for (boolean target : toPredict) {
                if (target) {
                    selected = true;
                    break;
                }
            }

            if (selected) {
                // predict selected targets and add information to known attributes.
                predictNextTargets(toPredict);
                afterPrediction();
            } else {
                noTargetsSelected();
            }

        } while (!predictionFinished());

        printDebug("- Finished prediction.", 1);
        return getPrediction();
    }

    /**
     * extendActiveModels
     *
     * Run through our waiting list of models and we activate them if we can. We use the modelCanActivate method to asses this.
     *
     * When a model is added to the activeModels list it means we believe we know enough to justify the use of said model.
     * */
    private void extendActiveModels() {
        String debugSuffix = "";

        //boolean[] targetsChanged = new boolean[getNbAttributes()];
        Iterator<Integer> it = waitingModelIndex.iterator();                // Using default methods, we construct an iterator over the LinkedList of waitingModels
        while (it.hasNext()) {
            int modelIndex = it.next();                                     // Fetch the next modelIndex
            if (modelCanActivate(modelIndex)) {                             // If it can activate
                activateModel(modelIndex);                                  // Do so
                it.remove();                                                // And remove from the waitinglist

                if (!debugSuffix.equals("")) debugSuffix += ",";
                debugSuffix += modelIndex;                                  // If the model was activated, we add it to the debugSuffix
            }
        }
        if (debug >= 1) {                                                   // If the debug level is appropiate, we print the activated models
            printDebug("-- Activated models (" + debugSuffix + ")", 1);
        }
    }

    /**
     * Method to activate a model with a specific index
     * */
    private void activateModel(int modelIndex) {
        ClusSchema schema = models.get(modelIndex).getSchema();     // get the ClusSchema of this particular model.
        /*
        * For each target attribute
        *
        * NB.: A ClusAttrType object is basically just a specific attribute of the input DataTuple,
        * it makes it easy to access all kinds of properties of that one specific attribute.
        * */
        for (ClusAttrType att : schema.getTargetAttributes()) {
            int attIndex = att.getIndex();                          // Get the index of the target attribute under consideration
            if (descriptiveAttributes[attIndex]) continue;          // If this is also a descriptive attribute (this list is compiled from a given relation in the init method)
            activeModelTargetIndex.get(attIndex).set(modelIndex);   // Get the bitset that belongs to this attribute, and set the entry belonging to the current model to one
            targetsToCheck[attIndex] = true;                        // Indicate that this attribute needs to be checked for activation
        }
    }

    /**
     * extendWaitingTargets
     *
     * We run over each target that is in the targetsToCheck list, if it meets the targetCanStartWaiting condition,
     * if it is not yet in the waitingAttributes list, we put it there and erase it from the targetsToCheck list.
     * */
    private void extendWaitingTargets() {
        String debugSuffix = "";
        for (int i = 0; i < targetsToCheck.length; i++) {
            if (targetsToCheck[i] && !waitingAttributes[i]) {
                if (!debugSuffix.equals("")) debugSuffix += ",";
                debugSuffix += (i + 1);
                if (targetCanStartWaiting(i)) {
                    waitingAttributes[i] = true;
                    targetsToCheck[i] = false;
                    debugSuffix += "*";
                }
            }
        }
        printDebug("-- Activated targets (" + debugSuffix + ")", 1);
    }

    /**
     * getModelScore
     *
     * Fetches the MercsModelScore for model with index modelIndex of the ensemble.
     * These scores are collected in the modelScores ArrayList, which is constructed in the init method.
     * */
    protected MercsModelScore getModelScore(int modelIndex) {
        return modelScores.get(modelIndex);
    }

    /**
     * Given a model, we collect all the descriptive attributes in a ClusAttrType[] array.
     * We then check if these attributes are also *available* attributes at the moment (checking if they occur in the availableAttributes)
     * If so, we add the attribute to the LinkedList<ClusAttrType> res.
     * */
    protected LinkedList<ClusAttrType> getAvailableSourceAttributesOf(MercsModel model) {
        ClusAttrType[] modelSourceAttrs = model.getSchema().getDescriptiveAttributes();
        LinkedList<ClusAttrType> res = new LinkedList<ClusAttrType>();
        for (ClusAttrType att : modelSourceAttrs) {
            if (availableAttributes[att.getIndex()])
                res.add(att);
        }
        return res;
    }

    /**
     * Get the number of available source attributes of a given model
     * */
    protected int getNumberOfAvailableSourceAttributes(MercsModel model) {
        return getAvailableSourceAttributesOf(model).size();
    }



    /*
     * =======================================
     * ADDITIONAL AND ABSTRACT METHODS
     * =======================================
     */

    /**
     * Not called by default, but may be used by implementations of this class.
     */
    protected void clearAllActiveModels() {
        activeModelTargetIndex.forEach(BitSet::clear);
    }

    protected void afterPrediction() {
        // May be overwritten by sub class.
        // I think this can also be an abstract method
    }

    protected abstract boolean modelCanActivate(int modelIndex);

    protected abstract boolean targetCanStartWaiting(int attributeIndex);

    protected abstract boolean[] selectNextTargets();

    protected abstract boolean predictionFinished();

    protected abstract void noTargetsSelected();


    /**
     * Debug related functions
     * */

    /**
     * Change the debug level. A higher debug level causes more intermediate
     * status updates to be printed to stdout.
     *
     * @param lvl Positive integer debug level.
     *            0: Silent (default)
     *            1: Report start of each new step in algorithm.
     *            2: Report selections and decision points.
     */
    public void setDebugLevel(int lvl) {
        this.debug = lvl;
    }

    protected void printDebug(String str, int minLevel) {
        if (debug >= minLevel) System.out.println(str);
    }

}


