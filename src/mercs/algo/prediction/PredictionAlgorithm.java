package mercs.algo.prediction;

import clus.data.rows.DataTuple;
import clus.data.type.ClusSchema;
import clus.statistic.ClusStatistic;

import mercs.model.EnsembleModel;

/**
 * PredictionAlgorithm
 *
 * An interface for prediction algorithms.
 *
 * Prediction algorithms are able to predict target attributes from a given partial data tuple.
 *
 * They are a component of mercs models.
 *
 *
 */
public interface PredictionAlgorithm {
    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Returns a weighted prediction of the missing target attributes of the
     * given tuple.
     *
     * How this prediction is done exactly, depends on the implementing subclass.
     *
     * @param ensembleModel The ensembleModel that does the prediction
     * @param input A DataTuple object used as input data
     * @param relation A ClusSchema, this tells us what to predict from what.
     * @return This method yields a ClusStatistic, which is the typical output format for a Clus, and hence Mercs prediction
     */
    ClusStatistic predictWeighted(EnsembleModel ensembleModel, ClusSchema relation, DataTuple input);

    /**
     * Simple setter for a boolean property, predictWeighted.
     * */
    void setPredictWeighted(boolean value);
}
