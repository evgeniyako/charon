package mercs.algo.prediction;

import clus.data.rows.DataTuple;
import clus.data.type.ClusSchema;

import mercs.model.EnsembleModel;
import mercs.model.MercsModel;

import java.util.Arrays;

/**
 *                                  PredictionAlgorithm
 *                              IterativePredictionAlgorithm
 *                               SimplePredictionAlgorithm
 *                               |                  |
 * ModelWeightedPredictionAlgorithm                 InputCappedPredictionAlgorithm
 *                                                  |
 *                                                  ParticiPationCappedPredictionAlgorithm
 *
 * Class that implements a certain kind of prediction algorithm. Here, models get considered if a
 * certain percentage of their input attributes is available to them.
 * */
public class InputCappedPredictionAlgorithm extends SimplePredictionAlgorithm {

    /*
     * =======================================
     * PROPERTIES (Decl, Get, Set)
     * =======================================
     */

    protected double modelInputAvailabilityThreshold;

    /**
     * Sets the modelInputAvailabilityThreshold
     * */
    public void setModelInputAvailabilityThreshold(double newThreshold) {
        this.modelInputAvailabilityThreshold = newThreshold;
    }

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    @Override
    protected void init(EnsembleModel ensembleModel, ClusSchema relation, DataTuple input) {
        super.init(ensembleModel, relation, input);     // Start the big initialisation method from the IterativePredictionAlgorithm
        modelInputAvailabilityThreshold = 0.8;          // Add a threshold, how much do we need to know from a model's input?
    }

    /**
     * Model may activate if the ratio of available descriptive attributes vs total descriptive attributes is sufficient.
     * In other words: If the model has enough of its inputs, we trust it.
     */
    @Override
    protected boolean modelCanActivate(int modelIndex) {
        if (includeTrivialModels) {                                             // If trivial models are okay, set the weight of this model to one
            modelScores.get(modelIndex).setModelWeight(1.0);
            return true;
        }

        MercsModel model = models.get(modelIndex);
        int numAvailable = getNumberOfAvailableSourceAttributes(model);         // Get the number of available source attributes
        int numTotal = model.getSchema().getNbDescriptiveAttributes();          // Get the number of total descriptive attributes
        double newScore = ((double) numAvailable) / numTotal;                   // Score based on the ratio of these two
        //modelScores.get(modelIndex).setModelWeight(newScore);

        return (newScore >= modelInputAvailabilityThreshold);                   // Return yes/no based on threshold
    }

    /**
     * What to do when no targets can be selected for prediction.
     * Basically, lowering the standards.
     * */
    @Override
    protected void noTargetsSelected() {
        if (modelInputAvailabilityThreshold >= 0.1) {   // Lower the threshold with 10%, if possible
            modelInputAvailabilityThreshold -= 0.1;
        } else {
            super.noTargetsSelected();
        }
    }

    /**
     * Indicate that no targets need checking
     * */
    @Override
    protected void afterPrediction() {
        // No dynamic target activation. Needs only be checked when contributing model is activated.
        Arrays.fill(targetsToCheck, false);
    }

}
