package mercs.algo.prediction;

import mercs.model.MercsModel;

/**
 * Interface PredictionAlgorithm
 *  |
 *  abstract IterativePredictionAlgorithm
 *   |
 *  SimplePredictionAlgoritm
 *   |
 *   ModelWeightedPredictionAlgorithm
 *
 * Only differs from its parent in the modelCanActivateMethod
 *
 * */
public class ModelWeightedPredictionAlgorithm extends SimplePredictionAlgorithm{

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Model may activate if at least one of its source attributes is
     * known (~available).
     *
     * We now also fix the weights according to the current scenario. If trivial models are included, we can just set the weights to one.
     *
     * Otherwise, we give all the models weights (available input (source) attributes)/(total descriptive attributes)
     */
    @Override
    protected boolean modelCanActivate(int modelIndex) {
        // If we include trivials, all the weights are equal to one
        if (includeTrivialModels) {
            modelScores.get(modelIndex).setModelWeight(1.0);
            return true;
        }

        MercsModel model = models.get(modelIndex);
        int numAvailable = getNumberOfAvailableSourceAttributes(model);
        int numTotal = model.getSchema().getNbDescriptiveAttributes();
        modelScores.get(modelIndex).setModelWeight(((double) numAvailable) / numTotal);

        return (numAvailable > 0);
    }
}
