package mercs.statistics.variabletargetvoting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import clus.data.type.ClusAttrType;
import clus.data.type.NominalAttrType;
import clus.main.Settings;
import clus.statistic.ClassificationStat;
import clus.statistic.ClusStatistic;
import mercs.algo.prediction.metrics.MercsModelScore;

/**
 * VTVClassificationStat
 *
 * This class is a generalization of the ClassificationStat statistics class (in Clus),
 * removing the assumption that every statistic is backed by the same
 * attribute schema, which evidently does not hold anymore in the Mercs case.
 */
public class VTVClassificationStat extends ClassificationStat {

    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    private static final long serialVersionUID = Settings.SERIAL_VERSION_ID;

    /**
     * Each attribute type has a static global index. This statistic
     * tracks a subset of attribute types.
     * This map is used to link global indices to local storage.
     */
    protected final HashMap<Integer,Integer> m_attrIndexMap;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     * */
    public VTVClassificationStat(NominalAttrType[] nomAtts) {
        super(nomAtts);                                     // Call to the Clus class, just passing the attributes
        m_attrIndexMap = new HashMap<Integer,Integer>();    // Creating our map of local indices to the real attribute indices
        for (int i1 = 0; i1 < nomAtts.length; i1++) {
            int i2 = nomAtts[i1].getIndex();
            m_attrIndexMap.put(i2, i1);
        }
    }

    /*
    * Voting methods
    * */

    /**
     * Differences with original:
     * - attributes are mapped, same indexing is not assumed
     * - sum_weights can be different per attribute
     *
     * Majority vote is just a vote with all equal weights, so we do not need to provide an array with weights.
     *
     * NB: TODO: m_Sumweight is still meaningless. Might need fixing.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void voteMajority(ArrayList votes) {
        voteWeighted(votes, null);
    }

    /**
     * Weighted vote method
     * */
    public void voteWeighted(ArrayList<ClassificationStat> votes, ArrayList<MercsModelScore> perfs) {
        reset();
        int nb_votes = votes.size();
        m_SumWeight = nb_votes;

        // Count number of votes per attribute.
        double[] voteSumWeights = new double[m_Attrs.length];
        for (int j = 0; j < votes.size(); j++) {
            ClassificationStat vote = votes.get(j);

            for (ClusAttrType at : vote.m_Attrs) {
                if (m_attrIndexMap.containsKey(at.getIndex())) {
                    if (perfs == null) {
                        voteSumWeights[m_attrIndexMap.get(at.getIndex())] += 1;
                    } else {
                        voteSumWeights[m_attrIndexMap.get(at.getIndex())] += perfs.get(j).getPredictionWeightForAttribute(at.getIndex());
                    }
                }
            }
        }
        m_SumWeights = voteSumWeights;

        for (int j = 0; j < nb_votes; j++){
            ClassificationStat vote = votes.get(j);
            for (int i = 0; i < vote.m_NbTarget; i++){
                int globalIndex = vote.m_Attrs[i].getIndex();
                if (!m_attrIndexMap.containsKey(globalIndex)) {
                    continue;
                }
                int localIndex = m_attrIndexMap.get(globalIndex);

                if (perfs == null) {
                    m_ClassCounts[localIndex][vote.getNominalPred()[i]]++;
                } else {
                    m_ClassCounts[localIndex][vote.getNominalPred()[i]] += perfs.get(j).getPredictionWeightForAttribute(globalIndex);
                }


            }
        }
        calcMean();
    }

    /**
     * Differences with original:
     * - attributes are mapped, same indexing is not assumed
     */
    @Override
    public void addVote(ClusStatistic vote) {
        ClassificationStat or = (ClassificationStat) vote;
        m_SumWeight += or.m_SumWeight;
        for (int i = 0; i < m_NbTarget; i++) {
            int globalIndex = or.m_Attrs[i].getIndex();
            if (!m_attrIndexMap.containsKey(globalIndex)) {
                continue;
            }
            int localIndex = m_attrIndexMap.get(globalIndex);
            m_SumWeights[localIndex] += or.m_SumWeights[i];
            double[] my = m_ClassCounts[localIndex];
            for (int j = 0; j < my.length; j++) my[j] += or.getProportion(i, j);
        }
    }

    /**
     * Differences with original:
     * - addVote is only called once per 'vote', whereas it was called 'nbAttributes'
     *   times in the original. I think this was an error, which didn't affect the
     *   result due to its symmetry. Not so in our case.
     */
    @Override
    @SuppressWarnings({ "rawtypes" })
    public void voteProbDistr(ArrayList votes) {
        reset();

        for (Object vote : votes) {
            ClassificationStat voteStat = (ClassificationStat) vote;
            addVote(voteStat);
        }

        calcMean();
    }

    /*
    * Statistics-related methods
    * */

    /**
     * Merges the statistics of two VTVClassificationStat objects.
     *
     * Existing attributes are overwritten. New attributes are added to the tail
     * end of the attribute list.
     *
     * @param m_ClassStat
     * @return index locations of the added/modified attributes, in order.
     */
    public int[] mergeStatistics(VTVClassificationStat m_ClassStat) {

        // Preallocate result
        int[] newIdxs = new int[m_ClassStat.m_NbTarget];

        // Count nb of new attributes.
        int nbNew = 0;
        for (NominalAttrType att : m_ClassStat.m_Attrs) {
            if (!m_attrIndexMap.containsKey(att.getIndex())) nbNew++;
        }

        // Read old data into dynamic data structures
        m_Attrs = Arrays.copyOf(m_Attrs, m_Attrs.length + nbNew);
        m_ClassCounts = Arrays.copyOf(m_ClassCounts, m_ClassCounts.length + nbNew);
        if (!isCalcMean()) calcMean();
        m_MajorityClasses = Arrays.copyOf(m_MajorityClasses, m_MajorityClasses.length + nbNew);
        m_SumWeights = Arrays.copyOf(m_SumWeights, m_SumWeights.length + nbNew);

        // Extend
        for (int i = 0; i < m_ClassStat.m_NbTarget; i++) {
            NominalAttrType att = m_ClassStat.getAttribute(i);
            int localIndex = m_attrIndexMap.size();
            if (m_attrIndexMap.containsKey(att.getIndex())) {
                localIndex = m_attrIndexMap.get(att.getIndex());
            } else {
                m_Attrs[localIndex] = att;
                m_attrIndexMap.put(att.getIndex(), localIndex);
            }
            newIdxs[i] = localIndex;
            m_ClassCounts[localIndex] = m_ClassStat.m_ClassCounts[i];
            m_MajorityClasses[localIndex] = m_ClassStat.m_MajorityClasses[i];
            m_SumWeights[localIndex] = m_ClassStat.m_SumWeights[i];
        }

        // Update attribute count
        this.m_NbTarget = m_Attrs.length;

        return newIdxs;
    }

    /**
     * Clone this object
     * */
    @Override
    public ClusStatistic cloneStat() {
        VTVClassificationStat res = new VTVClassificationStat(m_Attrs);
        res.m_Training = m_Training;
        return res;
    }

    /**
     * Get a single statistic in the correct format.
     * */
    public String getSingleStat(int idx){
        return m_Attrs[idx].getValue(m_MajorityClasses[idx]);
    }

}
