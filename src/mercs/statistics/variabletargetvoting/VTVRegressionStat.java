package mercs.statistics.variabletargetvoting;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import clus.data.type.NumericAttrType;
import clus.main.Settings;
import clus.statistic.ClusStatistic;
import clus.statistic.RegressionStat;
import clus.statistic.RegressionStatBase;
import clus.util.ClusFormat;
import mercs.algo.prediction.metrics.MercsModelScore;

/**
 * VTVRegressionStat
 *
 * This class is a generalization of the RegressionStat statistics class (in Clus),
 * removing the assumption that every statistic is backed by the same
 * attribute schema, which evidently does not hold anymore in the Mercs case.
 */
public class VTVRegressionStat extends RegressionStat{
    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    private static final long serialVersionUID = Settings.SERIAL_VERSION_ID;

    /**
     * Each attribute type has a static global index (~the code for the attribute type).
     *
     * This statistic tracks a subset of attribute types.
     * This map is used to link global indices to local storage.
     */
    protected final HashMap<Integer, Integer> m_attrIndexMap;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     * */
    public VTVRegressionStat(NumericAttrType[] numAtts) {
        super(numAtts);                                     // Call to the Clus class, just passing the attributes
        m_attrIndexMap = new HashMap<Integer, Integer>();   // Creating our map of local indices to the real attribute indices
        for (int i1 = 0; i1 < numAtts.length; i1++) {
            int i2 = numAtts[i1].getIndex();
            m_attrIndexMap.put(i2, i1);
        }
    }

    /*
    * Voting methods
    * */

    /**
     * Vote method, without weights
     * */
    @Override
    public void vote(ArrayList votes) {
        voteWeighted(votes, null);
    }

    /**
     * Weighted vote method
     * I have to go over this in detail
     * */
    public void voteWeighted(ArrayList<RegressionStatBase> rVotes, ArrayList<MercsModelScore> perfs) {
        // Forced to cast because Clus uses bad (nonexistant?) typing.

        // Count number of votes per attribute.
        double[] voteSumWeights = new double[m_NbAttrs];
        double[][] voteWeights = new double[rVotes.size()][m_NbAttrs];
        for (int j = 0; j < rVotes.size(); j++) {
            RegressionStatBase vote = rVotes.get(j);
            for (int i = 0; i < vote.m_NbAttrs; i++) {
                int globalIndex = vote.m_Attrs[i].getIndex();
                if (m_attrIndexMap.containsKey(globalIndex)) {
                    //nbVotes[m_attrIndexMap.get(at.getIndex())] += 1;
                    int localIndex = m_attrIndexMap.get(globalIndex);
                    if (perfs == null) {
                        voteSumWeights[localIndex] += 1;
                    } else {
                        voteWeights[j][localIndex] = perfs.get(j).getPredictionWeightForAttribute(globalIndex);
                        voteSumWeights[localIndex] += voteWeights[j][localIndex];
                    }
                }
            }
        }

        // Start casting votes.
        reset();
        m_Means = new double[m_NbAttrs];
        for (int j = 0; j < rVotes.size(); j++) {
            RegressionStatBase vote = rVotes.get(j);
            for (int i = 0; i < vote.m_NbAttrs; i++) {
                int globalIndex = vote.m_Attrs[i].getIndex();
                if (!m_attrIndexMap.containsKey(globalIndex)) {
                    continue;
                }
                int localIndex = m_attrIndexMap.get(globalIndex);
                if (perfs == null) {
                    m_Means[localIndex] += vote.m_Means[i] / voteSumWeights[localIndex];
                } else {
                    m_Means[localIndex] += voteWeights[j][localIndex] * (vote.m_Means[i] / voteSumWeights[localIndex]);
                }
            }
        }
    }

    /*
    * Statistics-related methods
    * */

    /**
     * Merges the statistics of two VTVRegressionStat objects.
     *
     * Existing attributes are overwritten. New attributes are added to the tail
     * end of the attribute list.
     *
     * @param m_RegStat
     * @return index locations of the added/modified attributes, in order.
     */
    public int[] mergeStatistics(VTVRegressionStat m_RegStat) {

        // Note: m_SumWeight is not carried over. Not sure how it applies.

        // Preallocate result
        int[] newIdxs = new int[m_RegStat.m_NbAttrs];

        // Count nb of new attributes.
        int nbNew = 0;
        for (NumericAttrType att : m_RegStat.m_Attrs) {
            if (!m_attrIndexMap.containsKey(att.getIndex()))
                nbNew++;
        }

        // Read old data into dynamic data structures
        m_Attrs = Arrays.copyOf(m_Attrs, m_Attrs.length + nbNew);
        if (m_Means == null) calcMean();
        m_Means = Arrays.copyOf(m_Means, m_Means.length + nbNew);
        m_SumSqValues = Arrays.copyOf(m_SumSqValues, m_SumSqValues.length + nbNew);
        m_SumValues = Arrays.copyOf(m_SumValues, m_SumValues.length + nbNew);
        m_SumWeights = Arrays.copyOf(m_SumWeights, m_SumWeights.length + nbNew);

        // Extend
        for (int i = 0; i < m_RegStat.m_NbAttrs; i++) {
            NumericAttrType att = m_RegStat.getAttribute(i);
            int localIndex = m_attrIndexMap.size();
            if (m_attrIndexMap.containsKey(att.getIndex())) {
                localIndex = m_attrIndexMap.get(att.getIndex());
            } else {
                m_Attrs[localIndex] = att;
                m_attrIndexMap.put(att.getIndex(), localIndex);
            }
            newIdxs[i] = localIndex;
            m_Means[localIndex] = m_RegStat.m_Means[i];
            m_SumSqValues[localIndex] = m_RegStat.m_SumSqValues[i];
            m_SumValues[localIndex] = m_RegStat.m_SumValues[i];
            m_SumWeights[localIndex] = m_RegStat.m_SumWeights[i];
        }

        // Update attribute count
        this.m_NbAttrs = m_Attrs.length;

        return newIdxs;
    }

    /**
     * Clone this object
     * */
    @Override
    public ClusStatistic cloneStat() {
        VTVRegressionStat clone = new VTVRegressionStat(m_Attrs);
        clone.m_Training = m_Training;
        return clone;
    }

    /**
     * Get a single statistic in the correct format.
     * */
    public String getSingleStat(int idx) {
        NumberFormat fr = ClusFormat.SIX_AFTER_DOT;
        return fr.format(m_Means[idx]);
    }


}
