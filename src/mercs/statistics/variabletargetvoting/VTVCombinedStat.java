package mercs.statistics.variabletargetvoting;

import clus.data.rows.DataTuple;
import clus.data.type.ClusAttrType;
import clus.data.type.NominalAttrType;
import clus.data.type.NumericAttrType;
import clus.main.Settings;
import clus.statistic.*;
import mercs.algo.prediction.metrics.MercsModelScore;
import org.apache.commons.lang3.NotImplementedException;

import java.util.ArrayList;
import java.util.LinkedList;


/**
 * VTVCombinedStat
 *
 * This kind of object gets created e.g. in the IterativePredictionAlgorithm, where it contains
 * predictions for target attributes as different models are used to predict different targets.
 *
 * So, this object basically holds results of predictions.
 *
 * It extends the ClusStatistic class, which collects all kinds of information of this kind.
 *
 * Basically, it consists of two parts, specific to the two possible situations for a target attribute,
 * which is either nominal or numeric. These parts implement a lot of methods specifically for their case.
 * This class is then used to unite these two situations in one class.
 *
 */
public class VTVCombinedStat extends ClusStatistic {
    /*
     * ===============================================
     * Properties (E.g.: Decl, Get, Set, isValid, ...)
     * ===============================================
     */

    private static final long serialVersionUID = Settings.SERIAL_VERSION_ID;

    private VTVRegressionStat m_RegStat;
    private VTVClassificationStat m_ClassStat;
    private final ArrayList<int[]> m_AttrIndex;

    // Integer codes for the different cases
    private static final int REG_STAT_ATTR = 0;
    private static final int CLASS_STAT_ATTR = 1;

    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Constructor
     *
     * The input consists of an array of ClusAttrType objects. These objects contain all kinds of information about a certain attribute.
     * We compile two lists of ClusAttrTypes, splitting them in Numeric and Nominal types,
     * and we create two dedicated objects to handle the statistics of these two cases.
     * */
    public VTVCombinedStat(ClusAttrType[] attrs) {
        // We build to lists to host the different kinds (i.e. numeric and nominal) of target attributes.
        LinkedList<NumericAttrType> numAttrs = new LinkedList<>();
        LinkedList<NominalAttrType> nomAttrs = new LinkedList<>();
        m_AttrIndex = new ArrayList<int[]>();

        for (ClusAttrType attr : attrs) {                                           // For each target attribute (that was part of the input of this method)
            switch (attr.getTypeIndex()) {
                case 0:    // NominalAttrType
                    // If the attribute is a nominal one, add it to the ArrayList of NominalAttrTypes
                    nomAttrs.add((NominalAttrType) attr);
                    // Add the kind of statistic coupled with the index in the nomAttrs array to the m_AttrIndex ArrayList
                    m_AttrIndex.add(new int[]{CLASS_STAT_ATTR, nomAttrs.size() - 1});
                    break;
                case 1:    // NumericAttrType
                    // If the attribute is a numeric one, add it to the ArrayList of NominalAttrTypes
                    numAttrs.add((NumericAttrType) attr);
                    // Add the kind of statistic coupled with the index in the nomAttrs array to the m_AttrIndex ArrayList
                    m_AttrIndex.add(new int[]{REG_STAT_ATTR, numAttrs.size() - 1});
                    break;
                default:
                    // Some error messages for if we have other types of targets.
                    System.err.println("VTVCombinedStat doesn't know how to handle target attributes of type "
                            + attr.getClass().getSimpleName());
                    throw new NotImplementedException(
                            "VTVCombinedStat doesn't know how to handle target attributes of type "
                                    + attr.getClass().getSimpleName());
            }
        }
        // Create our two specific Stat objects for the seperate cases
        m_RegStat = new VTVRegressionStat(numAttrs.toArray(new NumericAttrType[] {}));
        m_ClassStat = new VTVClassificationStat(nomAttrs.toArray(new NominalAttrType[] {}));
    }

    /**
     * Build this object from its logical components (bottom up basically)
     * */
    public VTVCombinedStat(VTVRegressionStat regStat, VTVClassificationStat classStat, ArrayList<int[]> attrIndex) {
        m_RegStat = regStat;
        m_ClassStat = classStat;
        m_AttrIndex = attrIndex;
    }

    /**
     * Clone the current object,
     * we do this by cloning its components.
     * */
    @Override
    public ClusStatistic cloneStat() {
        return new VTVCombinedStat((VTVRegressionStat) m_RegStat.cloneStat(),
                (VTVClassificationStat) m_ClassStat.cloneStat(), m_AttrIndex);
    }

    /**
     * Update the weights. This gets done by the two components
     * */
    @Override
    public void updateWeighted(DataTuple tuple, int idx) {
        m_RegStat.updateWeighted(tuple, tuple.getWeight());
        m_ClassStat.updateWeighted(tuple, tuple.getWeight());
    }

    /**
     * calculation of the mean, also done on the lower level.
     * */
    @Override
    public void calcMean() {
        m_RegStat.calcMean();
        m_ClassStat.calcMean();
    }

    /**
     * Get a string with the info of the statistics (of the prediction)
     * Once again we rely on the specific methods of the components.
     * */
    @Override
    public String getString(StatisticPrintInfo info) {
        StringBuffer buf = new StringBuffer();
        buf.append("[");
        buf.append(m_ClassStat.getString(info));
        buf.append(" | ");
        buf.append(m_RegStat.getString(info));
        buf.append("]");
        return buf.toString();
    }

    /**
     * Get a string that gives the predicted classname for a given target attribute, which is provided to this method by its index
     * */
    @Override
    public String getPredictedClassName(int idx) {
        int[] loc = m_AttrIndex.get(idx);
        switch (loc[0]) {
            case REG_STAT_ATTR:
                return m_RegStat.getPredictedClassName(loc[1]);
            case CLASS_STAT_ATTR:
                return m_ClassStat.getPredictedClassName(loc[1]);
            default:
                return "";
        }
    }

    /**
     * Predict a tuple.
     * */
    @Override
    public void predictTuple(DataTuple prediction) {
        m_ClassStat.predictTuple(prediction);
        m_RegStat.predictTuple(prediction);
    }

    /**
     * Get the nominal predictions
     * */
    @Override
    public int[] getNominalPred() {
        return m_ClassStat.getNominalPred();
    }

    /**
     * Collect all the nominal attributes
     * */
    public NominalAttrType[] getNominalAttr() {
        return m_ClassStat.m_Attrs;
    }

    /**
     * Get the numeric predictions
     * */
    @Override
    public double[] getNumericPred() {
        return m_RegStat.getNumericPred();
    }

    /**
     * Collect all the numeric attributes
     * */
    public NumericAttrType[] getNumericAttr() {
        return m_RegStat.m_Attrs;
    }

    /**
     * Add a given prediction with given weight to our current object
     * */
    @Override
    public void addPrediction(ClusStatistic other, double weight) {
        VTVCombinedStat or = (VTVCombinedStat) other;
        m_RegStat.addPrediction(or.m_RegStat, weight);
        m_ClassStat.addPrediction(or.m_ClassStat, weight);
    }

    /**
     * Return all statistics in one string.
     * */
    @Override
    public String getArrayOfStatistic() {
        StringBuffer buf = new StringBuffer();
        buf.append("[");
        for (int i = 0; i < m_AttrIndex.size(); i++) {
            if (i != 0)
                buf.append(",");
            int[] loc = m_AttrIndex.get(i);
            switch (loc[0]) {
                case CLASS_STAT_ATTR:
                    buf.append(m_ClassStat.getSingleStat(loc[1]));
                    break;
                case REG_STAT_ATTR:
                    buf.append(m_RegStat.getSingleStat(loc[1]));
                    break;
                default:
                    break;
            }
        }
        buf.append("]");
        return buf.toString();
    }

    /**
     * Reset all the properties
     * */
    @Override
    public void reset() {
        m_SumWeight = 0;
        m_RegStat.reset();
        m_ClassStat.reset();
    }

    /**
     * Copy another VTVCombinedStat object into this one
     * */
    @Override
    public void copy(ClusStatistic other) {
        VTVCombinedStat or = (VTVCombinedStat) other;
        m_SumWeight = or.m_SumWeight;
        m_RegStat.copy(or.m_RegStat);
        m_ClassStat.copy(or.m_ClassStat);
    }

    /**
     * Add a complete set of predictions (other) to our current object.
     * This resembles, but is not equal to, the addPrediction method.
     * */
    @Override
    public void add(ClusStatistic other) {
        VTVCombinedStat or = (VTVCombinedStat) other;
        m_RegStat.add(or.m_RegStat);
        m_ClassStat.add(or.m_ClassStat);
        m_SumWeight += or.m_SumWeight;
    }

    /**
     * The opposite of the add method
     * */
    @Override
    public void subtractFromThis(ClusStatistic other) {
        VTVCombinedStat or = (VTVCombinedStat) other;
        m_RegStat.subtractFromThis(or.m_RegStat);
        m_ClassStat.subtractFromThis(or.m_ClassStat);
        m_SumWeight -= or.m_SumWeight;
    }

    /**
     * Analogue to the substractFromThis method
     * */
    @Override
    public void subtractFromOther(ClusStatistic other) {
        VTVCombinedStat or = (VTVCombinedStat) other;
        m_RegStat.subtractFromOther(or.m_RegStat);
        m_ClassStat.subtractFromOther(or.m_ClassStat);
        m_SumWeight = or.m_SumWeight - m_SumWeight;
    }

    /**
     * Vote considering the given votes (without modelScores)
     * */
    @Override
    public void vote(ArrayList votes) {
        voteWeighted(votes, null);
    }

    /**
     * Vote given a list of ClusStatistics and a list of modelScores.
     * This is the logical scenario for a voting procedure.
     * */
    public void voteWeighted(ArrayList<ClusStatistic> votes, ArrayList<MercsModelScore> modelScores) {
        ArrayList<RegressionStatBase> rVotes = new ArrayList<RegressionStatBase>();
        ArrayList<ClassificationStat> cVotes = new ArrayList<ClassificationStat>();

        // Initialize ArrayLists of MercsModelScores
        ArrayList<MercsModelScore> rScores = null;
        ArrayList<MercsModelScore> cScores = null;
        if (modelScores != null) rScores = new ArrayList<MercsModelScore>();
        if (modelScores != null) cScores = new ArrayList<MercsModelScore>();

        // Fill in the xVotes andxScores objects
        for (int i = 0; i < votes.size(); i++) {
            ClusStatistic vote = votes.get(i);
            if (vote instanceof RegressionStatBase) {
                rVotes.add((RegressionStatBase) vote);
                if (modelScores != null) rScores.add(modelScores.get(i));
            } else if (vote instanceof ClassificationStat) {
                cVotes.add((ClassificationStat) vote);
                if (modelScores != null) cScores.add(modelScores.get(i));
            } else if (vote instanceof CombStat) {
                rVotes.add(((CombStat) vote).getRegressionStat());
                cVotes.add(((CombStat) vote).getClassificationStat());
                if (modelScores != null) {
                    cScores.add(modelScores.get(i));
                    rScores.add(modelScores.get(i));
                }
            } else {
                throw new IllegalArgumentException("Votes of type "
                        + vote.getClass().getName()
                        + " are not supported.");
            }
        }

        // Fill in the properties of this object
        if (modelScores == null) {
            m_RegStat.vote(rVotes);
            m_ClassStat.vote(cVotes);
        } else {
            m_RegStat.voteWeighted(rVotes, cScores);
            m_ClassStat.voteWeighted(cVotes, rScores);
        }
    }

    /**
     * Simplify the statistics if possible, namely when we only have one type of target attribute.
     * */
    public ClusStatistic simplifyStatistic() {
        if (m_RegStat.m_NbAttrs == 0) {
            return m_ClassStat;
        } else if (m_ClassStat.m_NbTarget == 0) {
            return m_RegStat;
        } else {
            return this;
        }
    }

    /**
     * Merges the statistics of two VTVCombinedStat statistics.
     *
     * Target attributes that are contained in both statistics are overwritten
     * using the new values. The indexing that was used to the original
     * attributes remains in effect. Target attributes that were in this object
     * but not in the other object are kept as-is (includes indexing). Target
     * attributes that were not in this object but are in the other object are
     * added to the tail end of the attribute collection.
     *
     * @param combStat
     */
    public void mergeStatistics(VTVCombinedStat combStat) {
        int lastRIndex = m_RegStat.m_NbAttrs - 1;
        int lastCIndex = m_ClassStat.m_Attrs.length - 1;

        int[] newRegPositions = m_RegStat.mergeStatistics(combStat.m_RegStat);
        int[] newClassPositions = m_ClassStat.mergeStatistics(combStat.m_ClassStat);

        int ir = 0;
        int ic = 0;
        for (int[] loc : combStat.m_AttrIndex) {
            switch (loc[0]) {
                case REG_STAT_ATTR:
                    if (newRegPositions[ir] > lastRIndex) {
                        m_AttrIndex.add(new int[] { REG_STAT_ATTR, newRegPositions[ir] });
                    }
                    ir++;
                    break;
                case CLASS_STAT_ATTR:
                    if (newClassPositions[ic] > lastCIndex) {
                        m_AttrIndex.add(new int[] { CLASS_STAT_ATTR, newClassPositions[ic] });
                    }
                    ic++;
                    break;
            }
        }

        assert m_RegStat.m_NbAttrs + m_ClassStat.m_NbTarget == m_AttrIndex.size();
    }
}
