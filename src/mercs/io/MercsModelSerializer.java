package mercs.io;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import mercs.model.MercsModel;
import mercs.model.MultiDirectionalEnsembleModel;
import mercs.model.SimpleEnsembleModel;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 *
 * Class which handles the (de-)serialization of a Multi Directional Ensemble model.
 * The class is currently based on the kryo framework, as this provides an easy
 * method to handle the serialization and de-serialization of java objects.
 *
 * If need be, this class can be extended/altered to use a dedicated scheme to store
 * the MERCS models.
 *
 * This class is used when we cluster, when in the Mercs class the startrun() method calls the
 * load method from the ModelBuilder class. Currently, this is only a thing in the clustering case.
 *
 * NB.: Currently, only SimpleEnsembleModels can be stored in a good way.
 *
 * References:
 * - Kryo serialization framework: https://github.com/EsotericSoftware/kryo
 *
 * Dependencies for Kryo:
 * - MinLog logging library: https://github.com/EsotericSoftware/minlog
 * - Reflectasm library: https://github.com/EsotericSoftware/reflectasm
 * - Objenesis library: https://github.com/easymock/objenesis
 */
public class MercsModelSerializer {


    /*
     * =======================================
     * METHODS
     * =======================================
     */

    /**
     * Method to persist a generated MERCS-model to disk, with the intent of later retrieving it.
     * @param model The model that is to be persisted
     * @param outputPath The location where the model is to be stored
     */
    public static boolean writeModelToFile(MercsModel model, String outputPath) {
        boolean succes = true;
        new File(getDirectoriesFromPath(outputPath)).mkdirs();

        if(model instanceof MultiDirectionalEnsembleModel) {
            SimpleEnsembleModel simpleModel = ((MultiDirectionalEnsembleModel) model).simplify();
            Kryo kryo = new Kryo();                     // This is a very straightforward framework to write a java class to a file (writeObject method)

            try {
                Output output = new Output(new FileOutputStream(outputPath));
                kryo.writeObject(output, simpleModel);
                output.close();
            } catch (FileNotFoundException e) {
                String message = "Failed to store MERCS model. File '"  +
                        outputPath + "' not found.";
                System.out.println(message);
                succes = false;
            }
        } else {
            String message = "Currently only storage of SimpleEnsembleModels is supported";
            System.out.println(message);
            succes = false;
        }

        return succes;
    }

    /**
     * Helper method to extract the directories part from a given path string.
     * @param path The path to examine
     * @return the directory part of a file path
     */
    private static String getDirectoriesFromPath(String path) {
        char separator = File.separatorChar;
        int index = path.lastIndexOf(separator);
        String directories;

        if(index > 0) {
            directories = path.substring(0, index);
        } else {
            directories = "";
        }

        return directories;
    }

    /**
     * Method to load a persisted MERCS-model from disk.
     * @param inputPath The location where the model is stored
     * @return The model that was persisted on disk
     */
    public static SimpleEnsembleModel readModelFromFile(String inputPath) {
        SimpleEnsembleModel model = null;
        Kryo kryo = new Kryo();
        // Allow Kryo to instantiate objects from classes which don't have a no-arg constructor
        kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));

        // Again, very straightforward use of this kryo framework. We just need to know from which class we are reading something.
        try {
            Input input = new Input(new FileInputStream(inputPath));
            model = kryo.readObject(input, SimpleEnsembleModel.class);
            input.close();
        } catch (FileNotFoundException e) {
            String message = "Failed to load MERCS model. File '" +
                    inputPath + "' not found.";
            System.out.println(message);
        }

        return model;
    }
}
